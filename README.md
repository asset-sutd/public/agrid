AGRID: Automatic Generation of RobotIc Descriptions
===================================================

AGRID aims at encoding discrete models of robotic systems for efficient
verification. The methodology based on the tool works as follows:

1. Define the scenario you aim at verifying; identify its parameters.
2. Run agrid passing the identify parameters. Choose to generate either a 
Satisfiability Modulo Theory (SMT) or a Bounded Model Checking (BMC) description.
3. Solve the generated description by using the appropriate solver, i.e., use
Z3 to solve the SMT encoding, NuSMV to solve the BMC encoding.

Contents
--------
- demo: examples of specifications to be used as input for AGRID.
- include and src: source code of AGRID.
- third_party: software used by AGRID to manage json descriptions (for experimental
        under-development features).
- SMT_extended: under development features for represent uncertainties.

Installation
------------

To install the software it is necessary to download the source code and compile
it. The tool is completely written in C++, thus it require a full C++ toolchain.

### Requirements

To compile the software you need the following software:
- a complete C++ toolchain compatible with C++-11: g++ 7+ or clang 3.3+;
- Cmake version 2.4+;

In order to evaluate the models produced by AGRID, you will need (at least) one
of the following tools to be used as backends for the AGRID-centered verification
methodology:
- NuSMV, version 2+, to use the BMC flow;
- Z3, latest version from the GitHub project [repository](https://github.com/Z3Prover/z3),
to use the SMT flow.

### Compile

1. move to the main project directory and create a working directory:
`$> cd agrid `
`$> mkdir objdir`
`$> cd objdir`

2. Run cmake:
`$> cmake ..`

3. Compile:
`$> make`

4. Install:
`$> make install`
To specify the directory DIR where to install, step 2 should be performed as
follows:
`$> cmake -DCMAKE_INSTALL_PREFIX=DIR ..

Usage
-----

The complete usage of the tool is the following.

`agrid	[-T] [-M] [-d size] [-x starting width] [-y starting height]`
`	[-O obstacles list file] [-s sensor type] [-c control sw file]`
`	[-m mission file] [-V] [-o output file]`
`	[-U] [-P sensor delay value] [-p minimum actuation delay]`
`	[-q maximum actuation delay]`
`	[-B maximum number of evaluation steps (for SMT)]`

-T enables the use of SMT rather than Bounded Model Checking
-M set the scenario to positive. Otherwise is negative.
-U Enable uncertainties.
-d sizeSize is expressed using the [width]X[height] syntax, where width
	and height are two positive integers.
-O obstacles_file Obstacles are listed in a file one obstacle per line.
	each entry is expressed on a line with syntax: X,Y
	where X will be the horizontal coordinate, while Y is the vertical
-s sensor type Sensors may be: standard, proximity, or radar. The type is
	followed by the length of the sensor:
	for instance, proximity2 indicates a proximity sensor of length 2.


### Use Cases

1. Generating a model on a 3 x 3 occupancy grid. The obstacle file is named 
obstacles.txt; sensors are one radar with sensing distance of 2; The mission
file is mission.smv; the control software is described in sw.smv; actuation and
sensing delay is set to 1. The robot starts in position 0,0. We want to solve
the problem is encoded as a SMT problem in the out.smt file.

`agrid -T -x 0 -y 0 -O obstacles.txt -s radar -c sw.smt - mission.smt -p 1 -P 1  -o out.smt`

The same scenario can be encoded as a BMC problem using the following command

`agrid -x 0 -y 0 -O obstacles.txt -s radar -c sw.smv - mission.smv -p 1 -P 1  -o out.smv`



