/**
@author      <a href="mailto:2016csy0004@iitrpr.ac.in">Amit Behal</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/main.cc
 * @brief   Entry point of the verifying tool.
 */

#include "main.hh"

using namespace SMT_extended;

int main( int argc, char * argv[] )
{
    Parameters * params = parseCmdLine(argc, argv);
    VerificationProblem * problem = new VerificationProblem(params);

    if(problem->getParameters()->use_smt == true) 
    {

        SMTPrinterVelAcc printer(problem);
	    printer.printSMTFile();

    }	

    

    delete(problem);
    delete(params);

}
