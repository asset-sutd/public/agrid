/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/nusmv_printer.cc
 * @brief   Source for the printer.
 */

#include "nusmv_printer.hh"

using namespace roboverify;

NuSMVPrinter::NuSMVPrinter( VerificationProblem * p ) :
    _problem(p),
    _parameters(p->getParameters())
{
}

NuSMVPrinter::~NuSMVPrinter()
{
}

void NuSMVPrinter::printNuSMVFile()
{
    std::ofstream outfile;
    outfile.open(_parameters->fileOut);

    // Print initial part: Mission planning.
    outfile << _getMissionSkeleton(); 

    // Assignment of target_achieved variable.
    outfile << _getTargetAchievedAssignment();

    // Print the decalaration of the occupancy grid. 
    outfile << _getSpaceDeclaration();

    // Print the declaration of the variable implementing the sensors
    outfile << _getSensorsDeclaration();

    // Print the declaration of the variable implementing the obstacles
    outfile << _getObstaclesDeclaration();

    // Print the sensor computation operations.
    outfile << _getSensorsComputation();

    // Print the obstacles computation operations.
    outfile << _getObstaclesComputation();


    // Assignment of the next position.
    outfile << _getNextPositionComputation();

    // Compute the matrix for static obstacles.
    outfile << _getOccupancyGridWithObstacle();

    // Print the controller skeleton.
    outfile << _getControllerSkeleton();

    outfile << _getMain();

    outfile.close();
}

std::string NuSMVPrinter::_getMissionSkeleton()
{
    std::stringstream ss;

    ss <<
        "MODULE mission(position_x, position_y)" << std::endl <<
        "\tVAR" << std::endl <<
        "\t\ttarget_achieved : boolean;" << std::endl << 
        "\t\ttarget_x : 0.." << std::to_string(_parameters->width - 1) 
        << ";" << std::endl <<
        "\t\ttarget_y : 0.." << std::to_string(_parameters->height - 1) 
        << ";" << std::endl << std::endl;

    if(_problem->getTiming() > 0)
    {
        ss << "\t\tmonitor_counter : 0.." << _problem->getTiming() + 2 << ";\n";
    } 

    if( _parameters->mission == "" )
    {
        ss << "\t\t -- PUT YOUR MISSION HERE!\n" << std::endl;
    }
    else
    {
        ss << _getMission() << std::endl;
    }

    if( _problem->getTiming() > 0 )
    {
        ss << "\tASSIGN" << std::endl <<
            "\t\tinit(monitor_counter) := 0;\n";
        ss << "\t\tnext(monitor_counter) :=\n" <<
            "\t\tcase\n" <<
            "\t\t\ttarget_achieved : 0;\n" <<
            "\t\t\t! target_achieved & monitor_counter < " 
            << _problem->getTiming() + 2
            << " : monitor_counter + 1;\n" <<
            "\t\t\tTRUE : monitor_counter;\n\t\tesac;\n"
            << std::endl;
    }

    return ss.str();
}

std::string NuSMVPrinter::_getMission()
{
    std::stringstream ss;

    std::string line;
    std::ifstream ifile(_parameters->mission.c_str());
    if( ifile.is_open() )
    {
        while( getline(ifile, line) )
        {
            ss << "\t" << line << std::endl;
        }
        ifile.close();
    }
    else
    {
        printError("The controller file does not exist.");
    }

    ss << std::endl;
    return ss.str();
}


std::string NuSMVPrinter::_getSpaceDeclaration()
{
    std::stringstream ss;

    ss << "MODULE environment(move)" << std::endl <<
        "\tVAR" << std::endl;
    if( _problem->sensors[0]->type != "proximity" && _problem->sensors[0]->type != "radar" )
    {
        ss << "\t\toccupancy : array -2.." + std::to_string(_parameters->width + 1)
            << " of array -2.." + std::to_string(_parameters->height + 1) <<
            " of boolean;" << std:: endl;
    }
    else
    {
        uint32_t length = _problem->sensors[0]->length;
        ss << "\t\toccupancy : array -" << 
            std::to_string( length + 1 ) << ".." 
            << std::to_string(_parameters->width + length ) <<
            " of array -" <<
            std::to_string( length + 1 ) << ".." 
            << std::to_string(_parameters->width + length ) <<
            " of boolean;\n";
    }
    ss <<  "\t\tposition_x : -1.." << std::to_string(_parameters->width) << ";" 
        << std::endl <<
        "\t\tposition_y : -1.." << std::to_string(_parameters->height) << ";" 
        << std::endl << std::endl;

    return ss.str();
}

std::string NuSMVPrinter::_getSensorsDeclaration()
{
    std::string sensors = _problem->sensors[0]->type;
    if( sensors == "radar" )
    {
        return _getSensorsDeclarationRadar(_problem->sensors[0]);
    }
    else if ( sensors == "proximity" )
    {
        return _getSensorsDeclarationProximity(_problem->sensors[0]);
    }
    else
    {
        return _getSensorsDeclarationStandard();
    }



}


std::string NuSMVPrinter::_getSensorsDeclarationStandard()
{
    std::string ret("");

    ret += "\t\tsensor_up : boolean;\n";
    ret += "\t\tsensor_down : boolean;\n";
    ret += "\t\tsensor_left : boolean;\n";
    ret += "\t\tsensor_right : boolean;\n\n";

    ret += "\t\tsensors : array 0..3 of 0..1;\n\n";

    return ret;
}

std::string NuSMVPrinter::_getSensorsDeclarationProximity(Sensors * sensors)
{
    std::string ret("");
    ret += "\t\tsensors : array 0..3 of 0..";
    ret += std::to_string(sensors->length);
    ret += ";\n";

    return ret;
}

std::string NuSMVPrinter::_getSensorsDeclarationRadar(Sensors * sensors)
{
    std::string ret("");

    ret += "\t\tsensors : array -";
    ret += std::to_string(sensors->length);
    ret += "..";
    ret += std::to_string(sensors->length);
    ret += " of array -";
    ret += std::to_string(sensors->length);
    ret += "..";
    ret += std::to_string(sensors->length);
    ret += " of 0..1;\n\n";
    return ret;
}


std::string NuSMVPrinter::_getObstaclesDeclaration()
{
    std::stringstream ss;

    for( size_t i = 0; i < _problem->obstacles.size(); ++i )
    {
        ss <<
            "\t\tobstacle_" << std::to_string(i) << "_x : 0.."
            << std::to_string(_parameters->width - 1) << ";\n";
        ss <<
            "\t\tobstacle_" << std::to_string(i) << "_y : 0.."
            << std::to_string(_parameters->height - 1) << ";\n";
    }

    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getSensorsComputation()
{
    std::string sensors = _problem->sensors[0]->type;
    if( sensors == "radar" )
    {
        return _getSensorsComputationRadar(_problem->sensors[0]);
    }
    else if( sensors == "proximity" )
    {
        return _getSensorsComputationProximity(_problem->sensors[0]);
    }
    else
    {
        return _getSensorsComputationStandard();
    }
}

std::string NuSMVPrinter::_getSensorsComputationStandard()
{
    std::string ret("");

    ret += "\tASSIGN\n"; 
    ret += "\t\tsensor_up := (occupancy[position_x][position_y + 1]);\n";
    ret += "\t\tsensor_down := (occupancy[position_x][position_y - 1]);\n";
    ret += "\t\tsensor_left := (occupancy[position_x - 1][position_y]);\n";
    ret += "\t\tsensor_right := (occupancy[position_x + 1][position_y]);\n\n";

    ret += "\tASSIGN\n";
    ret += "\t\tsensors[0] :=\n";
    ret += "\t\tcase\n";
    ret += "\t\t\tsensor_up : 1;\n";
    ret += "\t\t\tTRUE : 0;\n";
    ret += "\t\tesac;\n";
    ret += "\t\tsensors[1] := \n";
    ret += "\t\tcase\n";
    ret += "\t\t\tsensor_down : 1;\n";
    ret += "\t\t\tTRUE : 0;\n";
    ret += "\t\tesac;\n";
    ret += "\t\tsensors[2] := \n";
    ret += "\t\tcase\n";
    ret += "\t\t\tsensor_left : 1;\n";
    ret += "\t\t\tTRUE : 0;\n";
    ret += "\t\tesac;\n";
    ret += "\t\tsensors[3] := \n";
    ret += "\t\tcase\n";
    ret += "\t\t\tsensor_right : 1;\n";
    ret += "\t\t\tTRUE : 0;\n";
    ret += "\t\tesac;\n\n";



    return ret;
}

std::string NuSMVPrinter::_getSensorsComputationProximity(Sensors * sensors)
{
    std::string ret("");

    ret += "\tASSIGN\n";
    ret += "\t\tsensors[0] :=\n";
    ret += "\t\tcase\n";

    uint32_t length = sensors->length;

    for( uint32_t i = 1; i <= length; ++i )
    {
        ret += "\t\t\t";
        ret += "!occupancy[position_x][position_y + ";
        ret += std::to_string(i);
        ret += "] : ";
        ret += std::to_string(i - 1);
        ret += ";\n";
    }
    ret += "\t\t\tTRUE : ";
    ret += std::to_string(length);
    ret += ";\n";
    ret += "\t\tesac;\n\n";

    ret += "\t\tsensors[1] :=\n";
    ret += "\t\tcase\n";

    length = sensors->length;

    for( uint32_t i = 1; i <= length; ++i )
    {
        ret += "\t\t\t!occupancy[position_x][position_y - ";
        ret += std::to_string(i);
        ret += "] : ";
        ret += std::to_string(i - 1);
        ret += ";\n";
    }
    ret += "\t\t\tTRUE : ";
    ret += std::to_string(length);
    ret += ";\n";
    ret += "\t\tesac;\n\n";

    ret += "\t\tsensors[2] :=\n";
    ret += "\t\tcase\n";

    length = sensors->length;

    for( uint32_t i = 1; i <= length; ++i )
    {
        ret += "\t\t\t";
        ret += "!occupancy[position_x - ";
        ret += std::to_string(i);
        ret += "][position_y] : ";
        ret += std::to_string(i - 1);
        ret += ";\n";
    }
    ret += "\t\t\tTRUE : ";
    ret += std::to_string(length);
    ret += ";\n";
    ret += "\t\tesac;\n\n";

    ret += "\t\tsensors[3] :=\n";
    ret += "\t\tcase\n";

    length = sensors->length;

    for( uint32_t i = 1; i <= length; ++i )
    {
        ret += "\t\t\t!occupancy[position_x + ";
        ret += std::to_string(i);
        ret += "][position_y] : ";
        ret += std::to_string(i - 1);
        ret += ";\n";
    }
    ret += "\t\t\tTRUE : ";
    ret += std::to_string(length);
    ret += ";\n";
    ret += "\t\tesac;\n\n";



    return ret;   
}

std::string NuSMVPrinter::_getSensorsComputationRadar(Sensors * sensors)
{
    std::string ret("");

    ret += "\tASSIGN\n";
    int32_t init, end;
    init = sensors->length * -1;
    end = sensors->length;

    std::string ret1("");
    std::string ret2("");

    for( int32_t i = init; i <= end; ++i )
    {
        for( int32_t j = init; j <= end; ++j )
        {
            std::string tret("");
            tret += "\t\tsensors[";
            tret += std::to_string(i);
            tret += "][";
            tret += std::to_string(j);
            tret += "] := ";

            if( abs(i) + abs(j) > abs(sensors->length) )
            {
                tret += "0;\n";
                ret1 += tret;
            }
            else
            {
                tret += "\n\t\tcase\n";
                tret += "\t\t\toccupancy[position_x";
                if( i != 0 )
                {
                    i > 0 ? tret += " + " : tret += " - ";
                    tret += std::to_string(abs(i));
                }
                tret += "][position_y";

                if( j != 0 )
                {
                    j > 0 ? tret += " + " : tret += " - ";
                    tret += std::to_string(abs(j));
                }
                tret += "] : 1;\n";
                tret += "\t\t\tTRUE: 0;\n\t\tesac;\n\n";
                ret2 += tret;
            }
        }
    }
    ret += ret1;
    ret += "\n";
    ret += ret2;
    ret += "\n";

    return ret;
}

std::string NuSMVPrinter::_getObstaclesComputation()
{
    std::stringstream ss;

    ss << "\tASSIGN" << std::endl;

    ss << "\t\tinit(position_x) := " << _parameters->sx << ";\n";
    ss << "\t\tinit(position_y) := " << _parameters->sy << ";\n";

    // For now I consider only static obstacles.
    /// \TODO consider dynamic obstacles.
    for( size_t i = 0; i < _problem->obstacles.size(); ++i )
    {
        Obstacle * current = _problem->obstacles[i];

        ss << "\t\tobstacle_" << std::to_string(i) <<
            "_x := " << std::to_string(current->x) << ";" << std::endl;
        ss << "\t\tobstacle_" << std::to_string(i) <<
            "_y := " << std::to_string(current->y) << ";" << std::endl;
    }
    ss << std::endl;
    return ss.str();
}


std::string NuSMVPrinter::_getNextPositionComputation()
{
    std::stringstream ss;

    ss <<
        "\tTRANS" << std::endl <<
        "\tcase" << std::endl <<
        "\t\t(move = DOWN) : " << std::endl <<
        "\t\t\tnext(position_y) = position_y - 1 &" << std::endl <<
        "\t\t\t\tnext(position_x) = position_x;" << std::endl <<
        std::endl <<
        "\t\t(move = UP) : " << std::endl <<
        "\t\t\tnext(position_y) = position_y + 1 &" << std::endl <<
        "\t\t\t\tnext(position_x) = position_x;" << std::endl <<
        std::endl <<
        "\t\t(move = LEFT) : " << std::endl <<
        "\t\t\tnext(position_x) = position_x - 1 &" << std::endl <<
        "\t\t\t\tnext(position_y) = position_y;" << std::endl <<
        std::endl <<
        "\t\t(move = RIGHT) : " << std::endl <<
        "\t\t\tnext(position_x) = position_x + 1 &" << std::endl <<
        "\t\t\t\tnext(position_y) = position_y;" << std::endl <<
        std::endl <<
        "\t\tTRUE : " << std::endl <<
        "\t\t\tnext(position_x) = position_x &" << std::endl <<
        "\t\t\t\tnext(position_y) = position_y;" << std::endl <<
        "\tesac;" << std::endl << std::endl;

    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getTargetAchievedAssignment()
{
    std::stringstream ss;

    ss <<
        "\tASSIGN" << std::endl <<
        "\t\ttarget_achieved :=" << std::endl <<
        "\t\t\t(position_x = target_x & position_y = target_y);" << std::endl;

    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getOccupancyGridWithObstacle()
{
    std::stringstream ss;

    ss << "\tASSIGN" << std::endl;
    int w = static_cast< int >( _parameters->width );
    int h = static_cast< int >( _parameters->height );
    int start = -2;
    int lw = w + 1;
    int lh = h + 1;

    if( _problem->sensors[0]->type == "proximity" ||
            _problem->sensors[0]->type == "radar" )
    {
        start = (_problem->sensors[0]->length + 1) * (-1);

        lw = lw + (_problem->sensors[0]->length - 1);
        lh = lh + (_problem->sensors[0]->length - 1);
    }

    for( int x = start; x <= lw; ++x )
    {
        for(int y = start; y <= lh; ++y )
        {

            if((x <= -1) || (x >= w) || (y <= -1) || (y >= h))
            {
                ss <<
                    "\t\toccupancy[" << std::to_string(x) << "]["
                    << std::to_string(y) << "]\t:= FALSE;\n";
            }

        }
    }

    ss << std::endl;

    for( int x = 0; x < w; ++x )
    {
        for(int y = 0; y < h; ++y )
        {
            ss << "\t\t" <<
                std::string("occupancy[" + 
                        std::to_string(x) + "][" + std::to_string(y) 
                        + "]\t:= (!(\n\t\t\t");
            ss <<
                std::string("(position_x = " + std::to_string(x) + " & " +
                        "position_y = " + std::to_string(y) + ")");


            // Obstacles.
            for( size_t i = 0; i < _problem->obstacles.size(); ++i )
            {
                ss << " |\n\t\t\t" << 
                    std::string("(obstacle_" + std::to_string(i) +
                            "_x = " + std::to_string(x) + " & " +
                            "obstacle_" + std::to_string(i) +
                            "_y = " + std::to_string(y) + ")");

            }

            ss << "));" << std::endl;



        }
    }

    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getControllerSkeleton()
{
    std::stringstream ss;

    ss << std::endl << std::endl <<
        "MODULE controller(sensors, position_x, position_y, target_x, target_y )"
        << std::endl;
    ss <<
        "\tVAR\n" <<
        "\t\tcommand : {UP, DOWN, LEFT, RIGHT, NOP};" 
        << std::endl
        <<
        "\t\tintention : {UP, DOWN, LEFT, RIGHT, NOP};" 
        << std::endl;


    for( uint32_t i = 0; i < _problem->getLatency(); ++i )
    {
        ss << "\t\tlatency_" << i << " : {UP, DOWN, LEFT, RIGHT, NOP};"
            << std::endl;
    }
    ss << std::endl;

    if( _parameters->controller == "" )
    {
        ss << "\t\t -- PUT YOUR CONTROLLER IMPLEMENTATION HERE!\n" << std::endl;
    }
    else
    {
        ss << _getControllerSw() << std::endl;
    }

    ss << "\tASSIGN" << std::endl;
    if( _problem->getLatency() == 0 )
    {
        ss << "\t\tcommand := intention;" << std::endl;
    }
    else
    {
        ss << "\t\tinit(command) := NOP;" << std::endl;
        ss << "\t\tnext(latency_0) := intention;" << std::endl;
        ss << "\t\tinit(latency_0) := NOP;" << std::endl;
        for( uint32_t i = 1; i < _problem->getLatency(); ++i )
        {
            ss << "\t\tnext(latency_" << i << ") := latency_" << i-1 << ";\n";
            ss << "\t\tinit(latency_" << i << ") := NOP;" << std::endl;
        }
        ss << "\t\tnext(command) := latency_" 
            << _problem->getLatency() - 1 << ";" << std::endl;
    }


    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getControllerSw()
{
    std::stringstream ss;

    std::string line;
    std::ifstream ifile(_parameters->controller.c_str());
    if( ifile.is_open() )
    {
        while( getline(ifile, line) )
        {
            ss << "\t" << line << std::endl;
        }
        ifile.close();
    }
    else
    {
        printError("The controller file does not exist.");
    }

    ss << std::endl;
    return ss.str();
}

std::string NuSMVPrinter::_getMain()
{
    std::stringstream ss;

    ss << "MODULE main" << std::endl;
    ss << "\tVAR" << std::endl;
    ss << "\t\tmiss : mission(env.position_x, env.position_y);" << std::endl;
    ss << "\t\tenv : environment(robot.command);" << std::endl;
    ss << "\t\trobot : controller(env.sensors, env.position_x, env.position_y,"
        << std::endl <<  "\t\t\tmiss.target_x, miss.target_y);" << std::endl;
    ss << std::endl;

    ss << "LTLSPEC G(miss.target_achieved | miss.monitor_counter <=" <<
        _problem->getTiming() << ")";
    ss << std::endl;

    ss << "LTLSPEC G(env.position_x < "
        << _problem->getParameters()->width << " & env.position_x >= 0)" << std::endl;
    ss << "LTLSPEC G(env.position_y < "
        << _problem->getParameters()->height << " & env.position_y >= 0)" << std::endl;

    for( size_t i = 0; i < _problem->obstacles.size(); ++i )
    {
        ss << 
            "LTLSPEC G(env.position_x != env.obstacle_" << i << "_x | env.position_y != env.obstacle_" << i << "_y)"
            << std::endl;
    }
    if( _problem->getTiming() > 0 )
    {
        ss << "LTLSPEC G(miss.monitor_counter <= " << _problem->getTiming() << ")\n";
    }

    ss << std::endl;
    return ss.str();

}
