; ##### Robot Software - Melay Machines ( FSM With Output)
     
(define-fun Mel ( (prevCommand String) (s1 Bool) (s2 Bool) (s3 Bool) (s4 Bool)   (pos_x Int) (pos_y Int) (targ_x Int) (targ_y Int)) String   

(ite ( and (not(= prevCommand "down" )) (= s1 true) (> targ_y pos_y) )  "up" 
(ite ( and (not(= prevCommand "left" )) (= s3 true) (>= targ_x pos_x) (> targ_y pos_y) )  "right" 
(ite ( and (not(= prevCommand "right" )) (= s4 true) (<= targ_x pos_x) (> targ_y pos_y) )  "left" 

(ite (and  (not(= prevCommand "up" ))  (= s2 true) (< targ_y pos_y) ) "down"
(ite ( and (not(= prevCommand "left" )) (= s3 true) (>= targ_x pos_x) (< targ_y pos_y) )  "right" 
(ite ( and (not(= prevCommand "right" )) (= s4 true) (<= targ_x pos_x) (< targ_y pos_y) )  "left" 

(ite (and (not(= prevCommand "left" ))  (= s3 true)  (> targ_x pos_x)   ) "right"
(ite (and (not(= prevCommand "down" ))  (= s1 true) (>= targ_y pos_y)  (> targ_x pos_x)   ) "up"
(ite (and (not(= prevCommand "up" ))  (= s2 true) (<= targ_y pos_y) (> targ_x pos_x)   ) "down"

(ite (and (not(= prevCommand "right" ))  (= s4 true)  (< targ_x pos_x)   ) "left"
(ite (and (not(= prevCommand "down" ))  (= s1 true) (>= targ_y pos_y) (< targ_x pos_x)   ) "up"
(ite (and (not(= prevCommand "up" ))  (= s2 true) (<= targ_y pos_y) (< targ_x pos_x)   ) "down"  "null"  

))) ))) )))  ))))
     

