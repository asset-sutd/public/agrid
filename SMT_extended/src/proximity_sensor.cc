/**
 * @author      <a href="mailto:2016csy0004@iitrpr.ac.in">Amit Behal</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file    src/proimity_sensor.cc
 * @brief   Source for the printer.
 */

#include "smt_printer_velacc_model.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace SMT_extended;
std:: string SMTPrinterVelAcc::_computeSensorValuesProximity(int Range)
{

    std::string ret = "";  
   
    int defi = 0;

    if(defi == 0)
    {


        std::string funproto = "(define-fun det_up(";
  
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";
 
            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;

        std::string funbody = "";
        int i;


        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string check_outside = "";

        for(i=1;i<=2*numBlocksRobo;i++)
        {
            std::stringstream xIndex;
            xIndex << i;
         
            check_outside += "(< x!";
            check_outside += xIndex.str();
            check_outside += " 1) ";

            check_outside += "(> x!";
            check_outside += xIndex.str();
            check_outside += " ";
            check_outside += mpx.str();
            check_outside += ")";
        }


 
        funbody += "(ite ( or ";
        funbody += check_outside.c_str();
        funbody += " ) -1 \n\n" ;


        for(i= 0; i<= Range ; i++ )
        {
 
            std::stringstream rng ;
 
            if (i==0)
                rng << 0;
            else
                rng << i;

 
            funbody += "(ite ( or ";
       
            funbody += "\n      ";

            int dcount = 1;
            std::string dsa = "";

           while(dcount <= k)
           {

               std::stringstream da ;
               da << dcount;
             
               std::stringstream db ;
               db << dcount + 1;
          
               for(int all_i=1;all_i<=2*numBlocksRobo;all_i+=2)
               { 
                    std::stringstream temp_i;
                    std::stringstream temp_i_plus_one;

                    temp_i << all_i;
                    temp_i_plus_one << all_i+1;

                    dsa += "\n    (and ";
                    dsa += "(= ";
                    dsa += "x!";
                    dsa += temp_i.str();
                    dsa += " ";

                    dsa += "(first o" + da.str();
                    dsa += ")) ";
                
                    dsa +=  " (= (second o";
                    dsa += da.str();
                    dsa += ")   (+ x!";
                    dsa += temp_i_plus_one.str();
                    dsa += " ";
                    dsa += rng.str(); 
                    dsa += "))" ;
                    dsa += " )";

               
               }
 
 
               dsa += "\n";
 
  
               for(int temp_i=1;temp_i<=2*numBlocksRobo;temp_i+=2)
               { 

                   std::stringstream temp_all_i;
                   std::stringstream temp_all_i_plus_one;
               
                   temp_all_i << temp_i;
                   temp_all_i_plus_one << temp_i+1;


                   if(dcount ==1)
                   {           // Check Outside Map

                       dsa += "\n    (<  ";
                       dsa += "x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += " 1";
                       dsa += ") ";
                       dsa += "(>  ";
                       dsa += "x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += mpx.str();
                       dsa += ") ";
             
                       dsa += "(< (+ x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += " 1) ";
             
                       dsa += "(> (+ x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += mpy.str();
                       dsa += " )";
             
                   }

                }

                dcount+=1;
            } 

  
            funbody += dsa ;
  
            funbody += "\n\n) (- ";


            funbody += rng.str();
            funbody += " 1) ";
            funbody += "\n\n";

        }

        defi ++;
        
        
        std::stringstream snsrange_Var;
        snsrange_Var << Range;

        ret += funbody.c_str();
        ret += " ";
        ret += snsrange_Var.str();

        for(int j = 0;j<=Range;j++)
        {

           ret += ")";

        } 

        ret += "  )\n\n)\n\n";


    }           
  
    ret += "\n\n";


    if(defi == 1)
    {


        std::string funproto = "(define-fun det_down(";
     
      
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
             std::stringstream cp ;
             cp << count_proto;

             std::string Proto = "x!" + cp.str();
             
             funproto += " (";
             funproto += Proto;
             funproto += " Int)";


             count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;


        std::string funbody = "";


        int i;


        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string check_outside = "";

        for(i=1;i<=2*numBlocksRobo;i++)
        {
             std::stringstream xIndex;
             xIndex << i;
             
             check_outside += "(< x!";
             check_outside += xIndex.str();
             check_outside += " 1) ";

             check_outside += "(> x!";
             check_outside += xIndex.str();
             check_outside += " ";
             check_outside += mpx.str();
             check_outside += ")";
        }



        funbody += "(ite ( or ";
        funbody += check_outside.c_str();
        funbody += " ) -1 \n\n" ;


        for(i= 0; i<= Range ; i++ )
        {
 
            std::stringstream rng ;

            if (i==0)
              rng << 0;
            else
              rng << i;



            funbody += "(ite ( or ";
      
            funbody += "\n      ";

            int dcount = 1;
            std::string dsa = "";

            while(dcount <= k)
            {

                std::stringstream da ;
                da << dcount;
                 
                std::stringstream db ;
                db << dcount + 1;
              
                for(int all_i=1;all_i<=2*numBlocksRobo;all_i+=2)
                { 
                    std::stringstream temp_i;
                    std::stringstream temp_i_plus_one;

                    temp_i << all_i;
                    temp_i_plus_one << all_i+1;

                    dsa += "\n    (and ";
                    dsa += "(= ";
                    dsa += "x!";
                    dsa += temp_i.str();
                    dsa += " ";

                    dsa += "(first o" + da.str();
                    dsa += ")) ";
           
                    dsa +=  " (= (second o";
                    dsa += da.str();
                    dsa += ")   (- x!";
                    dsa += temp_i_plus_one.str();
                    dsa += " ";
                    dsa += rng.str(); 
                    dsa += "))" ;
                    dsa += " )";
           
                }


                dsa += "\n";




                for(int temp_i=1;temp_i<=2*numBlocksRobo;temp_i+=2)
                { 

                   std::stringstream temp_all_i;
                   std::stringstream temp_all_i_plus_one;
                   
                   temp_all_i << temp_i;
                   temp_all_i_plus_one << temp_i+1;


                   if(dcount ==1)
                   {    
                          // Check Outside Map

                    
                       dsa += "\n    (<  ";
                       dsa += "x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += " 1";
                       dsa += ") ";
                       dsa += "(>  ";
                       dsa += "x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += mpx.str();
                       dsa += ") ";
             
                       dsa += "(< (- x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += " 1) ";
                         
                       dsa += "(> (- x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += mpy.str();
                       dsa += " )";
             
                    }

                }
 
                dcount+=1;
            } 


            //   fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );

           funbody += dsa ;

           funbody += "\n\n) (- ";


           funbody += rng.str();
           funbody += " 1) ";

           funbody += "\n\n";

        }

        defi ++;
      
        std::stringstream snsrange_Var;
        snsrange_Var << Range;

        ret += funbody.c_str();
        ret += " ";
        ret += snsrange_Var.str();
            

        for(int j = 0;j<=Range;j++)
        {

           ret += ")";

        } 

        ret += "  )\n\n)\n\n";


    }           
 
    ret +="\n\n";

  

    defi = 0; // reset

    if(defi == 0)
    {


        std::string funproto = "(define-fun det_right(";
           
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
             
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";


            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret +=  funproto.c_str() ;


        std::string funbody = "";

        int i;


        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string check_outside = "";

        for(i=1;i<=2*numBlocksRobo;i++)
        {
            std::stringstream xIndex;
            xIndex << i;
             
            check_outside += "(< x!";
            check_outside += xIndex.str();
            check_outside += " 1) ";

            check_outside += "(> x!";
            check_outside += xIndex.str();
            check_outside += " ";
            check_outside += mpx.str();
            check_outside += ")";
        }



        funbody += "(ite ( or ";
        funbody += check_outside.c_str();
        funbody += " ) -1 \n\n" ;

        for(i= 0; i<= Range ; i++ )
        {

 
            std::stringstream rng ;


            if (i==0)
              rng << 0;
            else
              rng << i;


            funbody += "(ite ( or ";
 
            funbody += "\n      ";



           int dcount = 1;
           std::string dsa = "";

           while(dcount <= k)
           {

               std::stringstream da ;
               da << dcount;
                 
               std::stringstream db ;
               db << dcount + 1;
              
               for(int all_i=1;all_i<=2*numBlocksRobo;all_i+=2)
               { 
                    std::stringstream temp_i;
                    std::stringstream temp_i_plus_one;

                    temp_i << all_i;
                    temp_i_plus_one << all_i+1;

                    dsa += "\n    (and ";
                    dsa += "(= ";
                    dsa += "x!";
                    dsa += temp_i_plus_one.str();
                    dsa += " ";

                    dsa += "(second o" + da.str();
                    dsa += ")) ";
              
                    dsa +=  " (= (first o";
                    dsa += da.str();
                    dsa += ")   (+ x!";
                    dsa += temp_i.str();
                    dsa += " ";
                    dsa += rng.str(); 
                    dsa += "))" ;
                    dsa += " )";
 
                }


                dsa += "\n";
 
                for(int temp_i=1;temp_i<=2*numBlocksRobo;temp_i+=2)
                { 

                   std::stringstream temp_all_i;
                   std::stringstream temp_all_i_plus_one;
                   
                   temp_all_i << temp_i;
                   temp_all_i_plus_one << temp_i+1;


                   if(dcount ==1)
                   {           // Check Outside Map

                       //dsa += " (and ";
                       dsa += "\n    (<  ";
                       dsa += "x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += " 1";
                       dsa += ") ";
                       dsa += "(>  ";
                       dsa += "x!";
                       dsa += temp_all_i_plus_one.str();
                       dsa += " ";
                       dsa += mpx.str();
                       dsa += ") ";
             
                       dsa += "(< (+ x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += " 1) ";
             
                       dsa += "(> (+ x!";
                       dsa += temp_all_i.str();
                       dsa += " ";
                       dsa += rng.str();
                       dsa += ") " ;
                       dsa += mpy.str();
                       dsa += " )";
         
                   }

                }

 
                dcount+=1;
            } 
 
 
            funbody += dsa ;

            funbody += "\n\n) (- ";
 
            funbody += rng.str();
            funbody += " 1) ";
            funbody += "\n\n";
        }

        defi ++;
        
        std::stringstream snsrange_Var;
        snsrange_Var << Range;

        ret += funbody.c_str();
        ret += " ";
        ret += snsrange_Var.str();
            
        for(int j = 0;j<=Range;j++)
        {

            ret += ")";

        } 

        ret += "  )\n\n)\n\n";


    }    

    ret += "\n\n";


    if(defi == 1)
    {


        std::string funproto = "(define-fun det_left(";
     
      
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
             
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;

        std::string funbody = "";
 
        int i;


        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string check_outside = "";
 
        for(i=1;i<=2*numBlocksRobo;i++)
        {
            std::stringstream xIndex;
            xIndex << i;
             
            check_outside += "(< x!";
            check_outside += xIndex.str();
            check_outside += " 1) ";

            check_outside += "(> x!";
            check_outside += xIndex.str();
            check_outside += " ";
            check_outside += mpx.str();
            check_outside += ")";
        }



        funbody += "(ite ( or ";
        funbody += check_outside.c_str();
        funbody += " ) -1 \n\n" ;

        for(i= 0; i<= Range ; i++ )
        {

            std::stringstream rng ;

            if (i==0)
              rng << 0;
            else
              rng << i;



        funbody += "(ite ( or ";
        funbody += "\n      ";

        int dcount = 1;
        std::string dsa = "";

        while(dcount <= k)
        {

            std::stringstream da ;
            da << dcount;
             
            std::stringstream db ;
            db << dcount + 1;
          
            for(int all_i=1;all_i<=2*numBlocksRobo;all_i+=2)
            { 
                std::stringstream temp_i;
                std::stringstream temp_i_plus_one;

                temp_i << all_i;
                temp_i_plus_one << all_i+1;

                dsa += "\n    (and ";
                dsa += "(= ";
                dsa += "x!";
                dsa += temp_i_plus_one.str();
                dsa += " ";

                dsa += "(second o" + da.str();
                dsa += ")) ";
              
                dsa +=  " (= (first o";
                dsa += da.str();
                dsa += ")   (- x!";
                dsa += temp_i.str();
                dsa += " ";
                dsa += rng.str(); 
                dsa += "))" ;
                dsa += " )";

               
            }


            dsa += "\n";
 
            for(int temp_i=1;temp_i<=2*numBlocksRobo;temp_i+=2)
            { 

               std::stringstream temp_all_i;
               std::stringstream temp_all_i_plus_one;
               
               temp_all_i << temp_i;
               temp_all_i_plus_one << temp_i+1;


               if(dcount ==1)
               {           // Check Outside Map

                   //dsa += " (and ";
                   dsa += "\n    (<  ";
                   dsa += "x!";
                   dsa += temp_all_i_plus_one.str();
                   dsa += " ";
                   dsa += " 1";
                   dsa += ") ";
                   dsa += "(>  ";
                   dsa += "x!";
                   dsa += temp_all_i_plus_one.str();
                   dsa += " ";
                   dsa += mpx.str();
                   dsa += ") ";
         
                   dsa += "(< (- x!";
                   dsa += temp_all_i.str();
                   dsa += " ";
                   dsa += rng.str();
                   dsa += ") " ;
                   dsa += " 1) ";
     
                   dsa += "(> (- x!";
                   dsa += temp_all_i.str();
                   dsa += " ";
                   dsa += rng.str();
                   dsa += ") " ;
                   dsa += mpy.str();
                   dsa += " )";
         
                }

            }
  
            dcount+=1;
        } 


       funbody += dsa ;
       funbody += "\n\n) (- ";

       funbody += rng.str();
       funbody += " 1) ";
       funbody += "\n\n";

    }

    defi ++;
    std::stringstream snsrange_Var;
    snsrange_Var << Range;

    ret += funbody.c_str();
    ret += " ";
    ret += snsrange_Var.str();
            
    for(int j = 0;j<=Range;j++)
    {

        ret += ")";

    } 

    ret += "  )\n\n)\n\n";

    }           
 
    ret += "\n\n";



// ################################   Check Block Status Function

    defi=0;
    if(defi == 0)
    {


        std::string funproto = "(define-fun check_block_status(";
     
      
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
             
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";


            count_proto ++;
        }


        funproto += "  ) String \n\n\n";

        ret += funproto.c_str() ;

        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string funbody = "";

        funbody += "(ite ( or ";
        

        for(int tempIndexing= 1;tempIndexing <= 2*numBlocksRobo;tempIndexing+=2) 
        {  

            std::stringstream Ind_api;
            std::stringstream Ind_api_next;
              
            Ind_api << tempIndexing;
            Ind_api_next << tempIndexing+1;
               

            funbody += "\n    (< x!";
            funbody += Ind_api.str();
            funbody += " ";
            funbody += "1 ) (> x!";
            funbody += Ind_api.str();
            funbody += " ";
            funbody += mpx.str();
            funbody += ") (< ";
            funbody += " x!";
            funbody += Ind_api_next.str();
            funbody += " ";
            
            funbody += "1) (> ";
            funbody += " x!";
            funbody += Ind_api_next.str();
            funbody += " ";
            
            funbody += mpy.str();
            funbody += " ) ";
        }

        funbody += ") \"occupied\"\n\n";
  
        //int i;
 
        //std::stringstream rng ;
        //rng << Range;

        funbody += "(ite ( or ";

        int dcount = 1;
        std::string dsa = "";

        while(dcount <= k)
        {

            for(int stemp_i=1;stemp_i<=2*numBlocksRobo; stemp_i+=2)
            {
               std::stringstream da ;
               da << dcount;
                 
               std::stringstream sti;
               std::stringstream stj;
                
               sti << stemp_i;
               stj << stemp_i + 1;
              

               dsa += "\n    (and ";
               dsa += "(= ";
               dsa += "x!";
               dsa += sti.str();
               dsa += " ";
               dsa += "(first o" + da.str();

               dsa += ")) ";

               dsa +=  " (= (second o";
               dsa += da.str();
               dsa += ")  x!";
               dsa += stj.str();
               dsa += " ";

               dsa += ")" ;
               dsa += "  )";

           }


           dcount+=1;
        }   

 
       funbody += dsa ;
       funbody += " ) \"occupied\" \"free\"   ";
       funbody += "\n\n";

       defi ++;
       ret += funbody.c_str();
            
    }           
 
    ret +=  "))\n)\n\n";
   
    return ret;
 

}



