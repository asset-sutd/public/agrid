
#include "utilities.hh"

using namespace SMT_extended;

Parameters * SMT_extended::parseCmdLine( int argc, char * argv[] )
{
    Parameters * parameters = new Parameters();

    opterr = 0;
    int c;

    while(( c = getopt(argc, argv, "Td:MU:P:p:q:B:O:s:l:x:y:c:z:m:t:Vo:")) != -1 )
    {
        switch(c)
        {
            case 'T':
                parameters->use_smt = true;                
                break;
            case 'd':
                {
                std::string param = std::string(optarg);
                interpretSize(optarg, parameters);
                }
                break;
            case 'M':
                parameters->model_scenario = 'P';
                break;                
            case 'U':
                parameters->model_delays_uncertanities = 'Y';
                break;                
            case 'P':
                parameters->sensor_prop_delays = atoi(optarg);
                break;                
            case 'p':
                parameters->min_phys_counter = atoi(optarg);
                break;                
            case 'q':
                parameters->max_phys_counter = atoi(optarg);
                break;                
            case 'B':
                parameters->time_bound = atoi(optarg);
                break;                
            case 'O':
                parameters->obstacles_list = std::string(optarg);
                break;
            case 's':
                parameters->sensors = std::string(optarg);
                break;
            case 'l':
                parameters->latency = atoi(optarg);
                break;
            case 'x':
                parameters->sx = atoi(optarg);
                break;
            case 'y':
                parameters->sy = atoi(optarg);
                break;
            case 'c':
                parameters->controller = std::string(optarg);
                break;
            case 'z':
                parameters->robotsize = atoi(optarg);
                break;                             
            case 'm':
                parameters->mission = std::string(optarg);
                break;
            case 't':
                parameters->time = atoi(optarg);
                break;
            case 'o':
                parameters->fileOut = "./demo/demo_output/" + std::string(optarg);
                break;
            case 'V':
                parameters->verbose = true;
                break;
            default:
                printHelp();
                exit(-1);
        }
    }
    return parameters;
}

void SMT_extended::printMessage( 
        Parameters * p, std::string msg, bool colored )
{
    if( p->verbose )
    {
        if( colored )
        {
            std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
        }
        else
        {
            std::cout << msg << std::endl;
        }
    }
}

void SMT_extended::printError( std::string msg, bool colored )
{
    if( colored )
    {
        std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
    }
    else
    {
        std::cout << msg << std::endl;
    }
}

void SMT_extended::printWarning( 
        Parameters * p, std::string msg, bool colored )
{
    if( p->verbose )
    {
        if( colored )
        {
            std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
        }
        else
        {
            std::cout << msg << std::endl;
        }
    }
}

void SMT_extended::printHelp()
{
    std::cerr << "[USAGE]\n" <<
        "agrid [-T] [-d size] [-x starting width] [-y starting height]" 
        << " [-O obstacles] [-s sensor type] [-c control sw file] [-m mission file] [-V] [-o output file]"
        << std::endl
        << "\t-T enables the use of SMT rather than Bounded Model Checking" 
        << std::endl
        << "\tSize is expressed using the [width]X[height] syntax, where width and height are two positive integers."
        << std::endl
        << "\tObstacles are expressed using the following syntax: w1Eh1Aw2Eh2 ... wnEhn" << std::endl 
        << "\t\tEach letter E separates width and height, each A separates different obsactles." << std::endl
        << "\tSensors may be: standard, proximity, or radar. The type is followed by the length of the sensor:" << std::endl
        << "\t\tfor instance, proximity2 indicates a proximity sensor of length 2."
        << std::endl;
}


void SMT_extended::interpretSize( std::string arg, Parameters * p )
{
    size_t X = arg.find('X');
    std::string ww = arg.substr(0,X);
    std::string hh = arg.substr(X+1);

    if( X == std::string::npos )
    {
        printHelp();
        exit(-1);
    }

    if(
            ww.find_first_not_of("0123456789") != std::string::npos ||
            hh.find_first_not_of("0123456789") != std::string::npos
      )
    {
        printHelp();
        exit(-1);
    }

    p->width = atoi( ww.c_str() );
    p->height = atoi( hh.c_str() );

}

