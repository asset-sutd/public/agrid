; ##### Robot Software - Melay Machines ( FSM With Output)
     
(define-fun Mel ( (vel Int) (prevCommand String) (s1 Bool) (s2 Int) (s3 Int) (s4 Int) (s5 Int)   (pos_x Int) (pos_y Int) (targ_x Int) (targ_y Int)) String   

(ite ( and (>= vel 1)  (not(= prevCommand "stop" )) (= s2 1 ) (> targ_y pos_y) )  "stop" 

(ite (and  (>= vel 1)  (not(= prevCommand "stop" )) (= s3 1 ) (< targ_y pos_y) ) "stop"

(ite (and  (>= vel 1)  (not(= prevCommand "stop" ))  (= s4 1 ) (> targ_x pos_x)   ) "stop"

(ite (and  (>= vel 1)  (not(= prevCommand "stop" ))  (= s5 1 ) (< targ_x pos_x)   ) "stop"

(ite ( and (not(= prevCommand "down" )) (>= s2 1) (> targ_y pos_y) )  "up" 
(ite ( and (not(= prevCommand "left" )) (>= s4 1) (>= targ_x pos_x) (> targ_y pos_y) )  "right" 
(ite ( and (not(= prevCommand "right" )) (>= s5 1) (<= targ_x pos_x) (> targ_y pos_y) )  "left" 
     
(ite (and  (not(= prevCommand "up" ))  (>= s3 1) (< targ_y pos_y) ) "down"
(ite ( and (not(= prevCommand "left" )) (>= s4 1) (>= targ_x pos_x) (< targ_y pos_y) )  "right" 
(ite ( and (not(= prevCommand "right" )) (>= s5 1) (<= targ_x pos_x) (< targ_y pos_y) )  "left" 

(ite (and (not(= prevCommand "left" ))  (>= s4 1)  (> targ_x pos_x)   ) "right"
(ite (and (not(= prevCommand "down" ))  (>= s2 1) (>= targ_y pos_y)  (> targ_x pos_x)   ) "up"
(ite (and (not(= prevCommand "up" ))  (>= s3 1) (<= targ_y pos_y) (> targ_x pos_x)   ) "down"

(ite (and (not(= prevCommand "right" ))  (>= s5 1)  (< targ_x pos_x)   ) "left"
(ite (and (not(= prevCommand "down" ))  (>= s2 1) (>= targ_y pos_y) (< targ_x pos_x)   ) "up"
(ite (and (not(= prevCommand "up" ))  (>= s3 1) (<= targ_y pos_y) (< targ_x pos_x)   ) "down"  "null"  
 
))) ))) )))  )))  ))))
 
)
 