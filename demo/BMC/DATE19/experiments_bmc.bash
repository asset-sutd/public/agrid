#!/bin/bash

mkdir -p $1
mkdir -p $2
echo "size,type,length,latency,counter,time" > $1/results.csv

###########################################################################################
# Generate experiment varying the size of the matrix. Standard Sensors.
###########################################################################################

for i in {4..50}
do
    ./roboverify -d "$i"X$i -x 0 -y 0 -c ../specs/controller_standard_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -t 10 -o \
        $1/NO_LATENCY_"$i"X"$i"_standard.smv

    echo $i,standard,2,0,10,%e >> $2/log.txt

    /usr/bin/time -f"$i,standard,2,0,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/NO_LATENCY_"$i"X"$i"_standard.smv > /dev/null 2>> $2/log.txt
done

echo "Subject: Batch 1 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the size of the matrix. Standard Sensors." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_1.csv

###########################################################################################
# Generate experiment varying the size of the matrix. Proximity Sensors.
###########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv

for i in {4..50}
do
    ./roboverify -d "$i"X$i -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -s proximity2 -t 10 -o \
        $1/current.smv

    echo $i,proximity,2,0,10,%e >> $2/log.txt

    /usr/bin/time -f"$i,proximity,2,0,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/current.smv > /dev/null 2>> $2/log.txt
done

echo "Subject: Batch 2 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the size of the matrix. Proximity Sensors." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_2.csv

###########################################################################################
# Generate experiment varying the size of the matrix. Radar.
###########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv

for i in {4..50}
do
    ./roboverify -d "$i"X$i -x 0 -y 0 -c ../specs/controller_radar_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -s radar2 -t 10 -o \
        $1/current.smv

    echo $i,radar,2,0,10,%e >> $2/log.txt

    /usr/bin/time -f"$i,radar,2,0,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/current.smv > /dev/null 2>> $2/log.txt
done

echo "Subject: Batch 3 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the size of the matrix. Radar." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_3.csv

###########################################################################################
# Generate experiments varying the distance sensed by sensors. Proximity
###########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv

for i in {2..10}
do
    ./roboverify -d 50X50 -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 \
        -s proximity$i -t 10 \
        -o $1/current.smv

     /usr/bin/time -f"50,proximity,$i,0,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/current.smv > /dev/null 2>> $2/log.txt       
done

echo "Subject: Batch 4 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the length of the proximity sensor." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_4.csv

###########################################################################################
# Generate experiments varying the distance sensed by sensors. Radar
###########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv

for i in {2..10}
do
    ./roboverify -d 50X50 -x 0 -y 0 -c ../specs/controller_radar_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 \
        -s radar$i -t 10 \
        -o $1/current.smv

     /usr/bin/time -f"50,radar,$i,0,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/current.smv > /dev/null 2>> $2/log.txt
done

echo "Subject: Batch 5 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the length of the radar." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_5.csv

##########################################################################################
# Generate experiments varying the latency of the actuators.
##########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv

for i in {1..5}
do
    ./roboverify -d 50X50 -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 \
        -s proximity5 -l $i -t 10 \
        -o $1/current.smv

     /usr/bin/time -f"50,proximity,5,$i,10,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length 11 \
        $1/current.smv > /dev/null 2>> $2/log.txt       
done

echo "Subject: Batch 6 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the latency of the actuators." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_6.csv

##########################################################################################
# Generate experiments varying the counter lenght.
##########################################################################################

echo "size,type,length,latency,counter,time" > $1/results.csv
for i in {10..2..30}
do
    let bound=$i+1
     ./roboverify -d 50X50 -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 \
        -s proximity5 -t $i \
        -o $1/current.smv
   
     /usr/bin/time -f"50,proximity,5,0,$i,%e" -a -o $1/results.csv nusmv -dcx \
        -bmc -bmc_length $bound \
        $1/current.smv > /dev/null 2>> $2/log.txt       
done

echo "Subject: Batch 7 ended" > $1/mail.txt
echo >> $1/mail.txt
echo "Experiment varying the depth of the counters." >> $1/mail.txt
 
date >> $1/mail.txt
echo >> $1/mail.txt
echo >> $1/mail.txt

cat $1/results.csv >> $1/mail.txt
ssmtp lora.mik@gmail.com < $1/mail.txt
mv $1/results.csv $2/batch_7.csv

