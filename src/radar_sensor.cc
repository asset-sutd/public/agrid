
#include "smt_printer.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace roboverify;
std:: string SMTPrinter::_computeSensorValuesRadar(int radar_range)
{
   
    std::string ret = "";
    int defi = 0;
 
    if(defi == 0)
    {


        std::string funproto = "(define-fun det_radar( (x!0 String) ";
  
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }

        funproto += "  ) Bool \n";

        ret += funproto.c_str();


        ret += "(ite (= true ( or (<  x!1 1  ) (>  x!1 ";
        ret += MapWidth.str();
        ret += "  )   (<  x!2 1  ) (>  x!2 ";
        ret += MapHeight.str();
        ret += "  )   ) ) false";  

        int scount = 1;

        std::string sob_info = "";

        if(k >= 1 || numRobos > 1  )
        {

            ret += "\n\n\n(ite (= false (or ";

            for(int ti=-1*radar_range;ti<= radar_range ;ti++)
            {
  
                for(int tj=-1*radar_range;tj<= radar_range ;tj++)
                {
             
                    if(abs(ti) + abs(tj) <= radar_range)
                    { 
              
                      scount = 1;
                      int printonce = 0;

                      while(scount <= k)
                      {
           
                          std::stringstream sc ;
                          sc << scount;

                          std::stringstream rnge ;
                          rnge << radar_range;
           
                          sob_info += "\n\n        (and ";
   
                          sob_info += "(= (first o" ;
                          sob_info += sc.str();
                          sob_info += ") ";
       
                          if(ti < 0)
                              sob_info += "(- x!1 ";
                          else
                              sob_info += "(+ x!1 " ;


                          std::stringstream ic;
                          ic << abs(ti);

                          std::stringstream jc;
                          jc << abs(tj);
                            
                          sob_info += ic.str();
                          sob_info += ")) ";
          
                          sob_info += "(= (second o" ;
                          sob_info += sc.str();
                          sob_info += ") ";
               
                          if(tj < 0)
                              sob_info += "(- x!2 ";
                          else
                              sob_info += "(+ x!2 " ;

                          sob_info += jc.str();
                          sob_info += ")) ) ";
                    
                          if( printonce == 0) {
                              // Check within Map  
                              sob_info += "\n        (or ";
                    
                              // clause
                              sob_info += "(< ";
      
                              if(ti < 0)
                                  sob_info += "(- x!1 ";
                              else
                                  sob_info += "(+ x!1 "  ;

                          
                              sob_info += ic.str();
                              sob_info += ") ";

                              std::stringstream mapsizex ;     
                              std::stringstream mapsizey ;     
                           
                              mapsizex << mapx;
                              mapsizey << mapy;

                              sob_info += " 1)"; 

                              sob_info += " (< " ;

                              if(tj < 0)
                                  sob_info += "(- x!2 ";
                              else
                                  sob_info += "(+ x!2 "  ;
      
                              sob_info += jc.str();
                              sob_info += ") ";
                      
                              // sob_info += mapsizey.str();
                              sob_info += " 1)"; 

                              sob_info += " (> ";

 
                              if(ti < 0)
                                  sob_info += "(- x!1 ";
                              else
                                  sob_info += "(+ x!1 "  ;

                         
                              sob_info += ic.str();
                              sob_info += ") ";

                              sob_info += mapsizex.str();
                              sob_info += ")"; 
 
                              sob_info += " (> " ;

                              if(tj < 0)
                                  sob_info += "(- x!2 ";
                              else
                                  sob_info += "(+ x!2 "  ;

      
                              sob_info += jc.str();
                              sob_info += ") ";
                      
                              sob_info += mapsizey.str();
                              sob_info += "))"; 
                    
                              printonce++;
                          }   

                          scount++;

                      }


                      std::string dsa = "" ; 
                      std::string dsb = "" ;
   
                      int dcount = 3;
                      sob_info += "";

                      while(dcount <= 2 * numRobos)
                      {

                          std::stringstream da ;
                          da << dcount;

                          std::stringstream db ;
                          db << dcount+1;

                          std::stringstream rnge ;
                          rnge << radar_range;


                          sob_info += "\n        (and ";
                          sob_info += "(= x!" ;
                          sob_info += da.str();
                   

                          if(ti < 0)
                              sob_info += " (- x!1 ";
                          else
                              sob_info += " (+ x!1 "  ;


                          std::stringstream ic;
                          ic << abs(ti);

                          std::stringstream jc;
                          jc << abs(tj);
                                       
                          sob_info += ic.str();
                          sob_info += ")) ";
                                
              
                          sob_info += "(= x!" ;
                          sob_info += db.str();
                              
                          if(tj < 0)
                              sob_info += " (- x!2 ";
                          else
                              sob_info += " (+ x!2 "  ;
                  

                          sob_info += jc.str();
                          sob_info += ")) ) ";
                              
                          dcount+=2;
                      }

 
                  } // I, j, loop
              }
          }  

          ret += "\n\n ";
          ret += sob_info.c_str();
          ret += "\n\n) ) true false ))\n\n)  \n\n";

      }
       
      else
          ret += "\n\n\n false )\n\n)  \n\n ";
          
    
      ret += "\n";   
      defi ++;
    
         
  }



    // Up Sensor In case Radar detects obstacle 

    if(defi == 1)
    {


        std::string funproto = "(define-fun det_radar_up(  ";
   
        int count_proto = 1;
 
        while(count_proto <= 2*numRobos)
        {
             std::stringstream cp ;
             cp << count_proto;

             std::string Proto = "x!" + cp.str();
         
	         funproto += " (";
	         funproto += Proto;
	         funproto += " Int)";

             count_proto ++;
        }

        funproto += "  ) Bool \n"; 

        ret += funproto.c_str() ;


        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   


        ret += "\n     (ite (and  \n";

        int scount = 1;

        std::string sob_info = "";
 
        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!1 (first ";
            sob_info += Onum;
            sob_info += ") ) (= (+ 1 x!2) (second ";
            sob_info += Onum;
            sob_info += ")  )  ))";
          
            scount++;

            ret += sob_info.c_str();

        }


        ret += "\n\n";
        scount=1;
        while(scount <= k)
        {
             ret += sob_info = "";
             std::stringstream sc ;
             sc << scount;

             std::string Onum = "o" + sc.str();

             sob_info += "\n         (not(and (= x!1 (first ";
             sob_info += Onum;
             sob_info += ") ) (= x!2 (second ";
             sob_info += Onum;
             sob_info += ")  )  ))";
          
             scount++;
             ret += sob_info.c_str();

        }


        ret += "\n\n         (and (>= x!1 1 ) (<= x!1 mapX) (>= (+ 1 x!2) 1  ) (<= (+ 1 x!2) mapY  )     )";
 
        std::string dsa = "" ; 
        std::string dsb = "" ;
   
        int dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount;

            std::stringstream db ;
            db << dcount+1;

            std::string Da ;
            Da = "";

            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "x!" + da.str();

            dsa += ") ";
 
            dsa += "(= ";
            dsa += "(+ x!2 1) ";

            dsa += "x!" + db.str();

            dsa += "))";

            dcount+=2;
        } 


        if(numRobos > 1) 
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";
        }  

        dsa = "" ; 
        dsb = "" ;
   
        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount;

            std::stringstream db ;
            db << dcount+1;

            std::string Da ;
            Da = "";

            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "x!" + da.str();

            dsa += ") ";

            dsa += "(= ";
            dsa += " x!2  ";
            dsa += "x!" + db.str();
            dsa += "))";

            dcount+=2;
        } 


        if(numRobos > 1)
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";

        }     

          
        ret += "         )              true false)) \n\n)\n\n";

    }

    defi ++;
 
    // Down Sensor 
    if(defi == 2)
    {


        std::string funproto = "(define-fun det_radar_down(  ";
 
        int count_proto = 1;
 
        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }

        funproto += "  ) Bool \n"; 

        ret += funproto.c_str() ;

        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";
        ret += "\n     (ite (and  \n";

        int scount = 1;

        std::string sob_info = "";

        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!1 (first ";
            sob_info += Onum;
            sob_info += ") ) (= (- x!2 1) (second ";
            sob_info += Onum;
            sob_info += ")  )  ))";
          
            scount++;
            ret += sob_info.c_str();
        }


        ret += "\n\n";
        scount=1;
        
        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!1 (first ";
            sob_info += Onum;
            sob_info += ") ) (= x!2 (second ";
            sob_info += Onum;
            sob_info += ")  )  ))";
          
            scount++;

            ret += sob_info.c_str();

         }


         ret += "\n\n         (and (>= x!1 1 ) (<= x!1 mapX) (>= (- x!2 1) 1  ) (<= (- x!2 1) mapY  )     )  ";

         std::string dsa = "" ; 
         std::string dsb = "" ;
   
         int dcount = 3;

         while(dcount <= 2*numRobos)
         {
             std::stringstream da ;
             da << dcount;

             std::stringstream db ;
             db << dcount+1;

             std::string Da ;
             Da = "";

             dsa += " (and ";
             dsa += "(= ";
             dsa += "x!1 ";
             dsa += "x!" + da.str();

             dsa += ") ";
             dsa += "(= ";
             dsa += "(- x!2 1) ";
             dsa += "x!" + db.str();
             dsa += "))";

             dcount+=2;
          }  


          if(numRobos > 1)
          {
              ret += "\n         (not (= true (or ";
              ret += dsa.c_str();
              ret += " ) )) ";

          }   

          dsa = "" ; 
          dsb = "" ;
          dcount = 3;

          while(dcount <= 2*numRobos)
          {
              std::stringstream da ;
              da << dcount;

              std::stringstream db ;
              db << dcount+1;

              std::string Da ;
              Da = "";

              dsa += " (and ";
              dsa += "(= ";
              dsa += "x!1 ";
              dsa += "x!" + da.str();

              dsa += ") ";
              dsa += "(= ";
              dsa += " x!2  ";
              dsa += "x!" + db.str();
              dsa += "))";

              dcount+=2;
          } 

 
          if(numRobos > 1)
          {
              ret += "\n         (not (= true (or ";
              ret += dsa.c_str();
              ret += " ) )) ";

          }   

        ret += "         )              true false)) \n\n)\n\n";

    }

    defi ++;
  

    //********** Right Sensor

    defi = 1;

    if(defi == 1)
    {


        std::string funproto = "(define-fun det_radar_right(  ";
    
        int count_proto = 1;
 
        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }

        funproto += "  ) Bool \n"; 

        ret += funproto.c_str() ;
        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   


        ret += "\n     (ite (and  \n";

        int scount = 1;
        
        std::string sob_info = "";

        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!2 (second ";
            sob_info += Onum;
            sob_info += ") ) (= (+ 1 x!1) (first ";
            sob_info += Onum;
            sob_info += ")  )  ))";
            scount++;

            ret += sob_info.c_str();

        }

 
        ret += "\n\n";
        scount=1;
        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!2 (second ";
            sob_info += Onum;
            sob_info += ") ) (= x!1 (first ";
            sob_info += Onum;
            sob_info += ")  )  ))";
            scount++;

            ret += sob_info.c_str();

        }

     
        ret += "\n\n         (and (>= x!2 1 ) (<= x!2 mapY) (>= (+ 1 x!1) 1  ) (<= (+ 1 x!1) mapX  )     )";

 
        std::string dsa = "" ; 
        std::string dsb = "" ;
   
        int dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount+1;

            std::stringstream db ;
            db << dcount;

            std::string Da ;
            Da = "";

            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!2 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa += "(= ";
            dsa += "(+ x!1 1) ";
            dsa += "x!" + db.str();
            dsa += "))";

            dcount+=2;
        } 


        if(numRobos > 1)
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";

        }   

        dsa = "" ; 
        dsb = "" ;
   
        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount+1;


            std::stringstream db ;
            db << dcount;
 
            std::string Da ;
            Da = "";

            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!2 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa += "(= ";
            dsa += " x!1  ";
            dsa += "x!" + db.str();
            dsa += "))";

            dcount+=2;
        } 


        if(numRobos > 1)
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";

        }   

          
        ret += "         )              true false)) \n\n)\n\n";

    }

    defi ++;
 

    // Left Sensor
    if(defi == 2)
    {


        std::string funproto = "(define-fun det_radar_left(  ";
 
        int count_proto = 1;
        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }

        funproto += "  ) Bool \n"; 

        ret += funproto.c_str();
        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   

  
        ret += "\n     (ite (and  \n";
        int scount = 1;
        std::string sob_info = "";
    
        while(scount <= k)
        {
            sob_info = "";
            std::stringstream sc ;
            sc << scount;

            std::string Onum = "o" + sc.str();

            sob_info += "\n         (not(and (= x!2 (second ";
            sob_info += Onum;
            sob_info += ") ) (= (- x!1 1) (first ";
            sob_info += Onum;
            sob_info += ")  )  ))";
          
            scount++;

            ret += sob_info.c_str();

        }


      ret += "\n\n";
      scount=1;
      while(scount <= k)
      {
          sob_info = "";
          std::stringstream sc ;
          sc << scount;

          std::string Onum = "o" + sc.str();

          sob_info += "\n         (not(and (= x!2 (second ";
          sob_info += Onum;
          sob_info += ") ) (= x!1 (first ";
          sob_info += Onum;
          sob_info += ")  )  ))";
          
          scount++;

          ret += sob_info.c_str();

       }

    
       ret += "\n\n         (and (>= x!2 1 ) (<= x!2 mapY) (>= (- x!1 1) 1  ) (<= (- x!1 1) mapX  )     )  ";

  
       std::string dsa = "" ; 
       std::string dsb = "" ;
   
       int dcount = 3;
       while(dcount <= 2*numRobos)
       {
           std::stringstream da ;
           da << dcount+1;

           std::stringstream db ;
           db << dcount;

           std::string Da ;
           Da = "";

           dsa += " (and ";
           dsa += "(= ";
           dsa += "x!2 ";
           dsa += "x!" + da.str();

           dsa += ") ";
           dsa += "(= ";
           dsa += "(- x!1 1) ";
           dsa += "x!" + db.str();
           dsa += "))";

           dcount+=2;
        } 

        if(numRobos > 1)
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";

        }   

        dsa = "" ; 
        dsb = "" ;
    
        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount+1;

            std::stringstream db ;
            db << dcount;

            std::string Da ;
            Da = "";

            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!2 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa += "(= ";
            dsa += " x!1  ";
            dsa += "x!" + db.str();
            dsa += "))";
      
            dcount+=2;
        } 

        if(numRobos > 1)
        {
            ret += "\n         (not (= true (or ";
            ret += dsa.c_str();
            ret += " ) )) ";

        }   

        ret += "         )              true false)) \n\n)\n\n";

    }

    defi ++;
  
    ret += "\n\n";


    // ################################    Misc. Check Block Status Function ###################################

    defi=0;
    if(defi == 0)
    {


        std::string funproto = "(define-fun check_block_status(";
 
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) String \n\n\n";
  
        ret += funproto.c_str() ;

        std::stringstream mpx ;
        std:: stringstream mpy ;
	    mpx << mapx;
	    mpy << mapy;


        std::string funbody = "";

        funbody += "(ite ( or (< x!1 1 ) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< ";
        funbody += " x!2 1  ) (> ";
        funbody += " x!2 ";
        funbody += mpy.str();
        funbody += " ) ) \"occupied\"\n\n";

        // int i;

        std::stringstream rng ;

        funbody += "(ite ( or ";

        int dcount = 1;
        std::string dsa = "";

        while(dcount <= k)
        {
            std::stringstream da ;

            da << dcount;
              
            std::stringstream db ;
            db << dcount + 1;

                    
            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "(first o" + da.str();

            dsa += ")) ";

            dsa +=  " (= (second o";
            dsa += da.str();
            dsa += ")  x!2 ";
            dsa += ")" ;

            dsa += " )";
            dcount+=1;
        } 


        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount;

            std::stringstream db ;
            db << dcount+1;

           // std::stringstream temprange ;
          //  temprange << i;



            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa +=  " (=  x!"; 
       
            dsa += db.str();
            dsa +=  " x!2 ";   
             
            dsa += "))";
      
            dcount+=2;
        } 


       funbody += dsa ;

       funbody += " ) \"occupied\" \"free\"   ";


       funbody += "\n\n";


       defi ++;
       ret += funbody.c_str();
            
       for(int j = 1;j<= 2+numRobos-1;j++) // 1 for k obstacles, numRobos, 1 for extra ite
           ret += ")";
       

    }           
 

    ret += ")\n\n";

    return ret;

}





