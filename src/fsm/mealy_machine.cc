/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/fsm/mealy_machine.cc
 * @brief   Implementation for the Mealy Machine models.
 */

#include "fsm/mealy_machine.hh"

using namespace roboverify;
using namespace fsm;

MealyState::MealyState( std::string id ) :
    _id(id)
{
}

MealyState::~MealyState()
{
}

bool MealyState::operator==( const MealyState & s )
{
    return (_id == s._id);
}

std::string MealyState::getId()
{
    return _id;
}

void MealyState::setId( std::string id )
{
    _id = id;
}

MealyTransition::MealyTransition(
        MealyState * initial, MealyState * target ) :
    _initial(initial),
    _target(target)
{
}

MealyTransition::~MealyTransition()
{
}

MealyState * MealyTransition::getInitialState()
{
    return _initial;
}

MealyState * MealyTransition::getTargetState()
{
    return _target;
}

void MealyTransition::setInitialState( MealyState * state )
{
    _initial = state;
}

void MealyTransition::setTargetState( MealyState * state )
{
    _target = state;
}

MealyMachine::MealyMachine() :
    _initial(nullptr)
{
}

MealyMachine::~MealyMachine()
{
}

void MealyMachine::setInitialState( MealyState * state )
{
    _initial = state;
}

MealyState * MealyMachine::getInitialState()
{
    return _initial;
}

MealyState * MealyMachine::getStateById( std::string id )
{
    std::set< MealyState * >::iterator it;
    for( it = states.begin(); it != states.end(); ++it )
    {
        MealyState * curr = *it;
        if( id == curr->getId() ) return curr;
    }
    return nullptr;
}

std::string MealyMachine::print()
{
    std::string ret("");

    std::set< MealyTransition * >::iterator it;
    for(it = transitions.begin(); it != transitions.end(); ++it)
    {
        MealyTransition * current = *it;

        ret += current->getInitialState()->getId();
        ret += " -> ";
        ret += current->getTargetState()->getId();
        ret += "\n";
    }
    return ret;
}


