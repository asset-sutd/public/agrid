
#include "smt_printer.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace roboverify;
std:: string SMTPrinter::_computeSensorValuesStandard()
{
   

    std::string ret = "";  
	int defi = 0;

    // Defining the det_up function for determining the sensor value in up direction 
    if(defi == 0)
    {

	    std::string funproto = "(define-fun det_up(";
	    // Function Arguments, writing function prototype  
	    int count_proto = 1;

	    while(count_proto <= 2*numRobos)
	    {
	         std::stringstream cp ;
	         cp << count_proto;
	         std::string Proto = "x!" + cp.str();
	         
	         funproto += " (";
	         funproto += Proto;
	         funproto += " Int)";

	    	 count_proto ++;
	    }


	    funproto += "  ) Bool \n";

        ret += funproto.c_str(); 
        ret +=  "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   

	    // Function Body
	    ret += "\n\n  (ite (and\n";

	    // Static Obstacles Ahead
	    int scount = 1;

	    std::string sob_info = "";

	    while(scount <= k)
	    {
	         
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= (+ 1 x!2) (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         //fprintf(fpz,"%s", sob_info.c_str());
	         ret += sob_info.c_str(); 

	    }

	    
	    scount = 1; // reset
	    sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         //fprintf(fpz,"%s", sob_info.c_str());
	         ret += sob_info.c_str(); 

	    }


	    // Command is valid only if the robot stays within the Map Dimensions upon execution of the command
	    ret += "\n\n      (>= x!1 1 ) (<= x!1 mapX) (>= (+ 1 x!2) 1  ) (<= (+ 1 x!2) mapY  )\n\n\n";


	    // Dynmaic Obstacles 
	    std::string dsa = "" ; 
	    std::string dsb = "" ;
	   
	    int dcount = 3;

	    while(dcount <= 2*numRobos)
	    {
	    	std::stringstream da ;
	        da << dcount;


	        std::stringstream db ;
	        db << dcount+1;

	        std::string Da ;
	        Da = "";

	        dsa += " (and ";
	        dsa += "(= ";
	        dsa += "x!1 ";
	        dsa += "x!" + da.str();

	        dsa += ") ";
	        dsa += "(= ";
	        dsa += "(+ x!2 1) ";
	        dsa += "x!" + db.str();
	        dsa += "))";
	 

	    	dcount+=2;
	    }	

	    
	  //  fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );
          if(numRobos > 1)
          {
              //  fprintf(fpz, "      (not (= true %s )) ",  dsa.c_str()  );
                ret +=  "      (not (= true ";
                ret += dsa.c_str();
                ret += " )) ";

          }	
	        

	    // Robot doesn't move after the crash
	    dsa = "" ; 
	    dsb = "" ;
	   
	    dcount = 3;

	    while(dcount <= 2*numRobos)
	    {
	    	// da for x coordinates, db for y coordinates 
			std::stringstream da ;
	        da << dcount;


	        std::stringstream db ;
	        db << dcount+1;

	        std::string Da ;
	        Da = "";

	        dsa += " (and ";
	        dsa += "(= ";
	        dsa += "x!1 ";
	        dsa += "x!" + da.str();

	        dsa += ") ";
	        dsa += "(= ";
	        dsa += " x!2 ";
	        dsa += "x!" + db.str();
	        dsa += "))";
	       

	        dcount+=2;  // Two Arguments i.e x and y coordinates parsed in one iteration of the loop
	    } 


	  //  fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );
        if(numRobos > 1) 
        {
            //  fprintf(fpz, "      (not (= true %s )) ",  dsa.c_str()  );
              ret += "      (not (= true ";
              ret += dsa.c_str();
              ret += " )) ";

        }   

	    // function return value - true sensor value means move allowed 
	          
	    ret += "  )  true false ))\n)\n\n";

	} // End of if(defi==0) 



    defi ++; // defi flag used to replicate body of above det_up function into det_down function with small modifications.

    ret += "\n\n";

    



    // Defining the det down function




    if(defi == 1)
    {

	    std::string funproto = "(define-fun det_down(";
	  
	    int count_proto = 1;

        // Function Arguments 
	    while(count_proto <= 2*numRobos)
	    {
	         std::stringstream cp ;
	         cp << count_proto;

	         std::string Proto = "x!" + cp.str();
	         
	         funproto += " (";
	         funproto += Proto;
	         funproto += " Int)";


        	 count_proto ++;
	    }


	    funproto += "  ) Bool \n";

	    //fprintf(fpz,"%s", funproto.c_str() );

        ret += funproto.c_str();


//        fprintf(fpz, "\n\n(ite (or (< x!1 1) (> x!1 %d) (< x!2 1) (> x!2 %d) )  false ", mapx, mapy );   

        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   

	    ret += "\n\n  (ite (and\n";

	    int scount = 1;

	    std::string sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= (- x!2 1) (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         //fprintf(fpz,"%s", sob_info.c_str());
             ret += sob_info.c_str();
	    }


	    scount = 1;

	    sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         ret +=  sob_info.c_str();

	    }


	    ret += "\n\n      (>= x!1 1 ) (<= x!1 mapX) (>= (- x!2 1) 1  ) (<= (- x!2 1) mapY  )\n\n";


	    std::string dsa = "" ; 
	    std::string dsb = "" ;
	   
	    int dcount = 3;


	    while(dcount <= 2*numRobos)
	    {
	           std::stringstream da ;
	           da << dcount;


	           std::stringstream db ;
	           db << dcount+1;

	           std::string Da ;
	           Da = "";

	           dsa += " (and ";
	           dsa += "(= ";
	           dsa += "x!1 ";
	           dsa += "x!" + da.str();

	           dsa += ") ";
	           dsa += "(= ";
	           dsa += "(- x!2 1) ";
	           dsa += "x!" + db.str();
	           dsa += "))";

    	       dcount+=2;
		} 	



//	    fprintf(fpz, "\n      (not (= true (or %s ) )) ",  dsa.c_str()  );
		if(numRobos > 1)
     	{
                 // fprintf(fpz, "\n      (not (= true %s )) ",  dsa.c_str()  );
                 ret += "\n      (not (= true ";
                 ret += dsa.c_str();
                 ret += " )) ";
     	}    



	    dsa = "" ; 
	    dsb = "" ;
	   
	    dcount = 3;
	    while(dcount <= 2*numRobos)
	    {
	           std::stringstream da ;
	           da << dcount;

	           std::stringstream db ;
	           db << dcount+1;

	           std::string Da ;
	           Da = "";

	           dsa += " (and ";
	           dsa += "(= ";
	           dsa += "x!1 ";
	           dsa += "x!" + da.str();

	           dsa += ") ";
	           dsa += "(= ";
	           dsa += " x!2 ";
	           dsa += "x!" + db.str();
	           dsa += "))";


	           dcount+=2;
	    } 



//	    fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );
		if(numRobos > 1)
     	{
                 // fprintf(fpz, "\n      (not (= true %s )) ",  dsa.c_str()  );
                 ret += "\n      (not (= true ";
                 ret += dsa.c_str();
                 ret += " )) ";
     	}   


        // Function Return Value i.e. Sensor Value 
	    ret += "   ) true false ) )\n)\n\n";
 
      

    } // End of if(defi==1)  i.e. end of det_down function definition




   defi=0;

   if(defi == 0)
    {


	    std::string funproto = "(define-fun det_right(";
	 
	    // Function Arguments, writing function prototype  
	    int count_proto = 1;

	    while(count_proto <= 2*numRobos)
	    {
	         std::stringstream cp ;
	         cp << count_proto;

	         std::string Proto = "x!" + cp.str();
	         
	         funproto += " (";
	         funproto += Proto;
	         funproto += " Int)";


	    	 count_proto ++;
	    }


	    funproto += "  ) Bool \n";

	    //fprintf(fpz,"%s", funproto.c_str() );
        ret += funproto.c_str();  


        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   


	    // Function Body
	    ret += "\n\n  (ite (and\n";


	    // Static Obstacles Ahead
	    int scount = 1;

	    std::string sob_info = "";

	    while(scount <= k)
	    {
	         
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= (+ 1 x!1) (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         ret += sob_info.c_str();

	    }

	    
	    scount = 1; // reset
	    sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         ret += sob_info.c_str();

	    }


	    // Command is valid only if the robot stays within the Map Dimensions upon execution of the command
	    ret += "\n\n      (>= (+ x!1 1) 1 ) (<= (+ x!1 1) mapX) (>= x!2 1 ) (<= x!2 mapY  )\n\n\n";


	    // Dynmaic Obstacles 
	    std::string dsa = "" ; 
	    std::string dsb = "" ;
	   
	    int dcount = 3;

	    while(dcount <= 2*numRobos)
	    {
	    	std::stringstream da ;
	        da << dcount;


	        std::stringstream db ;
	        db << dcount+1;

	        std::string Da ;
	        Da = "";

	        dsa += " (and ";
	        dsa += "(= ";
	        dsa += "(+ 1 x!1) ";
	        dsa += "x!" + da.str();

	        dsa += ") ";
	        dsa += "(= ";
	        dsa += " x!2 ";
	        dsa += "x!" + db.str();
	        dsa += "))";
	 

	    	dcount+=2;
	    }	

	    
	  //  fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );
          if(numRobos > 1)
          {
               ret += "      (not (= true ";
               ret += dsa.c_str();
               ret += " )) ";

          }	
	        

	    // Robot doesn't move after the crash
	    dsa = "" ; 
	    dsb = "" ;
	   
	    dcount = 3;

	    while(dcount <= 2*numRobos)
	    {
	    	// da for x coordinates, db for y coordinates 
			std::stringstream da ;
	        da << dcount;


	        std::stringstream db ;
	        db << dcount+1;

	        std::string Da ;
	        Da = "";

	        dsa += " (and ";
	        dsa += "(= ";
	        dsa += "x!1 ";
	        dsa += "x!" + da.str();

	        dsa += ") ";
	        dsa += "(= ";
	        dsa += " x!2 ";
	        dsa += "x!" + db.str();
	        dsa += "))";
	       

	        dcount+=2;  // Two Arguments i.e x and y coordinates parsed in one iteration of the loop
	    } 


          if(numRobos > 1)
          {
               ret += "      (not (= true ";
               ret += dsa.c_str();
               ret += " )) ";
               //  fprintf(fpz, "      (not (= true %s )) ",  dsa.c_str()  );

          }		
          
	    // function return value - true sensor value means move allowed 
	          
	    ret += "  )   true false ) )\n)\n\n";

	} // End of if(defi==0) 



    defi ++; // defi flag used to replicate body of above det_up function into det_down function with small modifications.

    /* ##################################################################################################################
       ################################################################################
       ####################################################

	*/         

    // Defining det_down function similar to det_up function 

    ret += "\n\n";

    if(defi == 1)
    {

	    std::string funproto = "(define-fun det_left(";
	  
	    int count_proto = 1;

        // Function Arguments 
	    while(count_proto <= 2*numRobos)
	    {
	         std::stringstream cp ;
	         cp << count_proto;

	         std::string Proto = "x!" + cp.str();
	         
	         funproto += " (";
	         funproto += Proto;
	         funproto += " Int)";


        	 count_proto ++;
	    }


	    funproto += "  ) Bool \n";

	    ret += funproto.c_str();
        
        ret += "\n\n(ite (or (< x!1 1) (> x!1 mapX) (< x!2 1) (> x!2 mapY) )  false ";   


	    ret += "\n\n  (ite (and\n";


	    int scount = 1;

	    std::string sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= (- x!1 1) (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         ret += sob_info.c_str();

	    }


	    scount = 1;

	    sob_info = "";

	    while(scount <= k)
	    {
	         sob_info = "";
	         std::stringstream sc ;
	         sc << scount;

	         std::string Onum = "o" + sc.str();

	         sob_info += "      (not(and (= x!1 (first ";
	         sob_info += Onum;
	         sob_info += ") ) (= x!2 (second ";
	         sob_info += Onum;
	         sob_info += ")  )  ))\n";
	          
	         scount++;

	         ret += sob_info.c_str();

	    }


//	    fprintf(fpz,"\n\n      (>= x!1 1 ) (<= x!1 mapX) (>= (- x!2 1) 1  ) (<= (- x!2 1) mapY  )\n\n"  );
	    ret += "\n\n      (>= (- x!1 1) 1 ) (<= (- x!1 1) mapX) (>= x!2 1 ) (<= x!2 mapY  )\n\n\n";


	    std::string dsa = "" ; 
	    std::string dsb = "" ;
	   
	    int dcount = 3;


	    while(dcount <= 2*numRobos)
	    {
	           std::stringstream da ;
	           da << dcount;


	           std::stringstream db ;
	           db << dcount+1;

	           std::string Da ;
	           Da = "";

	           dsa += " (and ";
	           dsa += "(= ";
	           dsa += "x!1 ";
	           dsa += "x!" + da.str();

	           dsa += ") ";
	           dsa += "(= ";
	           dsa += "(- x!2 1) ";
	           dsa += "x!" + db.str();
	           dsa += "))";

    	       dcount+=2;
		} 	



//	    fprintf(fpz, "\n      (not (= true (or %s ) )) ",  dsa.c_str()  );
		if(numRobos > 1)
		{
            // fprintf(fpz, "\n      (not (= true %s )) ",  dsa.c_str()  );
            ret += "\n      (not (= true ";
            ret += dsa.c_str();
            ret += " )) ";


		}	
     	    


	    dsa = "" ; 
	    dsb = "" ;
	   
	    dcount = 3;
	    while(dcount <= 2*numRobos)
	    {
	           std::stringstream da ;
	           da << dcount;

	           std::stringstream db ;
	           db << dcount+1;

	           std::string Da ;
	           Da = "";

	           dsa += " (and ";
	           dsa += "(= ";
	           dsa += "x!1 ";
	           dsa += "x!" + da.str();

	           dsa += ") ";
	           dsa += "(= ";
	           dsa += " x!2 ";
	           dsa += "x!" + db.str();
	           dsa += "))";


	           dcount+=2;
	    } 



//	    fprintf(fpz, "      (not (= true (or %s ) )) ",  dsa.c_str()  );
		if(numRobos > 1)
		{
            // fprintf(fpz, "\n      (not (= true %s )) ",  dsa.c_str()  );
            ret += "\n      (not (= true ";
            ret += dsa.c_str();
            ret += " )) ";


		}	
     	    



        // Function Return Value i.e. Sensor Value 
	    ret += "   )  true false ) )\n)\n\n";

    } // End of if(defi==1)  



    // ################################    Misc. Check Block Status Function ###################################

    defi=0;
    if(defi == 0)
    {


        std::string funproto = "(define-fun check_block_status(";
 
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) String \n\n\n";
  
        ret += funproto.c_str() ;

        std::stringstream mpx ;
        std:: stringstream mpy ;
	    mpx << mapx;
	    mpy << mapy;


        std::string funbody = "";

        funbody += "(ite ( or (< x!1 1 ) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< ";
        funbody += " x!2 1  ) (> ";
        funbody += " x!2 ";
        funbody += mpy.str();
        funbody += " ) ) \"occupied\"\n\n";

    //    int i;

        std::stringstream rng ;

        funbody += "(ite ( or ";

        int dcount = 1;
        std::string dsa = "";

        while(dcount <= k)
        {
            std::stringstream da ;

            da << dcount;
              
            std::stringstream db ;
            db << dcount + 1;

                    
            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "(first o" + da.str();

            dsa += ")) ";

            dsa +=  " (= (second o";
            dsa += da.str();
            dsa += ")  x!2 ";
            dsa += ")" ;

            dsa += " )";
            dcount+=1;
        } 


        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount;

            std::stringstream db ;
            db << dcount+1;

           // std::stringstream temprange ;
           // temprange << i;



            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa +=  " (=  x!"; 
       
            dsa += db.str();
            dsa +=  " x!2 ";   
             
            dsa += "))";
      
            dcount+=2;
        } 


       funbody += dsa ;

       funbody += " ) \"occupied\" \"free\"   ";


       funbody += "\n\n";


       defi ++;
       ret += funbody.c_str();
            
       for(int j = 1;j<= 2+numRobos-1;j++) // 1 for k obstacles, numRobos, 1 for extra ite
           ret += ")";
       

    }           
 

    ret += ")\n\n";

    return ret;

}






