/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/fsm/variable.cc
 * @brief   Implementation of the Variable class. 
 *
 * @note    The code of this class is partiall took by CHASE:
 *          https://chase-cps.github.io/
 *
 */

#include "fsm/variable.hh"

using namespace roboverify;
using namespace fsm;

Variable::Variable( std::string name, Type type,
        int min, int max ) :
    _name(name),
    _vartype(type),
    _minValue(min),
    _maxValue(max)
{
}

Variable::~Variable()
{
}

std::string Variable::GetName()
{
    return _name;
}

Type Variable::GetVarType()
{
    return _vartype;
}

int Variable::getMaxValue()
{
    return _maxValue;
}

int Variable::getMinValue()
{
    return _minValue;
}

