/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/fsm/mealy_machine.cc
 * @brief   Header for the Mealy Machine representation.
 */

#pragma once

#include "utilities.hh"
#include "fsm/variable.hh"

#include <set>
#include <map>
#include <vector>


namespace roboverify { namespace fsm {



    class MealyState
    {
        public:

            std::map< Variable *, int > values;

            /// @brief Constructor.
            /// @param id Name of the state.
            MealyState( std::string id );

            /// @brief public destructor.
            ~MealyState();

            std::string getId();
            void setId( std::string id );

        protected:
            std::string _id;

            MealyState( const MealyState & );
            MealyState & operator=( const MealyState & );

            bool operator==( const MealyState & );

    };

    class MealyTransition
    {
        public:
            std::map< Variable *, int > values;

            MealyTransition( 
                    MealyState * initial = nullptr,
                    MealyState * target = nullptr );
            MealyTransition();
            ~MealyTransition();

            MealyState * getInitialState();
            MealyState * getTargetState();

            void setInitialState( MealyState * state );
            void setTargetState( MealyState * state );

        protected:

            MealyState * _initial;
            MealyState * _target;

            MealyTransition( const MealyTransition & );
            MealyTransition & operator=( const MealyTransition & );
    };

    class MealyMachine
    {
        public:

            std::vector< Variable * > input_variables;
            std::vector< Variable * > output_variables;
            std::set< MealyState * > states;
            std::set< MealyTransition * > transitions;

            MealyMachine();

            ~MealyMachine(); 

            void setInitialState( MealyState * state );
            MealyState * getInitialState();
            MealyState * getStateById( std::string id );

            std::string print();

        protected:

            MealyState * _initial;

            MealyMachine( const MealyMachine & );
            MealyMachine & operator=( const MealyMachine & );
            
    };
}}

