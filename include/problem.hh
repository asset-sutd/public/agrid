/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/problem.hh
 * @brief   Header for the problem representation.
 */

#pragma once

#include "utilities.hh"
#include <fstream>
#include <vector>

#include "fsm/mealy_machine.hh"

namespace roboverify
{

    typedef struct obstacle_t
    {
        uint32_t x;
        uint32_t y;

        obstacle_t(uint32_t x, uint32_t y) :
            x(x),
            y(y)
        {
        }

    } Obstacle;

    typedef struct sensors_t
    {
        std::string type;
        uint32_t length;

        sensors_t(std::string s, uint32_t l) :
            type(s),
            length(l)
        {
        }
    } Sensors;
    
    class VerificationProblem 
    {
        public:
            std::vector< Obstacle * > obstacles;
            std::vector< Sensors * > sensors;
            bool ** occupancy_grid;

            VerificationProblem(Parameters * par);
            ~VerificationProblem();

            /// @brief Function to access the parameters setting the problem.
            /// @return pointer to the parameters structure.
            Parameters * getParameters();

            uint32_t getLatency();

            uint32_t getTiming();

            /// @brief Procedure to print to the screen the occupancy matrix.
            void printGrid();

        protected:

            uint32_t _latency;
            uint32_t _time;
            Parameters * _parameters;
            fsm::MealyMachine * _software;

            VerificationProblem( const VerificationProblem & );
            VerificationProblem & operator=( const VerificationProblem & );

            /// @brief Procedure to put obstacles into the occupancy grid.
            void _setStaticObstacles();
    };
}

