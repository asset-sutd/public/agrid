#!/bin/bash

if ! [ -x "$(command -v agrid)" ]; then
    echo 'Error: agrid executable not found.' >&2
    exit 1
fi


# Generate experiments varying the size of the matrix.
for i in 4 5 6 7 8
do
    agrid -d "$i"X$i -x 0 -y 0 -c ../specs/controller_standard_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -o \
        $1/NO_LATENCY_"$i"X"$i"_standard.smv

    agrid -d "$i"X$i -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -s proximity2 -o \
        $1/NO_LATENCY_"$i"X"$i"_proximity2.smv

    agrid -d "$i"X$i -x 0 -y 0 -c ../specs/controller_radar_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -s radary2 -o \
        $1/NO_LATENCY_"$i"X"$i"_radar2.smv
done

# Generate experiments varying the distance sensed by sensors. Proximity
for i in 2 3 4
do
    agrid -d 7X7 -x 0 -y 0 -c ../specs/controller_proximity_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -o \
        -s proximity$i \
        $1/NO_LATENCY_7X7_proximity$i.smv
done

# Generate experiments varying the distance sensed by sensors. Radar
for i in 2 3 4
do
    agrid -d 7X7 -x 0 -y 0 -c ../specs/controller_radar_latency0.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -o \
        -s radar$i \
        $1/NO_LATENCY_7X7_radar$i.smv
done

# Generate experiments varying the latency of the actuators.
for i in 1 2 3
do
    agrid -d 7X7 -x 0 -y 0 -c ../specs/controller_proximity_latency2.smv -m \
        ../specs/mission.smv -O 1E1A2E1A1E3A2E3 -o \
        -s proximity2 -l $i -t 10 \
        $1/LATENCY_"$i"_7X7_proximity.smv
done

