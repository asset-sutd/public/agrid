/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/problem.cc
 * @brief   Implementation of the problem representation.
 */

#include "problem.hh"

using namespace SMT_extended;

VerificationProblem::VerificationProblem( Parameters * par ):
    obstacles(),
    sensors(),
    occupancy_grid(nullptr),
    _latency(par->latency),
    _time(par->time),
    _parameters(par)
{
    // Height = i; Width = j;
    occupancy_grid = new bool*[par->height];
    for( uint32_t y = 0; y < par->height; ++y )
    {
        occupancy_grid[y] = new bool[par->width];
        for( uint32_t x = 0; x < par->width; ++x )
        {
            occupancy_grid[y][x] = true;
        }
    }
    _setStaticObstacles();

    if( occupancy_grid[_parameters->sy][_parameters->sx] == false )
    {
        printError("Wrong initial position");
    }

    std::string s(par->sensors);

    std::size_t found = s.find_first_of("0123456789");
    uint32_t length = 0;
    if( found != std::string::npos )
    {
        length = atoi(s.substr(found).c_str());
    }
    
    Sensors * sens = new Sensors(s.substr(0,found), length);

    sensors.push_back(sens);

}

VerificationProblem::~VerificationProblem()
{
    for( uint32_t k = 0; k < _parameters->height; ++k )
    {
        delete [] occupancy_grid[k];
    }
    delete [] occupancy_grid;

    obstacles.clear();
    sensors.clear();
}

Parameters * VerificationProblem::getParameters()
{
    return _parameters;
}

uint32_t VerificationProblem::getLatency()
{
    return _latency;
}

uint32_t VerificationProblem::getTiming()
{
    return _time;
}

void VerificationProblem::_setStaticObstacles()
{
    // String analysis
    std::string current("");

    bool xy = true;
    uint32_t cx = 0;
    uint32_t cy = 0;

    for( size_t i = 0; i < _parameters->obstacles_list.size(); ++i )
    {
        char c = _parameters->obstacles_list[i];
        bool correct = false;
        if( c >= '0' && c <= '9' )
        {
            correct = true;
            current += c;
        }

        if( xy == true && c == 'E' && 
                std::strtoul(current.c_str(), NULL, 0) < _parameters->width) 
        {
            cx = atoi(current.c_str());
            current = std::string("");
            xy = false;
            correct = true;
        }

        if(xy == false && 
                (c == 'A' || i == _parameters->obstacles_list.size() - 1) &&  
                std::strtoul(current.c_str(), NULL, 0) < _parameters->height) 
        {
            cy = atoi(current.c_str());
            current = std::string("");
            xy = true;

            std::string toprint("Obstacles in: ");
            toprint += std::to_string(cx);
            toprint += ":";
            toprint += std::to_string(cy);
            printMessage(_parameters, toprint);

            occupancy_grid[cy][cx] = false;

            Obstacle * obs = new Obstacle(cx, cy);
            obstacles.push_back(obs); 
            
            correct = true;
        }

        if( ! correct )
            printError("Bad formatting of the obstacles string");
    }
}

void VerificationProblem::printGrid()
{
    for( int32_t y = _parameters->height - 1; y >= 0; --y )
    {
        for( uint32_t x = 0; x < _parameters->width; ++x )
        {
            std::cout << occupancy_grid[y][x] << " ";
        }
        std::cout << std::endl;
    }
}
