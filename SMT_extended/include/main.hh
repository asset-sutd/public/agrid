/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/main.hh
 * @brief   Header for the entry point of the verifying tool.
 */

#pragma once

#include "utilities.hh"
#include "problem.hh"
#include "smt_printer_velacc_model.hh"


/**
 * @brief Main namespace of the program.
 */
namespace SMT_extended
{
    int main( int argc, char * arvv[] );
}

