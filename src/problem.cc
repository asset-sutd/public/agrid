/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/problem.cc
 * @brief   Implementation of the problem representation.
 */

#include "problem.hh"

using namespace roboverify;

VerificationProblem::VerificationProblem( Parameters * par ):
    obstacles(),
    sensors(),
    occupancy_grid(nullptr),
    _latency(par->latency),
    _time(par->time),
    _parameters(par),
    _software(nullptr)
{
    // Height = i; Width = j;
    occupancy_grid = new bool*[par->height];
    for( uint32_t y = 0; y < par->height; ++y )
    {
        occupancy_grid[y] = new bool[par->width];
        for( uint32_t x = 0; x < par->width; ++x )
        {
            occupancy_grid[y][x] = true;
        }
    }
    _setStaticObstacles();

    if( occupancy_grid[_parameters->sy][_parameters->sx] == false )
    {
        printError("Wrong initial position");
    }

    std::string s(par->sensors);

    std::size_t found = s.find_first_of("0123456789");
    uint32_t length = 0;
    if( found != std::string::npos )
    {
        length = atoi(s.substr(found).c_str());
    }
    
    Sensors * sens = new Sensors(s.substr(0,found), length);

    sensors.push_back(sens);

}

VerificationProblem::~VerificationProblem()
{
    for( uint32_t k = 0; k < _parameters->height; ++k )
    {
        delete [] occupancy_grid[k];
    }
    delete [] occupancy_grid;

    obstacles.clear();
    sensors.clear();
}

Parameters * VerificationProblem::getParameters()
{
    return _parameters;
}

uint32_t VerificationProblem::getLatency()
{
    return _latency;
}

uint32_t VerificationProblem::getTiming()
{
    return _time;
}

void VerificationProblem::_setStaticObstacles()
{
    if( _parameters->obstacles_file == "" ) return;

    // String analysis
    std::string line;

    uint32_t cx = 0;
    uint32_t cy = 0;

    std::ifstream ofile( _parameters->obstacles_file.c_str() );
    if( ! ofile.is_open() )
    {
        printError("Impossible to open the static obstacles list file.");
    }
        
    std::size_t comma;
    while (getline( ofile, line ))
    {
        comma = line.find_first_of(',');
        std::string sx = line.substr(0,comma);
        std::string sy = line.substr(comma+1);

        cx = std::stoul(sx);
        cy = std::stoul(sy);

        Obstacle * obs = new Obstacle(cx, cy);
        obstacles.push_back(obs); 

    }

    ofile.close();
            
}

void VerificationProblem::printGrid()
{
    for( int32_t y = _parameters->height - 1; y >= 0; --y )
    {
        for( uint32_t x = 0; x < _parameters->width; ++x )
        {
            std::cout << occupancy_grid[y][x] << " ";
        }
        std::cout << std::endl;
    }
}
