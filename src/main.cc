/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/main.cc
 * @brief   Entry point of the verifying tool.
 */

#include "main.hh"

using namespace roboverify;

int main( int argc, char * argv[] )
{

    Parameters * params = parseCmdLine(argc, argv);
    VerificationProblem * problem = new VerificationProblem(params);

    if(problem->getParameters()->use_smt == true) 
    {
	    SMTPrinter printer(problem);
	    printer.printSMTFile();
    }	

    else
    {	
	    NuSMVPrinter printer(problem);
	    printer.printNuSMVFile();
    }

    delete(problem);
    delete(params);

}
