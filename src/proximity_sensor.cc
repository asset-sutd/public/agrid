
#include "smt_printer.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace roboverify;
std:: string SMTPrinter::_computeSensorValuesProximity(int Range)
{

    std::string ret = "";  
   
    int defi = 0;

    if(defi == 0)
    {

        std::string funproto = "(define-fun det_up(";

        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;

  
        std::string funbody = ""; 
        int i;

        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


	    funbody += "(ite ( or (< x!1 1) (> x!1 ";
	    funbody += mpx.str();
	    funbody += ") (< x!2 1) (> x!2 ";
	    funbody += mpy.str();
	    funbody += ") ) -1 \n\n" ;


        for(i= 0; i<= Range ; i++ )
        {

            std::stringstream rng ;
 
            if (i==0)
                rng << 0;
            else
                rng << i;

            funbody += "(ite ( or \n      ";

            int dcount = 1;
            std::string dsa = "";

            while(dcount <= k)
            {

                std::stringstream da ;
                da << dcount;
             
                std::stringstream db ;
                db << dcount + 1;
          
                dsa += "\n    (and ";
                dsa += "(= ";
                dsa += "x!1 ";
                dsa += "(first o" + da.str();
                dsa += ")) ";
        
                dsa +=  " (= (second o";
                dsa += da.str();
                dsa += ")   (+ x!2 ";
                dsa += rng.str(); 
                dsa += "))" ;
                dsa += " )";

           

                if(dcount ==1)
                {    
                   // Check Outside Map

	               //dsa += " (and ";
	               dsa += "\n    (<  ";
	               dsa += "x!1 ";
	               dsa += " 1";
	               dsa += ") ";
	               dsa += "(>  ";
	               dsa += "x!1 ";
	               dsa += mpx.str();
	               dsa += ") ";

                   dsa += "(< (+ x!2 ";
                   dsa += rng.str();
                   dsa += ") " ;
                   dsa += " 1) ";

                   dsa += "(> (+ x!2  ";
                   dsa += rng.str();
                   dsa += ") " ;
                   dsa += mpy.str();
                   dsa += " ) \n";

                }
 



                dcount+=1;
            } 
  
   
            dcount = 3;

            while(dcount <= 2*numRobos)
            {
	            std::stringstream da ;
	            da << dcount;

	            std::stringstream db ;
	            db << dcount+1;

	            std::stringstream temprange ;
	            temprange << i;

	            dsa += "    (and ";
	            dsa += "(= ";
	            dsa += "x!1 ";
	            dsa += "x!" + da.str();

	            dsa += ") ";
	            dsa +=  " (=  x!"; 
	      
	            dsa += db.str();
	            dsa +=  " (+ x!2 ";   
	            dsa += temprange.str();
	            dsa += ")))";

	            dcount+=2;
	        } 



	        funbody += dsa ;
	 
	        funbody += " )   (- ";

	        funbody += rng.str();
	        funbody += " 1) ";

	        funbody += "\n\n";

	    }

        defi ++;


        std::stringstream snsRange ;
        snsRange << Range;

        ret += funbody.c_str();
        ret += snsRange.str(); 


        for(int j = 0;j<=Range;j++)
            ret +=  ")";
 
         
        ret += "  )\n\n)\n\n";

    }           

    ret += "\n\n";

    // Define Down Sensor
    


    if(defi == 1)
    {


        std::string funproto = "(define-fun det_down(";
 
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;


        std::string funbody = "";

        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        funbody += "(ite ( or (< x!1 1) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< x!2 1) (> x!2 ";
        funbody += mpy.str();
        funbody += ") ) -1 \n\n" ;


        int i;
        for(i= 0; i<= Range ; i++ )
        {
        
            std::stringstream rng ;

            if (i==0)
                rng << 0;
            else
                rng << i;

            funbody += "(ite ( or ";
    
            funbody += "\n      ";

            int dcount = 1;
            std::string dsa = "";

            while(dcount <= k)
            {
                std::stringstream da ;
                da << dcount;
             
                std::stringstream db ;
                db << dcount + 1;

             
                dsa += "\n    (and ";
                dsa += "(= ";
                dsa += "x!1 ";
                dsa += "(first o" + da.str();
                dsa += ")) ";
        
                dsa +=  " (= (second o";
                dsa += da.str();
                dsa += ")   (- x!2 ";
                dsa += rng.str(); 
                dsa += "))" ;
                dsa += " )";

                if(dcount ==1)
                {
           
                    // Check Outside Map

		           //dsa += " (and ";
		           dsa += "\n    (<  ";
		           dsa += "x!1 ";
		           dsa += " 1";
		           dsa += ") ";
		           dsa += "(>  ";
		           dsa += "x!1 ";
		           dsa += mpx.str();
		           dsa += ") ";

                   dsa += "(< (- x!2  ";
                   dsa += rng.str();
                   dsa += ") " ;
                   dsa += " 1) ";

                   dsa += "(> (- x!2  ";
                   dsa += rng.str();
		           dsa += ") " ;
		           dsa += mpy.str();
		           dsa += " )\n";

                }
 
                dcount+=1;
            } 

 
            dcount = 3;


            while(dcount <= 2*numRobos)
            {
                std::stringstream da ;
                da << dcount;

                std::stringstream db ;
                db << dcount+1;

                std::stringstream temprange;
                temprange << i;

                dsa += "    (and ";
                dsa += "(= ";
                dsa += "x!1 ";
                dsa += "x!" + da.str();

                dsa += ") ";
                dsa +=  " (=  x!"; 
   
                dsa += db.str();
                dsa +=  " (- x!2 ";   
                dsa += temprange.str();
                dsa += ")))";
            
                dcount+=2;
            } 

            funbody += dsa ;
 
            funbody += " )   (- ";
            funbody += rng.str();
            funbody += " 1) ";

            funbody += "\n\n";

        }

        defi ++;

        std::stringstream snsRange ;
        snsRange << Range;

        ret += funbody.c_str();
        ret += snsRange.str(); 
            
        for(int j = 0;j<=Range;j++)
            ret += ")";

        ret +=  "  )\n\n)\n\n";

    }  
       
    ret += "\n\n";


    // ****** Right Sensor Function

    defi = 0;

    if(defi == 0)
    {


        std::string funproto = "(define-fun det_right(";
 
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;

        std::string funbody = "";
        int i;

        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


   
        funbody += "(ite ( or (< x!1 1) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< x!2 1) (> x!2 ";
        funbody += mpy.str();
        funbody += ") ) -1 \n\n" ;

        for(i= 0; i<= Range ; i++ )
        {

            std::stringstream rng ;

            if (i==0)
                rng << 0;
            else
                rng << i;


            funbody += "(ite ( or ";

            funbody += "\n      ";
 
            int dcount = 1;
            std::string dsa = "";

            while(dcount <= k)
            {
                std::stringstream da ;

                da << dcount;
             
                std::stringstream db ;
                db << dcount + 1;

     
                dsa += "\n    (and ";
                dsa += "(= ";
                dsa += "x!2 ";
                dsa += "(second o" + da.str();
                dsa += ")) ";
        
                dsa +=  " (= (first o";
                dsa += da.str();
                dsa += ")   (+ x!1 ";
                dsa += rng.str(); 
                dsa += "))" ;
                dsa += " )";

                if(dcount ==1)
                {
           
                    // Check Outside Map

	                //dsa += " (and ";
	                dsa += "\n    (<  ";
	                dsa += "x!2 ";
		            dsa += " 1";
		            dsa += ") ";
		            dsa += "(>  ";
		            dsa += "x!2 ";
		            dsa += mpy.str();
		            dsa += ") ";

 
                    dsa += "(< (+ x!1  ";
                    dsa += rng.str();
                    dsa += ") " ;
                    //  dsa += mpx.str();
                    dsa += " 1) ";

                    dsa += "(> (+ x!1  ";
                    dsa += rng.str();
                    dsa += ") " ;
                    dsa += mpx.str();
                    dsa += " )\n";

                }

                dcount+=1;
            } 

   
            dcount = 3;


	        while(dcount <= 2*numRobos)
	        {
	            std::stringstream da ;
	            da << dcount+1;

	            std::stringstream db ;
	            db << dcount;

	            std::stringstream temprange ;
	            temprange << i;

	            dsa += "    (and ";
	            dsa += "(= ";
	            dsa += "x!2 ";
	            dsa += "x!" + da.str();
	 
	            dsa += ") ";
	  
	            dsa +=  " (=  x!"; 
	            dsa += db.str();
	            dsa +=  " (+ x!1 ";   
	            dsa += temprange.str();
	            dsa += ")))";
	            
	            dcount+=2;
	        } 


	        funbody += dsa ;

	        funbody += " )   (- ";
	        funbody += rng.str();
	        funbody += " 1) ";

	        funbody += "\n\n";

	    }

        defi ++;
         
	    std::stringstream snsRange ;
	    snsRange << Range;

	    ret += funbody.c_str();
	    ret += snsRange.str(); 
	   
	    for(int j = 0;j<=Range;j++)
	        ret +=  ")";

	    ret += "  )\n\n)\n\n";

    }           

    ret += "\n\n";


    // Left Sensors

    if(defi == 1)
    {

        std::string funproto = "(define-fun det_left(";
   
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
           std::stringstream cp ;
           cp << count_proto;

           std::string Proto = "x!" + cp.str();
         
           funproto += " (";
           funproto += Proto;
           funproto += " Int)";

           count_proto ++;
        }


        funproto += "  ) Int \n\n\n";

        ret += funproto.c_str() ;

        std::string funbody = "";

        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


   
        funbody += "(ite ( or (< x!1 1) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< x!2 1) (> x!2 ";
        funbody += mpy.str();
        funbody += ") ) -1 \n\n" ;


        int i;
        for(i= 0; i<= Range ; i++ )
        {

            std::stringstream rng ;

            if (i==0)
                rng << 0;
            else
                rng << i;

            funbody += "(ite ( or ";
     
            funbody += "\n      ";
            
            int dcount = 1;
            std::string dsa = "";

            while(dcount <= k)
            {
                std::stringstream da ;
                da << dcount;

                std::stringstream db ;
                db << dcount + 1;

                dsa += "\n    (and ";
                dsa += "(= ";
                dsa += "x!2 ";
                dsa += "(second o" + da.str();
                dsa += ")) ";
        
                dsa +=  " (= (first o";
                dsa += da.str();
                dsa += ")   (- x!1 ";
                dsa += rng.str(); 
                dsa += "))" ;
                dsa += " )";

                if(dcount ==1)
                {
           
                    // Check Outside Map

                    //dsa += " (and ";
                    dsa += "\n    (<  ";
	                dsa += "x!2 ";
	                dsa += " 1";
		            dsa += ") ";
		            dsa += "(>  ";
		            dsa += "x!2 ";
		            dsa += mpy.str();
		            dsa += ") ";


                    dsa += "(< (- x!1  ";
                    dsa += rng.str();
                    dsa += ") " ;
                    dsa += " 1) ";
 

		            dsa += "(> (- x!1  ";
		            dsa += rng.str();
		            dsa += ") " ;
		            dsa += mpx.str();
		            dsa += " )\n";

                }


                dcount+=1;
            } 

   
            dcount = 3;

            while(dcount <= 2*numRobos)
            {
                std::stringstream da ;
                da << dcount+1;

                std::stringstream db ;
                db << dcount;

                std::stringstream temprange ;
                temprange << i;

                dsa += "    (and ";
                dsa += "(= ";
                dsa += "x!2 ";
                dsa += "x!" + da.str();

                dsa += ") ";
 
                dsa +=  " (=  x!"; 
    
                dsa += db.str();
                dsa +=  " (- x!1 ";   
                dsa += temprange.str();
                dsa += ")))";
            

                dcount+=2;
            } 


            funbody += dsa ;

	        funbody += " )   (- ";

	        funbody += rng.str();
	        funbody += " 1) ";

	        funbody += "\n\n";

	    }

	    defi ++;
	  
	    std::stringstream snsRange ;
	    snsRange << Range;

	    ret += funbody.c_str();
	    ret += snsRange.str(); 
	         
	    for(int j = 0;j<=Range;j++)
	        ret += ")";

	    ret += "  )\n\n)\n\n";

    }           

    ret += "\n\n\n\n";



    // ################################    Misc. Check Block Status Function ###################################

    defi=0;
    if(defi == 0)
    {


        std::string funproto = "(define-fun check_block_status(";
 
        int count_proto = 1;

        while(count_proto <= 2*numRobos)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) String \n\n\n";
  
        ret += funproto.c_str() ;

        std::stringstream mpx ;
        std:: stringstream mpy ;
	    mpx << mapx;
	    mpy << mapy;


        std::string funbody = "";

        funbody += "(ite ( or (< x!1 1 ) (> x!1 ";
        funbody += mpx.str();
        funbody += ") (< ";
        funbody += " x!2 1  ) (> ";
        funbody += " x!2 ";
        funbody += mpy.str();
        funbody += " ) ) \"occupied\"\n\n";

     //   int i;

        std::stringstream rng ;

        funbody += "(ite ( or ";

        int dcount = 1;
        std::string dsa = "";

        while(dcount <= k)
        {
            std::stringstream da ;

            da << dcount;
              
            std::stringstream db ;
            db << dcount + 1;

                    
            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "(first o" + da.str();

            dsa += ")) ";

            dsa +=  " (= (second o";
            dsa += da.str();
            dsa += ")  x!2 ";
            dsa += ")" ;

            dsa += " )";
            dcount+=1;
        } 


        dcount = 3;

        while(dcount <= 2*numRobos)
        {
            std::stringstream da ;
            da << dcount;

            std::stringstream db ;
            db << dcount+1;

         //   std::stringstream temprange ;
           // temprange << i;



            dsa += " (and ";
            dsa += "(= ";
            dsa += "x!1 ";
            dsa += "x!" + da.str();

            dsa += ") ";
            dsa +=  " (=  x!"; 
       
            dsa += db.str();
            dsa +=  " x!2 ";   
             
            dsa += "))";
      
            dcount+=2;
        } 


       funbody += dsa ;

       funbody += " ) \"occupied\" \"free\"   ";


       funbody += "\n\n";


       defi ++;
       ret += funbody.c_str();
            
       for(int j = 1;j<= 2+numRobos-1;j++) // 1 for k obstacles, numRobos, 1 for extra ite
           ret += ")";
       

    }           
 

    ret += ")\n\n";

    return ret;

}



