/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/utilities.hh
 * @brief   Header for utilities functions.
 */

#pragma once

#include <iostream>
#include <unistd.h>
#include <cstdint>
#include <string>

/**
 * @brief Main namespace of the program.
 */
namespace roboverify
{
    typedef struct _parameters
    {
        /// @brief Solver type
        /// true - indicate that SMT is used; otherwise BMC is used.
        bool use_smt;
        /// @brief Output file path.
        std::string fileOut;
        /// @brief Verbose flag. Exection is verbose if flag is true.
        bool verbose;

        /// @brief Number of tiles of the width of the grid.
        uint32_t width;
        /// @brief Number of tiles of the height of the grid.
        uint32_t height;

        /// @brief Starting point in the x axis
        uint32_t sx;

        /// @brief Starting point in the y axis
        uint32_t sy;

        /// @brief File listing the starting positions of the robots.
        std::string initial_positions_file;

        /// @brief list of obstacles.
        std::string obstacles_file;

        /// @brief list of sensors.
        std::string sensors;

        /// @brief The latency of the actuators.
        uint32_t latency;

        /// @brief path to the controller SW file.
        std::string controller;

        std::string mission;

        /// @brief time constraint. 0 acts as a jolly to identify no constraint.
        uint32_t time;

        // New Parameters
        /// \TODO Remove
        /// @brief true is for positive, false for negative scenario.
        bool model_scenario;

        bool model_delays_uncertanities;
        uint32_t sensor_prop_delays;
        uint32_t min_phys_counter;
        uint32_t max_phys_counter;
        uint32_t time_bound;




        /// @brief Standard constructor.
        _parameters() :
            use_smt(false),
            fileOut("specification.smv"),
            verbose(false),
            width(3),
            height(3),
            sx(0),
            sy(0),
            initial_positions_file(""),
            obstacles_file(""),
            sensors("standard"),
            latency(0),
            controller(""),
            mission(""),
            time(10),
            model_scenario(false),
            model_delays_uncertanities(false),
            sensor_prop_delays(0),
            min_phys_counter(2),
            max_phys_counter(4),
            time_bound(10)
        {
        }
    } Parameters;


    /// @brief Method printing usage banner of chase.
    void printHelp();

    /// @brief Method parsing the input command line.
    Parameters * parseCmdLine( int argc, char * argv[] );

    /// @brief Managing of standard messages.
    /// @param p        Pointer to the structure storing the command line parameters.
    /// @param msg      Message to print.
    /// @param colored  Flag to enable colors. If true messages are colored.
    void printMessage( 
            Parameters * p, std::string msg, bool colored=false ); 

    /// @brief Managing of error messages.
    /// @param msg      Message to print.
    /// @param colored  Flag to enable colors. If true messages are colored.
    void printError( std::string msg, bool colored=false ); 

    /// @brief Managing of warning messages.
    /// @param p        Pointer to the structure storing the command line parameters.
    /// @param msg      Message to print.
    /// @param colored  Flag to enable colors. If true messages are colored.
    void printWarning( 
            Parameters * p, std::string msg, bool colored=false ); 

    void interpretSize( std::string arg, Parameters * p );

}

