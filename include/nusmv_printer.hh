/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/nusmv_printer.hh
 * @brief   Header for the printer.
 */

#pragma once

#include "problem.hh"
#include <fstream>
#include <sstream>
#include <cassert>
namespace roboverify
{
    class NuSMVPrinter
    {
        public:
            NuSMVPrinter( VerificationProblem * p );
            ~NuSMVPrinter();

            void printNuSMVFile();

        protected:

            VerificationProblem * _problem;
            Parameters * _parameters;

            NuSMVPrinter( const NuSMVPrinter & );
            NuSMVPrinter & operator=( const NuSMVPrinter & );

            std::string _getMissionSkeleton();
            std::string _getMission();
            std::string _getSpaceDeclaration();
            std::string _getObstaclesDeclaration();
            std::string _getSensorsDeclaration();
            std::string _getSensorsDeclarationStandard();
            std::string _getSensorsDeclarationProximity(Sensors * sensors);
            std::string _getSensorsDeclarationRadar(Sensors * sensors);

            std::string _getObstaclesComputation();
            
            std::string _getSensorsComputation();
            std::string _getSensorsComputationStandard();
            std::string _getSensorsComputationProximity(Sensors * sensors);
            std::string _getSensorsComputationRadar(Sensors * sensors);

            
            std::string _getNextPositionComputation();
            std::string _getTargetAchievedAssignment();


            std::string _getOccupancyGridWithObstacle();

            std::string _getControllerSkeleton();
            std::string _getControllerSw();

            std::string _getMain();

    };
}
