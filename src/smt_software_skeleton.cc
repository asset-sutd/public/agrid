#include "smt_printer.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace roboverify;

std::string SMTPrinter::_getSoftwareSkeleton(std::string fn)
{
    
    /// \TODO Remove the SnsDist usage (it is here to remove a warning).
   // if( SnsDist == 0 ){ SnsDist = 0; }


    std::string foo = "";
 
 
    std::ifstream file(fn) ; // open this file for input

    std::string line ;
    while( std::getline( file, line ) ) // for each line read from the file
    {
        foo += line;
        foo += "\n";
    }

    
    return foo ;

 }
