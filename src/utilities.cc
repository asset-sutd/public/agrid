#include "utilities.hh"

using namespace roboverify;

Parameters * roboverify::parseCmdLine( int argc, char * argv[] )
{
    Parameters * parameters = new Parameters();

    opterr = 0;
    int c;

    while(( c = getopt(argc, argv, 
                    "Td:MU:P:p:q:B:O:s:l:x:y:i:c:m:t:Vo:")) != -1 )
    {
        switch(c)
        {
            case 'T':
                parameters->use_smt = true;                
                break;
            case 'd':
                {
                std::string param = std::string(optarg);
                interpretSize(optarg, parameters);
                }
                break;
            case 'M':
                parameters->model_scenario = true;
                break;                
            case 'U':
                parameters->model_delays_uncertanities = true;
                break;                
            case 'P':
                parameters->sensor_prop_delays = atoi(optarg);
                break;                
            case 'p':
                parameters->min_phys_counter = atoi(optarg);
                break;                
            case 'q':
                parameters->max_phys_counter = atoi(optarg);
                break;                
            case 'B':
                parameters->time_bound = atoi(optarg);
                break;                
            case 'O':
                parameters->obstacles_file = std::string(optarg);
                break;
            case 's':
                parameters->sensors = std::string(optarg);
                break;
            case 'l':
                parameters->latency = atoi(optarg);
                break;
            case 'x':
                parameters->sx = atoi(optarg);
                break;
            case 'y':
                parameters->sy = atoi(optarg);
                break;
            case 'i':
                parameters->initial_positions_file = std::string(optarg);
                break;
            case 'c':
                parameters->controller = std::string(optarg);
                break;
            case 'm':
                parameters->mission = std::string(optarg);
                break;
            case 't':
                parameters->time = atoi(optarg);
                break;
            case 'o':
                parameters->fileOut = std::string(optarg);
                break;
            case 'V':
                parameters->verbose = true;
                break;
            default:
                printHelp();
                exit(-1);
        }
    }

    if( parameters->fileOut == "specification.smv" 
            && parameters->use_smt )
    {
        parameters->fileOut = "specification.smt2";
    }

    return parameters;
}

void roboverify::printMessage( 
        Parameters * p, std::string msg, bool colored )
{
    if( p->verbose )
    {
        if( colored )
        {
            std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
        }
        else
        {
            std::cout << msg << std::endl;
        }
    }
}

void roboverify::printError( std::string msg, bool colored )
{
    if( colored )
    {
        std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
    }
    else
    {
        std::cout << msg << std::endl;
    }
}

void roboverify::printWarning( 
        Parameters * p, std::string msg, bool colored )
{
    if( p->verbose )
    {
        if( colored )
        {
            std::cout << "\033[1;32m"<< msg << "\033[0m" << std::endl;
        }
        else
        {
            std::cout << msg << std::endl;
        }
    }
}

void roboverify::printHelp()
{
    std::cerr << "[USAGE]\n" <<
        "agrid\t[-T] [-M] [-d size] [-x starting width] [-y starting height]" << std::endl
        << "\t[-O obstacles list file] [-s sensor type] [-c control sw file] " << std::endl
        << "\t[-m mission file] [-V] [-o output file]" << std::endl
        << "\t[-U] [-P sensor delay value] [-p minimum actuation delay] " << std::endl
        << "\t[-q maximum actuation delay] " << std::endl
        << "\t[-B maximum number of evaluation steps (for SMT)]" << std::endl
        << std::endl
        << "\t-T enables the use of SMT rather than Bounded Model Checking" 
        << std::endl
        /// \TODO Remove this, by allowing to specify the scenario externally.
        << "\t-M set the scenario to positive. Otherwise is negative." 
        << std::endl
        << "\t-U Enable uncertainties." << std::endl
        << "\t-d size" 
        << "Size is expressed using the [width]X[height] syntax, where width"
        << std::endl << "\t\tand height are two positive integers."
        << std::endl
        << "\t-O obstacles_file Obstacles are listed in a file one obstacle per line." 
        << std::endl
        << "\t\teach entry is expressed on a line with syntax: X,Y" << std::endl
        << "\t\twhere X will be the horizontal coordinate, while Y is the vertical"
        << std::endl << "\t-s sensor type " 
        << "Sensors may be: standard, proximity, or radar. The type is" 
        << std::endl << "\t\tfollowed by the length of the sensor:" << std::endl
        << "\t\tfor instance, proximity2 indicates a proximity sensor of length 2."
        << std::endl
//        << 
        << std::endl;
}


void roboverify::interpretSize( std::string arg, Parameters * p )
{
    size_t X = arg.find('X');
    std::string ww = arg.substr(0,X);
    std::string hh = arg.substr(X+1);

    if( X == std::string::npos )
    {
        printHelp();
        exit(-1);
    }

    if(
            ww.find_first_not_of("0123456789") != std::string::npos ||
            hh.find_first_not_of("0123456789") != std::string::npos
      )
    {
        printHelp();
        exit(-1);
    }

    p->width = atoi( ww.c_str() );
    p->height = atoi( hh.c_str() );

}

