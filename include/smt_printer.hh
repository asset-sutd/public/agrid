/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/nusmv_printer.hh
 * @brief   Header for the printer.
 */

#pragma once

#include "problem.hh"
#include <fstream>
#include <sstream>
#include <cassert>
namespace roboverify
{
    class SMTPrinter
    {
        public:
            SMTPrinter( VerificationProblem * p );
            ~SMTPrinter();

            void printSMTFile();
        
            int numRobos;
            int k;        // Number of static Obtsacles
            int mapx ;   // Map Width
	        int mapy;   // Map Height
            int T;     // Time to Complete the task

            char SensorType;
            int SensorDistance;
            
            // Sensor and Actuauation Delays
            bool if_model_sns_act_delays;  
            bool modelScenario;
            int SnsPropagDelay;
            int minpc;
            int mpc;
            std::string robot_stop_criteria ;
  

            std::stringstream MapWidth;  
            std::stringstream MapHeight;



        protected:

            VerificationProblem * _problem;
            Parameters * _parameters;

            SMTPrinter( const SMTPrinter & );
            SMTPrinter & operator=( const SMTPrinter & );

            std::string _getScenario();
            std::string _getSoftwareSkeleton(std::string filename);
            std::string _computeSensorValuesStandard();
            std::string _computeSensorValuesProximity(int SnsRange);       
            std::string _computeSensorValuesRadar(int SnsRange);       
            std::string _robotPositionVars();
            std::string _generatedCommandVars();         

            // Actuation Delays
            std::string  _CommandInExecution_And_PhysCounter();
            std::string  _gtca_for_latest_command();
            std::string  _sensors_In_Effect();
            std::string  _generateVelocitySpec();
            std::string  _decidingWhatCommandToExecute();
            std::string _getCommandGeneratedUnderDelays();
            std::string _getCommandActionsUnderDelays();        
            std::string _getSensorOutputUnderDelays();
            std:: string _computeSensorValuesRadarUnderDelays(int SnsRange); 

            std::string _getTargetVars();    
            std::string _getSensorTupleVars(); 
            std::string _getInitialPos();    
            std::string _getTargetsRange();
            std::string _getSensorOutput();
            std::string _getCommandGenerated();
            std::string _getCommandActions();
            std::string _getSafetySpec();
            std::string _verifySpecNegation();
            std::string _generateModel();

        
    };


 
 

}
