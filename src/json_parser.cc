/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	src/json_parser.cc
 * @brief   Implementation of the JSON parser for the Mealy Machine models.
 * 
 */

#include "fsm/mealy_machine.hh"
#include "json_parser.hh"

#include <set>
#include <map>
#include <list>


using namespace roboverify;
using namespace fsm;

using namespace nlohmann;

MealyMachine * JSONParser::parseMealyMachine( std::string json_file )
{
    MealyMachine * machine = new MealyMachine();

    std::ifstream in( json_file.c_str() );
    if( in.fail() )
    {
        std::cerr << "No such file or directory: " << json_file << std::endl;
        exit(-1);
    }

    json specification;
    in >> specification;

    json environment = specification["ENV"];
    json system = specification["SYS"];
    json nodes = specification["nodes"];

    _createVariables( machine, environment, system );
    _createStates( machine, nodes );
    _createTransitions( machine, nodes );

    return machine;

}

void JSONParser::_createVariables( MealyMachine * machine, json env, json sys )
{
    for( json::iterator it = env.begin(); it != env.end(); ++it )
    {
        json varEnv = it.value();
        for( json::iterator jt = varEnv.begin(); jt != varEnv.end(); ++jt )
        {
            std::string name(jt.key());
            Variable * var;
            if( jt.value() == "boolean" )
            {
                var = new Variable(name);
            }
            else
            {
                std::vector< int > bounds = 
                    jt.value().get<std::vector<int>>();
                var = new Variable(name, t_int, bounds[0], bounds[1]);
            }
            machine->input_variables.push_back(var);
        }

    }

    for( json::iterator it = sys.begin(); it != sys.end(); ++it )
    {
        json varSys = it.value();
        for( json::iterator jt = varSys.begin(); jt != varSys.end(); ++jt )
        {
            std::string name(jt.key());
            Variable * var;
            if( jt.value() == "boolean" )
            {
                var = new Variable(name);
            }
            else
            {
                std::vector< int > bounds = 
                    jt.value().get<std::vector<int>>();
                var = new Variable(name, t_int, bounds[0], bounds[1]);
            }
            machine->output_variables.push_back(var);
        }

    }

}

void JSONParser::_createStates( MealyMachine * machine, json states )
{
    for( json::iterator it = states.begin(); it != states.end(); ++it )
    {
        json state = it.value();
        std::string name(it.key());
        machine->states.insert( _createState(machine, name, state) );
    }
}

MealyState * JSONParser::_createState( 
        MealyMachine * machine, std::string name, json state )
{

    MealyState * ret = new MealyState( name );

    std::vector< int > values = state["state"];
    size_t size_input = machine->input_variables.size();
    size_t size_output = machine->output_variables.size();

    if( values.size() != (size_input + size_output) )
    {
        std::cerr << "WRONG STATE" << std::endl;
        exit(-1);
    }

    for( size_t i = 0; i < values.size(); ++i )
    {
        if( i < size_input )
        {
            Variable * var = machine->input_variables[i];
            std::pair< Variable *, int > p(var, values[i]);
            ret->values.insert(p);
        }
        else
        {
            Variable * var = machine->output_variables[i - size_input];
            std::pair< Variable *, int > p(var, values[i]);
            ret->values.insert(p);
        }
    }

    return ret;
}


void JSONParser::_createTransitions( MealyMachine * machine, json states )
{
    for( json::iterator it = states.begin(); it != states.end(); ++it )
    {
        json state = it.value();
        std::string name(it.key());

        MealyState * initial = machine->getStateById(name);

        std::vector< std::string > transitions = state["trans"]; 
        for( size_t i = 0; i < transitions.size(); ++i )
        {
            MealyState * target = machine->getStateById( transitions[i] );
            MealyTransition * transition = 
                new MealyTransition( initial, target );
            machine->transitions.insert(transition);
        }
    }
}

