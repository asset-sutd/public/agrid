/**
 * @author      <a href="mailto:2016csy0004@iitrpr.ac.in">Amit Behal</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file    src/smt_software_skeleton.cc
 * @brief   Source for the printer.
 */

#include "smt_printer_velacc_model.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace SMT_extended;

std::string SMTPrinterVelAcc::_getSoftwareSkeleton(std::string fn)
{
    
    /// \TODO Remove the SnsDist usage (it is here to remove a warning).
   // if( SnsDist == 0 ){ SnsDist = 0; }


    std::string foo = "";
 
 
    std::ifstream file(fn) ; // open this file for input

    std::string line ;
    while( std::getline( file, line ) ) // for each line read from the file
    {
        foo += line;
    }

    
    return foo ;

 }
