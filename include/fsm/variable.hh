/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/fsm/variable.hh
 * @brief   Header for the Variable class.
 *
 * @note    The code of this class is partiall took by CHASE:
 *          https://chase-cps.github.io/
 *
 */

#include <string>

namespace roboverify { namespace fsm {

    enum Type{
        t_bool,
        t_int
    };

    class Variable
    {
        public:

            /// @brief Constructor.
            /// @param name Name of the variable.
            /// @param type Type of the variable.
            Variable( std::string name, Type type = t_bool,
                  const int min = 0, const int max = 1 );

            /// @brief Destructor.
            ~Variable();

            /// @brief Function returning the variable name.
            /// @return string of the name of the variable.
            std::string GetName();

            /// @brief Function returning the variable type.
            /// @return type of the variable.
            Type GetVarType();

            int getMaxValue();
            int getMinValue();

        protected:

            /// @brief name of the Variable
            std::string _name;
            /// @brief type of the Variable.
            Type _vartype;

            int _minValue;
            int _maxValue;

            Variable( const Variable & );
            Variable & operator=( const Variable & );

    };

}}

