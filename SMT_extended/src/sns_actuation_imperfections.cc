/**
 * @author      <a href="mailto:2016csy0004@iitrpr.ac.in">Amit Behal</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file    src/smt_actuation_imprefections.cc
 * @brief   Source for the printer.
 */

#include "smt_printer_velacc_model.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace SMT_extended;

std::string SMTPrinterVelAcc:: _computeSensorValuesRadarUnderDelays(int radar_range)
{

    // Radar Sensors Under Extended VA model
    std::string ret = "";

    int defi = 0;

    if(defi == 0)
    {

        std::string funproto = "(define-fun det_radar( (x!0 String) ";
    
        int count_proto = 1;

        while(count_proto <= 2*numBlocksRobo)
        {
            std::stringstream cp ;
            cp << count_proto;

            std::string Proto = "x!" + cp.str();
         
            funproto += " (";
            funproto += Proto;
            funproto += " Int)";

            count_proto ++;
        }


        funproto += "  ) Bool \n";

        ret += funproto.c_str() ;

         
 
   
        std::string funbody = "";
        std::string sob_info = "";

        int i;


        std::stringstream mpx ;
        std::stringstream mpy ;
        mpx << mapx;
        mpy << mapy;


        std::string check_outside = "";

        for(i=1;i<=2*numBlocksRobo;i++)
        {
            std::stringstream xIndex;
            xIndex << i;
         
            check_outside += "(< x!";
            check_outside += xIndex.str();
            check_outside += " 1) ";

            check_outside += "(> x!";
            check_outside += xIndex.str();
            check_outside += " ";
            check_outside += mpx.str();
            check_outside += ")";
        }



        funbody += "(ite ( or ";
        funbody += check_outside.c_str();
        funbody += " ) false \n\n" ;

    
        sob_info += funbody;

  
        int scount = 1;


        if(k >= 1 || numBlocksRobo > 1  )
        {

            sob_info += "\n\n\n(ite (= false (or ";

            for(int ti=-1*radar_range;ti<= radar_range ;ti++)
            {

                for(int tj=-1*radar_range;tj<= radar_range ;tj++)
                {
                    // if(abs(i)+abs(j)<= 3)
                       //   fprintf(fpz,"   x(%d)(%d)", i,j); 

                    //   fprintf(fpz,"\n\n\n");

                    if(abs(ti) + abs(tj) <= radar_range)
                    { 



                    for(int numBlocksRoboIterator = 1; numBlocksRoboIterator <= 2*numBlocksRobo; numBlocksRoboIterator+=2 )
                    { 
                     
                        scount = 1;

                        int printonce = 0;


                        while(scount <= k)
                        {
         
                            std::stringstream sc ;
                            sc << scount;

                            std::stringstream rnge ;
                            rnge << radar_range;
                         
                    

                            std::stringstream blocksRoboXcoord ;
                            std::stringstream blocksRoboYcoord ;
                            

                            blocksRoboXcoord << numBlocksRoboIterator;
                            blocksRoboYcoord << numBlocksRoboIterator + 1;
                            
                            

                            sob_info += "\n\n        (and ";

                 
                            sob_info += "(= (first o" ;
                            sob_info += sc.str();
                            sob_info += ") ";
                         

                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";

                            }                                 


                            std::stringstream ic;
                            ic << abs(ti);

                            std::stringstream jc;
                            jc << abs(tj);
                                     
                            sob_info += ic.str();
                            sob_info += ")) ";
                              

                            sob_info += "(= (second o" ;
                            sob_info += sc.str();
                            sob_info += ") ";
                         
                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";

                            }                  
                 
                            sob_info += jc.str();
                            sob_info += ")) ) ";
          
                       
                            if( printonce >= 0) 
                            {   // Check within Map  
                                sob_info += "\n        (or ";
                                
                                // clause
                                sob_info += "(< ";

                     
                                //sob_info += "(< (first o" ;
                                //sob_info += sc.str();
                                //sob_info += ") ";
                                               

                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";

                            }                          
                
                                sob_info += ic.str();
                                sob_info += ") ";

                                std::stringstream mapsizex ;     
                                std::stringstream mapsizey ;     
                         
                                mapsizex << mapx;
                                mapsizey << mapy;


                                //  sob_info += mapsizex.str();
                                sob_info += " 1)"; 

                                sob_info += " (< " ;

                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";

                            }                          
   
                                sob_info += jc.str();
                                sob_info += ") ";

                                //    sob_info += mapsizey.str();
                                sob_info += " 1)"; 

                                // ##########################################################################

                                sob_info += " (> ";

                         
                                //sob_info += "(< (first o" ;
                                //sob_info += sc.str();
                                //sob_info += ") ";

                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboXcoord.str();
                                  sob_info += " ";

                            }                          
          
                                sob_info += ic.str();
                                sob_info += ") ";

                                // stringstream mapsizex ;     
                                //stringstream mapsizey ;     
 
                                //mapsizex << mapx;
                                //mapsizey << mapy;


                                sob_info += mapsizex.str();
                                sob_info += ")"; 
 
                                sob_info += " (> " ;

                            if(ti < 0)
                            {

                                  sob_info += "(- x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";
                            } 
                                
                            else
                            {

                                  sob_info += "(+ x!";
                                  sob_info += blocksRoboYcoord.str();
                                  sob_info += " ";

                            }                            ;


                                sob_info += jc.str();
                                sob_info += ") ";

                                sob_info += mapsizey.str();
                                sob_info += ")"; 

                                sob_info += ")"; 

                                printonce++;
                            }

                            scount++;
                        }

                 } // end of for loop over numblocksrobo       


                        //  fprintf(fpz,"\n      (and (>= x!1 1 ) (<= x!1 mapX) (>= x!2) 1  ) (<= (+ 1 x!2) mapY  )     )\n\n"  );



                        std::string dsa = "" ; 
                        std::string dsb = "" ;
                       
                        int dcount = 3;
                        sob_info += "";

                        while(dcount <= 2 * numRobos)
                        {


                            std::stringstream da ;
                            da << dcount;


                            std::stringstream db ;
                            db << dcount+1;

                            //  sob_info += "         (= false (radar x!1 x!2 x!";
                            // sob_info += da.str();
                            // sob_info += " x!";
                            // sob_info += db.str();
                            // sob_info += ") ) \n" ; 
                            std::stringstream rnge ;
                            rnge << radar_range;

                            sob_info += "\n        (and ";
 
                            sob_info += "(= x!" ;
                            sob_info += da.str();
                             

                            if(ti < 0)
                                sob_info += " (- x!1 ";
                            else
                                sob_info += " (+ x!1 "  ;


                            std::stringstream ic;
                            ic << abs(ti);

                            std::stringstream jc;
                            jc << abs(tj);
                                     
                            sob_info += ic.str();
                            sob_info += ")) ";
                            
                            sob_info += "(= x!" ;
                            sob_info += db.str();
                            //sob_info += ") ";
                         
                            if(tj < 0)
                                sob_info += " (- x!2 ";
                            else
                                sob_info += " (+ x!2 "  ;

                
                            sob_info += jc.str();
                            sob_info += ")) ) ";

                            dcount+=2;
                        }

                    } // I, j, loop
                }
            }  


            ret +=  "\n\n ";
            ret += sob_info.c_str();
            ret += "\n\n) ) true false ))\n\n)  \n\n ";

        }
       
        else
        {

            ret += "\n\n\n false )\n\n)  \n\n ";

        } 
 
        ret += "\n"; 
        defi ++;
    
        //  for(int tempindex=1; tempindex < k+numBlocksRobo;tempindex ++) 
        //      fprintf(fpz,")");

         
    }




    ret += _computeSensorValuesProximity(radar_range);


    return ret;

}







std:: string SMTPrinterVelAcc::_CommandInExecution_And_PhysCounter()
{
   
    std::string ret = "";   

    // Actuation  for robot
    for (int i= 1 ;i<= numRobos ; i++)
    {

        for (int j=-1;j<=T;j++)
        {

            std::string P = "";  

            std::stringstream a ;
            std::stringstream b ;

            a << i;
            b << j;

            P += "Command_In_exec_";
            P += a.str();

            P += "_@_" ;
            P += b.str();

            ret += "(declare-const ";
            ret += P.c_str();
            ret += " String )\n";
                        

        }
  
        ret += "\n\n";
 
    }

    for(int i=1;i<=numRobos;i++)
    {
        std::stringstream roboId;
        roboId << i;

        ret += "\n\n\n(assert (= Command_In_exec_";
        ret += roboId.str();
        ret += "_@_-1 \"null\") )\n\n\n";

    }

    // Actuation delays Physical Counter and Uncertanities
    for (int i= 1 ;i<= numRobos ; i++)
    {

        for (int j=0;j<=T;j++)
        {

            std::string P = "";  

            std::stringstream a ;
            std::stringstream b ;

            a << i;
            b << j;

            P += "Phys_counter_";  // Physical counter for command generated by robot a.str()  at time b.str()
            P += a.str();

            P += "_@_" ;
            P += b.str();

            ret += "\n\n(declare-const ";
            ret += P.c_str();
            ret += " Int )\n" ;
   
            ret += "(assert (ite (= Command_";
            ret += a.str();
            ret += "_@_";
            ret += b.str();
            ret += " \"stop\") (and (>= Phys_counter_" ;           
            ret += a.str();
            ret += "_@_";
            ret += b.str();
            ret += " Min_PC) (<= Phys_counter_";
            ret += a.str();
            ret += "_@_";
            ret += b.str();
            ret +=" Max_PC)) (= Phys_counter_";
            ret += a.str();
            ret += "_@_";
            ret += b.str();
            ret += " 0) ))\n" ;
            
        }
  
        ret += "\n\n";
 

    }

    




    return ret;

}



std:: string SMTPrinterVelAcc::_gtca_for_latest_command()
{
   
    std::string foo = "";  
    foo += "\n\n; gtca_@_ Is Shorthand For Generation Time Of Command Actuated At A Given Time Instance \n\n";

    for(int j=-1;j<=T;j++)
    {
        std::stringstream eval_step;
        eval_step << j;

        foo += "\n(declare-const gtca_@_";
        foo += eval_step.str();
        foo += " Int)";
    
    }

    foo += "\n(assert (= gtca_@_-1 -1))\n\n\n";
  
    return foo; 

}





std:: string SMTPrinterVelAcc::_sensors_In_Effect()
{
   
    std::string effsns = "";  
    
    for (int i= 1 ;i<= numRobos ; i++)
    {

      for (int j=0;j<=T+SnsPropagDelay;j++)
      {

           std::string P = "";  

           std::stringstream a ;
           std::stringstream b ;

           a << i;
           b << j;

           P += "EffectSns_";
           P += a.str();

           P += "_@_" ;
           P += b.str();


          effsns += "\n\n(declare-const ";
          effsns += P.c_str();
 
          if(SensorType == 'P') 
              effsns += " (Sensors Int Int Int Int)) ";

          if(SensorType == 'R') 
              effsns += " (Sensors Bool Int Int Int Int)) ";
        

           
          if(j-SnsPropagDelay < 0)
          {

  
              if(SensorType == 'P')
              {
                  effsns += "\n(assert (and ";
                  effsns += " (= (up ";
                  effsns += P.c_str();
                  effsns += ") -1) (= (down ";
                  effsns += P.c_str();
                  effsns += ") -1) (= (right " ;
                  effsns += P.c_str();
                  effsns += ") -1) (= (left ";
                  effsns += ") -1) )) " ;           

              }  

              if(SensorType == 'R')
              {
                  effsns += "\n(assert (and ";
                  effsns += "(= (rd " ;
                  effsns += P.c_str();
                  effsns += ") false)";
                  effsns += " (= (up ";
                  effsns += P.c_str();
                  effsns += ") -1) (= (down ";
                  effsns += P.c_str();
                  effsns += ") -1) (= (right " ;
                  effsns += P.c_str();
                  effsns += ") -1) (= (left ";
                  effsns += ") -1) )) " ;           

              }


          }

            
          else
          {
              std::stringstream roboId;
              roboId << i;

              std::stringstream delayedSensorValues; 
              delayedSensorValues << j-SnsPropagDelay;

              effsns += "\n(assert (= ";
              effsns += P.c_str();
              effsns += " Sns_";
              effsns += roboId.str();
              effsns +="_@_";
              effsns += delayedSensorValues.str();
              effsns += "))";
 
          }



 
      }
  
      effsns += "\n\n";
 
   }

   effsns += "\n";

   return effsns;
}




std:: string SMTPrinterVelAcc::_generateVelocitySpec()
{
   
    std::string ret = "";  
 
    // Segment : Declaration of variables for robots' velocity
    for(int i=1;i<=numRobos; i++)
    {
        for(int j=-1;j<= T; j++)
        {
            std::stringstream roboId;
            std::stringstream eval_step;

            roboId << i;
            eval_step << j;  

            ret += "\n(declare-const velocity_";
            ret += roboId.str();
            ret += "_@_";
            ret += eval_step.str();
            ret += " Int)" ;

            if(j<=0)
            {
                 ret += "\n(assert (= velocity_";
                 ret += roboId.str();
                 ret += "_@_";
                 ret += eval_step.str();
                 ret += " 0))\n";

            }  
                

        } 

    } 

    for(int i=1;i<=numRobos; i++)
    {
        for(int j=-1;j<= T; j++)
        {
            std::stringstream roboId;
            std::stringstream eval_step;

            roboId << i;
            eval_step << j;  

            ret += "\n(declare-const acc_";
            ret += roboId.str();
            ret += "_@_";
            ret += eval_step.str();
            ret += " Int)" ;

            if(j<0)
            {
                 ret += "\n(assert (= acc_";
                 ret += roboId.str();
                 ret += "_@_";
                 ret += eval_step.str();
                 ret += " 0))\n";

            }  
                

        } 

    } 





    ret += "\n\n";


 
    // Reseting and increase or decrease in velocity

    for(int i=1;i<=numRobos; i++)
    {
        for(int j=1;j<= T; j++)
        {

          std::stringstream roboId;
          std::stringstream eval_step;
          std::stringstream prev_eval_step;
          std::stringstream second_prev_eval_step;

          roboId << i;
          eval_step << j;
          prev_eval_step << j-1;
          second_prev_eval_step << j-2;
           
//           fprintf(fpz, "\n(assert (= velocity_%d_@_%d (+ velocity_%d_@_%d (* acc_%d_@_%d 1)) ))", i, j, i, j-1, i, j-1 ); 

          ret += "\n(assert (= velocity_";
          ret += roboId.str();
          ret += "_@_";
          ret += eval_step.str();
          
          ret += " (+ velocity_";
          ret += roboId.str();
          ret += "_@_";
          ret += prev_eval_step.str();
          ret += " (* acc_";
          ret += roboId.str();
          ret += "_@_";
          ret += prev_eval_step.str();

          ret += " 1)) ))";
 
        }  
    }

    ret += "\n";
    return ret;
 

}



std:: string SMTPrinterVelAcc::_decidingWhatCommandToExecute()
{
   
    std::string foo = "";  

    foo += "\n\n; Efficient Way Of Delayed Executions Of Each Command\n\n";

    for (int j=0;j<=T;j++)
    {
        foo += "\n\n\n\n\n(assert \n\n\n" ;  
 
        int pvCount = 0;
        for(int temp = mpc;temp >= minpc; temp--)
        {
            if(j-temp >= 0) 
            {

                std::stringstream j_val;
                std::stringstream temp_val; 
                std::stringstream j_minus_temp_val;

                j_val << j;
                temp_val << temp;
                j_minus_temp_val << j-temp;

                foo += "\n(ite (and (= Phys_counter_1_@_";
                foo += j_minus_temp_val.str() ;
                foo += " ";
                foo += temp_val.str();
                foo += ") (= Command_1_@_";
                foo += j_minus_temp_val.str();
                foo += " \"stop\") )  (and (= Command_In_exec_1_@_" ;
                foo += j_val.str();
                foo += " \"null\") (= gtca_@_" ;
                foo += j_val.str();
                foo += " ";
                foo += j_minus_temp_val.str() ;
                foo += ")  ) " ;
                pvCount ++;       
            }   

       }
 
 
        int velocity_Criteria;
        if(mpc>0)
        {
              velocity_Criteria = mpc+2;
              // Specify your custom Velocity criteria here !
        }  
          
        else
            velocity_Criteria = mpc;       


           if(mpc > 0)
           { 
              // Local Scope  
              std::stringstream eval_step;
              std::stringstream prev_eval_step;
              std::stringstream velocityCrit;

              eval_step << j;
              prev_eval_step << j-1;
              velocityCrit << velocity_Criteria;


              foo += "\n(ite (and (> velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"up\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (> velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"down\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (> velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"right\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (> velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"left\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
 


           }
           else
           {
              // Local Scope  
              std::stringstream eval_step;
              std::stringstream prev_eval_step;
              std::stringstream velocityCrit;

              eval_step << j;
              prev_eval_step << j-1;
              velocityCrit << velocity_Criteria;


              foo += "\n(ite (and (< velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"up\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (< velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"down\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (< velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"right\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
              foo += "\n(ite (and (< velocity_1_@_";
              foo += eval_step.str();
              foo += " ";
              foo += velocityCrit.str();
              foo += ") (= Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " \"left\") (= \"occupied\" (check_block_status (first Pos_1_@_";
              foo += eval_step.str();
              foo += ") (+ (second Pos_1_@_"  ;
              foo += eval_step.str();
              foo += ") 1 )  )) ) (and (= Command_In_exec_1_@_";
              foo += eval_step.str();
              foo += " Command_In_exec_1_@_";
              foo += prev_eval_step.str();
              foo += " ) (= gtca_@_";
              foo += eval_step.str();
              foo += " gtca_@_";
              foo += prev_eval_step.str();
              foo += ") )";
            
    } 

 
  
   int positivecount = 0;

   for(int innerlc = j;  innerlc  >= j-mpc ; innerlc--)
   {     
        std::string allprevphys = ""; 

        for(int insideinner = j; insideinner >= innerlc ; insideinner --)
        { 

            if(innerlc >= 0 )
            { 
 
                  std::stringstream innerloopcount ;
                  innerloopcount << insideinner;

                  std::stringstream outerloopcount ;
                  outerloopcount << j;

                  if(innerlc == j )
                  {   allprevphys += "(= ( + ";
                              
                      allprevphys += innerloopcount.str();

                      allprevphys += " Phys_counter_1_@_";
                      allprevphys += innerloopcount.str();

                      allprevphys += ") ";
                      allprevphys += outerloopcount.str();
                      allprevphys += ") "; //" (Command_In_exec_1_@_";

                  }
                  else 
                  {
                      if(insideinner == innerlc)
                      {   allprevphys += "( = ( + "; 
                               
                          allprevphys += innerloopcount.str();

                          allprevphys += " Phys_counter_1_@_";
                          allprevphys += innerloopcount.str();

                          allprevphys += ") ";
                          allprevphys += outerloopcount.str();
                          allprevphys += ") "; //" (Command_In_exec_1_@_";
 
                      }
                      else
                      {
                          allprevphys += "(not ( = ( + "; 
                          allprevphys += innerloopcount.str();

                          allprevphys += " Phys_counter_1_@_";
                          allprevphys += innerloopcount.str();

                          allprevphys += ") ";
                          allprevphys += outerloopcount.str();
                          allprevphys += "))  "; //" (Command_In_exec_1_@_";
 
                      }  
                              

                  }

              }

          }    

          if(innerlc>=0)
          {  

             if(innerlc==j)
             {
                   if(innerlc==0)
                   {  
                        // Local Scope 
                        std:: stringstream j_val;
                        std:: stringstream innerlc_val; 
                        
                        j_val << j;
                        innerlc_val << innerlc;

                        foo += "\n(ite  ";
                        foo += allprevphys.c_str();
                        foo += " (and (= Command_In_exec_1_@_";
                        foo += j_val.str();
                        foo += " Command_1_@_";
                        foo += innerlc_val.str();
                        foo += " )  (= gtca_@_"  ;
                        foo += j_val.str();
                        foo += " ";
                        foo += innerlc_val.str() ;
                        foo += ") )     ";

                        positivecount++;
 

                   }
                   else
                   {

                        // Local Scope 
                        std:: stringstream j_val;
                        std:: stringstream innerlc_val; 
                        std:: stringstream prev_j_val;

                        
                        j_val << j;
                        innerlc_val << innerlc;
                        prev_j_val << j-1;
                        positivecount++;
  
                        foo += "\n(ite (and (>= ";
                        foo += innerlc_val.str();
                        foo += " gtca_@_"  ;
                        foo += prev_j_val.str();
                        foo += ") ";
                        foo += allprevphys.c_str();
                        foo += " )     (and (= Command_In_exec_1_@_"   ;
                        foo += j_val.str();
                        foo += " Command_1_@_";
                        foo += innerlc_val.str();
                        foo += " )  (= gtca_@_"  ;
                        foo += j_val.str();
                        foo += " ";
                        foo += innerlc_val.str();
                        foo += ") )    ";

                    }  


             }
             

              else
              {
                  if(innerlc==0)
                  {

                      positivecount++;
 
                      // Local Scope 
                      std:: stringstream j_val;
                      std:: stringstream innerlc_val; 
                        
                      j_val << j;
                      innerlc_val << innerlc;

                      foo += "\n(ite  (= true (and  ";
                      foo += allprevphys.c_str();
                      foo += " ))  (and (= Command_In_exec_1_@_";
                      foo += j_val.str();
                      foo += " Command_1_@_";
                      foo += innerlc_val.str();
                      foo += " ) (= gtca_@_";
                      foo += j_val.str();
                      foo += " ";
                      foo += innerlc_val.str();
                      foo += ")   ) ";

                  }
                  else
                  {
                      positivecount++;  
  
                      // Local Scope 
                      std:: stringstream j_val;
                      std:: stringstream innerlc_val; 
                      std:: stringstream prev_j_val;

                        
                      j_val << j;
                      innerlc_val << innerlc;
                      prev_j_val << j-1;

                      foo += "\n(ite (and (>= ";
                      foo += innerlc_val.str();
                      foo += " gtca_@_";
                      foo += prev_j_val.str();
                      foo += ")  (= true (and  ";
                      foo += allprevphys.c_str();
                      foo += " ))  )  (and (= Command_In_exec_1_@_";
                      foo += j_val.str();
                      foo += " Command_1_@_";
                      foo += innerlc_val.str();
                      foo += " ) (= gtca_@_";
                      foo += j_val.str();
                      foo += " ";
                      foo += innerlc_val.str();
                      foo += ")   )  ";
 
                  } 
              }  
                 
           }
 

     } 


    // Checking if no command is available to execute at the given time instance 
    std::string noCommand = "";
      for(int insideinner = j; insideinner >= j-mpc ; insideinner --)
            { 

                 if(insideinner >= 0 )
                 { 
   
                        std::stringstream innerloopcount ;
                        innerloopcount << insideinner;

                        std::stringstream outerloopcount ;
                        outerloopcount << j;
                        
                        noCommand += "(not (= ( + ";
                        noCommand += innerloopcount.str();
                        noCommand += " Phys_counter_1_@_";
                        noCommand += innerloopcount.str();
                        noCommand += ") ";
                        noCommand += outerloopcount.str();
                        noCommand += ")) "; //" (Command_In_exec_1_@_";

                  }

           }

  

           if(j>=1)
           {
                    // Local Scope
                    std::stringstream j_val;
                    std::stringstream prev_j_val;

                    j_val << j;
                    prev_j_val << j-1;
                     
                    foo += "\n(and (= Command_In_exec_1_@_";
                    foo += j_val.str() ;
                    foo += " \"pseudonull\" )  (= gtca_@_";
                    foo += j_val.str() ;
                    foo += " gtca_@_";
                    foo += prev_j_val.str();
                    foo += " )   ) ";


           }   

              
           else
           {
                  // Local Scope
                  std::stringstream j_val;
                  j_val << j;
                    
                  foo += "\n(and (= Command_In_exec_1_@_";
                  foo += j_val.str()  ;
                  foo += " \"pseudonull\" )  (= gtca_@_";
                  foo += j_val.str()  ;
                  foo += " ";
                  foo += j_val.str()  ;
                  foo += " )   ) ";

            } 
               

 

 
          for(int i=1;i<=positivecount;i++)
              foo += ")" ;
     

          for(int tempvar = 1;tempvar <= pvCount; tempvar++)
                foo += ")" ;
 

          foo +=  "  )";
          foo += "  ))))\n\n\n\n"; 

 
}


 
 


/* 

//  Earlier implememntation 

fprintf(fpz,"\n\n; Delayed Executions Of Each Command\n\n");


for (j=0;j<=T;j++)
{

    for (int pk=0;pk<=mpc;pk++)
    {
           if(j+pk <= T){  

           string P = "";  

           stringstream a ;
           stringstream b ;


           b << j;

           P += "Phys_counter_1_@_";  // Physical counter for command generated by robot a.str()  at time b.str()
           P += b.str();


//           fprintf(fpz,"\n(assert (implies (= Phys_counter_1_@_%d %d )  (= Command_1_@_%d Command_In_exec_1_@_%d  )) ) ", j,pk, j, j+pk );     




          string laterCommands = "";
          
 
          for(int cc = j+1; cc <= j+mpc; cc++ )    // cc stands for command count
          {
               if(cc <= T)
               { 
                
                 stringstream latercom ;
                 latercom << cc;

                 stringstream thiscomtime ;
                 thiscomtime << j;


                 laterCommands += " (<= (+ ";
                 laterCommands += latercom.str();
                 laterCommands += " Phys_counter_1_@_";
                 laterCommands += latercom.str();
                 laterCommands += ") "; 
                 laterCommands +=  " (+ ";
                 laterCommands += thiscomtime.str();
                 laterCommands += " Phys_counter_1_@_";
                 laterCommands += thiscomtime.str();
                 laterCommands += ") )";
             }  

          } 

       
     if(j<T)
         fprintf(fpz,"\n\n(assert (ite (= true (and (= Phys_counter_1_@_%d %d)  (= false (or %s ) ) ) ) (and (= Something_to_exec_1_@_%d true)   (= Command_1_@_%d Command_In_exec_1_@_%d ) )    (= 1 1 )  ))  ", j, pk, laterCommands.c_str(), j+pk,j, j+pk );
     if(j==T)
         fprintf(fpz,"\n\n(assert (ite (= Phys_counter_1_@_%d %d)  (and (= Something_to_exec_1_@_%d true)  (= Command_1_@_%d Command_In_exec_1_@_%d )) (= 1 1 )  ))  ", j, pk, j+pk, j, j+pk );
      




//          (assert (ite (= true (and (= Phys_counter_1_@_0 10)  (or (<=  1 + phys@ 1 10)  ()          )           )      )                            ))




//     fprintf(fpz,"(assert (ite (= true (and (= Phys_counter_1_@_%d %d)  (or %s ) ) ) (= Command_1_@_%d Command_In_exec_1_@_%d ) (= Command_In_exec_1_@_%d \"null\" )", j pk, laterCommands.c_str(), j, j+pk, j+pk  );
 
 




          }
          
    }

    fprintf(fpz, "\n\n");

}
  


fprintf(fpz,"\n\n\n\n");

 

for (j=0;j<=T;j++)
{

    fprintf(fpz,"\n(assert (ite (not (= Something_to_exec_1_@_%d true)) (= Command_In_exec_1_@_%d \"null\") (= 1 1)      ))", j, j );  

}




fprintf(fpz,"\n\n\n\n");
 



//###################################################################################################################
 
for (j=0;j<=T;j++)
{

   if(j==0)
        fprintf(fpz,"\n(assert (implies ( = false (or  (= Command_In_exec_1_@_0 \"up\") (= Command_In_exec_1_@_0 \"down\") (= Command_In_exec_1_@_0 \"right\") (= Command_In_exec_1_@_0 \"left\") )) (= Command_In_exec_1_@_0 \"null\") )) " );     
   else 
        fprintf(fpz,"\n(assert (implies ( = false (or  (= Command_In_exec_1_@_%d \"up\") (= Command_In_exec_1_@_%d \"down\") (= Command_In_exec_1_@_%d \"right\") (= Command_In_exec_1_@_%d \"left\") )) (= Command_In_exec_1_@_%d Command_In_exec_1_@_%d ) )) ", j, j, j, j, j, j-1 );     



}
 

 
//###################################################################################################################

  timeClause off
for (j=0;j<=T;j++)
{



    
     string timeclause = ""; 


     for(int loopc = 0; loopc <= j; loopc++ )  
     {
              stringstream lpc ;
              lpc << loopc;

              stringstream outloopc ;
              outloopc << j;



              timeclause += " (=  (+ "; 
              timeclause += lpc.str();

              timeclause += " Phys_counter_1_@_";
              timeclause += lpc.str();
              
              timeclause += ") ";
              timeclause += outloopc.str();
              timeclause += " )";


     }
 
 fprintf(fpz, "\n\n(assert (implies (= false (and  %s ))  (= Command_In_exec_1_@_%d \"null\" )  ))  ", timeclause.c_str(), j);




}
  
 
   for (i= 1 ;i<= numRobos ; i++)
   {

      for (j=1;j<=T;j++)
      {

           string P = "";  

           stringstream a ;
           stringstream b ;

           a << i;
           b << j;

           P += "Targ_";
           P += a.str();

           P += "_x_" ;
           P += b.str();


          fprintf(fpz, "(declare-const %s Int )\n", P.c_str() );


      }

  fprintf(fpz, "\n\n");
 

   }







   fprintf(fpz, "\n");

   for (i= 1 ;i<= numRobos ; i++)
   {

      for (j=1;j<=T;j++)
      {

           string P = "";  

           stringstream a ;
           stringstream b ;

           a << i;
           b << j;

           P += "Targ_";
           P += a.str();

           P += "_y_" ;
           P += b.str();


          fprintf(fpz, "(declare-const %s Int )\n", P.c_str() );


      }


     fprintf(fpz, "\n\n");

   }

*/

    return foo;
}


std::string SMTPrinterVelAcc:: _getSensorOutputUnderDelays()
{

    std::string foo = "";

    std::string funpar2 ;
    std::string Fa;

    //int kk ;
    std::string Sensa;

    std::string Fb;
    std::string Sensb;


    for(int i=1;i<=T+1;i++)
    {
        std::string funpar2 = "";
        

        for(int j=1;j<=numRobos; j++)
        {
        
            Fa = "";     
            Sensa = "";     

            Fb = "";     
            Sensb = "";     


            std::stringstream efa ;
            std::stringstream efb ;

            std::stringstream sefa ;
            std::stringstream sefb ;

     
            efa << i-1;
            efb << j;

            sefa << i-1;
            sefb << j;

            std::string effa = efa.str()  ;
            std::string effb = efb.str();

            std::string seffa = sefa.str()  ;
            std::string seffb = sefb.str();


            Fa += "(first Pos_";
            Fa += effb ;
            Fa += "_@_";


            Fa += effa;
            Fa += ") " ;


            Fb += "(second Pos_";
            Fb += seffb ;
            Fb += "_@_";

            Fb += seffa;
            Fb += ") " ;


            Sensa += "Sns_";
            Sensa += effb ;
            Sensa += "_@_";


            Sensa += effa;
            //Sensa += ") " ;


            Sensb += "Sns_";
            Sensb += seffb ;
            Sensb += "_@_";

            Sensb += seffa;
            //Sensb += ") " ;
        

            Fa += "(second Pos_";
            Fa += effb ;
            Fa += "_@_";

            Fa += effa;
            Fa += ") " ;


            Fb += "(first Pos_";
            Fb += seffb ;
            Fb += "_@_";


            Fb += seffa;
            Fb += ") " ;


            for(int kk=1;kk<=numBlocksRobo; kk++)
            {

                if(kk!=j){
                    std::stringstream fa ;
                    std::stringstream fb ;
     
                    fa << i-1;
                    fb << kk;

                    std::string ffa = fa.str()  ;
                    std::string ffb = fb.str();

                    Fa += "(first Pos_";
                    Fa += ffb ;
                    Fa += "_@_";


                    Fa += ffa;
                    Fa += ") " ;
               

                    Fa += "(second Pos_";
                    Fa += ffb ;
                    Fa += "_@_";


                    Fa += ffa;
                    Fa += ") " ;


                    Fb += "(second Pos_";
                    Fb += ffb ;
                    Fb += "_@_";


                    Fb += ffa;
                    Fb += ") " ;
               

                    Fb += "(first Pos_";
                    Fb += ffb ;
                    Fb += "_@_";


                    Fb += ffa;
                    Fb += ") " ;

                }
  
            }


            std::stringstream SensorRange_Val;
            SensorRange_Val << SensorDistance;


            // Computing Sensor Values
            if(SensorType == 'S'  ||   SensorType == 'P' )
            {    
                foo += "(assert (= (up ";
                foo += Sensa.c_str();
                foo += ") (det_up ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (down ";
                foo += Sensa.c_str();
                foo += ") (det_down ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (right ";
                foo += Sensa.c_str();
                foo += ") (det_right ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (left ";
                foo += Sensa.c_str();
                foo += ") (det_left ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

            }

            else
            {


                foo += "\n\n\n\n(assert (= (rd ";
                foo += Sensa.c_str();
                foo += ")  (det_radar \"rd\" ";
                foo += Fa.c_str();
                foo += ") ) )"; 

     
                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (up "; 
                foo += Sensa.c_str();
                foo += ") ";
                foo += SensorRange_Val.str();
                foo += ") ))";
                 foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (up ";          
                foo += Sensa.c_str();
                foo += ")  (det_up ";
                foo += Fa.c_str();
                foo += ") ) ))"; 

                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (down "; 
                foo += Sensa.c_str();
                foo += ") ";
                foo += SensorRange_Val.str();
                foo += ") ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (down ";          
                foo += Sensa.c_str();
                foo += ")  (det_down ";
                foo += Fa.c_str();
                foo += ") ) ))"; 


                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (right "; 
                foo += Sensa.c_str();
                foo += ") ";
                foo += SensorRange_Val.str();
                foo += ") ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (right ";          
                foo += Sensa.c_str();
                foo += ")  (det_right ";
                foo += Fa.c_str();
                foo += ") ) ))"; 

                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (left "; 
                foo += Sensa.c_str();
                foo += ") ";
                foo += SensorRange_Val.str();
                foo += ") ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (left ";          
                foo += Sensa.c_str();
                foo += ")  (det_left ";
                foo += Fa.c_str();
                foo += ") ) ))"; 







            }   
        }   
   
        foo += "\n\n";
    }

    return foo;
}    



std::string SMTPrinterVelAcc:: _getCommandGeneratedUnderDelays()
{

    std::string ret = "";

    for(int i=0;i<T;i++)
    {

      for(int j=1;j<=numRobos;j++)
      {
         
         std::string scom = "";
         std::string sns = "";
         std::string spos = "";
         std::string starg = "";
         
         std::stringstream aj ;
         std::stringstream ai;

         std::stringstream comai;
     

         aj << j;
         ai << i;
         comai << i+SnsPropagDelay;

         scom += "Command_"; 
         scom += aj.str();
         scom +=  "_@_";
         scom += ai.str();


         sns += "EffectSns_"; 
         sns += aj.str();
         sns +=  "_@_";
         sns += comai.str();

         spos += "Pos_"; 
         spos += aj.str();
         spos +=  "_@_";
         spos += ai.str();

         starg += "Targ_"; 
         starg += aj.str();
         starg +=  "_@";
          

         std::stringstream prev_ai;
         prev_ai << i-1;

         ret += "(assert (= ";
         ret += scom.c_str();
         ret += " ( Mel_direction velocity_";
         ret += aj.str();
         ret += "_@_";
         ret += prev_ai.str();
         ret += " Command_";
         ret += aj.str();
         ret += "_@_";
         ret += prev_ai.str();
         ret += "  ";

         if(SensorType == 'R')  
         {
              ret += " (rd ";
              ret += sns.c_str();
              ret += ")";
         }


         ret += " (up ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (down ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (right ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (left ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (first ";
         ret += spos.c_str();
         ret += ") ";

         ret += " (second ";
         ret += spos.c_str();
         ret += ") ";

         ret += " (first ";
         ret += starg.c_str();
         ret += ") ";

         ret += " (second ";
         ret += starg.c_str();
         ret += ") ";
         
         ret += "  ) ) )\n";
   
        //####################################

         ret += "(assert (= ";
         ret += "acc_";
         ret += aj.str();
         ret += "_@_";
         ret += ai.str();

         ret += " ( Mel_acceleration_magnitude velocity_";
         ret += aj.str();
         ret += "_@_";
         ret += prev_ai.str();
         ret += " Command_";
         ret += aj.str();
         ret += "_@_";
         ret += prev_ai.str();
         ret += "  ";

         if(SensorType == 'R')  
         {
              ret += " (rd ";
              ret += sns.c_str();
              ret += ")";
         }


         ret += " (up ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (down ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (right ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (left ";
         ret += sns.c_str();
         ret += ") ";

         ret += " (first ";
         ret += spos.c_str();
         ret += ") ";

         ret += " (second ";
         ret += spos.c_str();
         ret += ") ";

         ret += " (first ";
         ret += starg.c_str();
         ret += ") ";

         ret += " (second ";
         ret += starg.c_str();
         ret += ") ";
         
         ret += "  ) ) )\n";
   
 






















    }

    ret += "\n";

   } 


    return ret;
}




std::string SMTPrinterVelAcc:: _getCommandActionsUnderDelays()
{
    // Semantics of the Command i.e. the actions to be performed
    std::string sem = "";

    //##################################################################################################
    for(int i=0;i<T;i++)
    {

      for(int j=1;j<=numRobos;j++)
      {

         std::stringstream aj ;
         std::stringstream ai;
         aj << j;
         ai << i;
         
         std::stringstream posai ;
         posai << i+1;
              
         std::stringstream posaiprev ;
         posaiprev << i+1;

         std::string scom = "";

         std::string spos = "";
         std::string sposprev = "";

         std::string targpos = "";
         

         if(j==1)
             scom += "Command_In_exec_";
         else
             scom += "Command_"; 


         scom += aj.str();
         scom +=  "_@_";
         scom += ai.str();

         spos += "Pos_"; 
         spos += aj.str();
         spos +=  "_@_";
         spos += ai.str();


         sposprev += "Pos_"; 
         sposprev += aj.str();
         sposprev +=  "_@_";
         sposprev += posaiprev.str();


         targpos += "Targ_"; 
         targpos += aj.str();
         targpos +=  "_@";
         

         sem += "(assert (implies (and  (= ";
         sem += scom.c_str();
         sem += " \"up\") (distinct ";
         sem += spos.c_str();
         sem += " ";
         sem += targpos.c_str();
         sem += ")) (and ";


        for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
        {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream eval_step;
            std::stringstream next_eval_step;

            sem += "\n     (and (= (first Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += next_eval_step.str();
            sem += ")";      
            sem += "(first Pos_" ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") ) (= (second Pos_"  ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") (+ velocity_1_@_";
            sem += eval_step.str();
            sem += " (second Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += "))) )";

 
        }  

            sem += "\n)))\n\n";



         sem += "(assert (implies (and  (= ";
         sem += scom.c_str();
         sem += " \"down\") (distinct ";
         sem += spos.c_str();
         sem += " ";
         sem += targpos.c_str();
         sem += ")) (and ";


        for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
        {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream eval_step;
            std::stringstream next_eval_step;

            sem += "\n     (and (= (first Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += next_eval_step.str();
            sem += ")";      
            sem += "(first Pos_" ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") ) (= (second Pos_"  ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") (- velocity_1_@_";
            sem += eval_step.str();
            sem += " (second Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += "))) )";

 
        }  

            sem += "\n)))\n\n";


 

         sem += "(assert (implies (and  (= ";
         sem += scom.c_str();
         sem += " \"right\") (distinct ";
         sem += spos.c_str();
         sem += " ";
         sem += targpos.c_str();
         sem += ")) (and ";


        for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
        {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream eval_step;
            std::stringstream next_eval_step;

            sem += "\n     (and (= (second Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += next_eval_step.str();
            sem += ")";      
            sem += "(second Pos_" ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") ) (= (first Pos_"  ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") (+ velocity_1_@_";
            sem += eval_step.str();
            sem += " (first Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += "))) )";

 
        }  

            sem += "\n)))\n\n";


         sem += "(assert (implies (and  (= ";
         sem += scom.c_str();
         sem += " \"left\") (distinct ";
         sem += spos.c_str();
         sem += " ";
         sem += targpos.c_str();
         sem += ")) (and ";


        for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
        {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream eval_step;
            std::stringstream next_eval_step;

            sem += "\n     (and (= (second Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += next_eval_step.str();
            sem += ")";      
            sem += "(second Pos_" ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") ) (= (first Pos_"  ;
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += ") (- velocity_1_@_";
            sem += eval_step.str();
            sem += " (first Pos_";
            sem += val_tsi.str();
            sem += "_@_"  ;
            sem += eval_step.str();
            sem += "))) )";

 
        }  

            sem += "\n)))\n\n";






         sem += "(assert (implies (or   (= ";
         sem += scom.c_str();
         sem += " \"null\" ) (= ";
         sem += scom.c_str();
         sem += " \"pseudonull\" ) ";

         for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
         {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream next_eval_step;
             

             sem += "\n        (= Pos_";
             sem += val_tsi.str()  ;
             sem += "_@_";
             sem += next_eval_step.str();
             sem += " Targ_1_@)" ;
 
         }  



        sem += ")\n\n        (and   \n" ;

        for(int tsi = 1; tsi <= numBlocksRobo; tsi ++ )
        {
            std::stringstream val_tsi ;
            val_tsi << tsi;   

            std::stringstream eval_step;
            std::stringstream next_eval_step;

            sem += "\n        (= Pos_";
            sem += val_tsi.str()  ;
            sem += "_@_";
            sem += next_eval_step.str();
            sem += " " ;
            sem += "Pos_";
            sem += val_tsi.str()  ;
            sem += "_@_";
            sem += eval_step.str();
            sem += ")";

 
        }  


        sem += ") \n ))   \n" ;



         

     }

      sem +="\n\n";
  }

   return sem;
}