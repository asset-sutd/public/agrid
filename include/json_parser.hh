/**
 * @author      <a href="mailto:michele_lora@sutd.edu.sg">Michele Lora</a>
 * @date        27th March 2019
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file	include/json_parser.hh
 * @brief   Header of the JSON parser for the Mealy Machine models.
 */


#pragma once

#include "fsm/mealy_machine.hh"
#include "json.hpp"

#include <iostream>
#include <fstream>

namespace roboverify
{
    class JSONParser
    {
        public:

            static fsm::MealyMachine * parseMealyMachine( 
                    std::string json_file );

        protected:
            JSONParser(){}

            static void _createVariables( 
                    fsm::MealyMachine * machine,
                    nlohmann::json env, 
                    nlohmann::json sys );

            static void _createStates(
                    fsm::MealyMachine * machine,
                    nlohmann::json states );

            static fsm::MealyState * _createState( 
                    fsm::MealyMachine * machine,
                    std::string name,
                    nlohmann::json state );

            static void _createTransitions(
                    fsm::MealyMachine * machine,
                    nlohmann::json states );

    };
}
