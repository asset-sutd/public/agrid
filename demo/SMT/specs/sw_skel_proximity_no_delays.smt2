; ##### Robot Software - Melay Machines ( FSM With Output)

(define-fun Mel ( (prevCommand String) (s1 Int) (s2 Int) (s3 Int) (s4 Int)   (pos_x Int) (pos_y Int) (targ_x Int) (targ_y Int)) String
 (ite ( and (not(= prevCommand "down" )) (>= s1 1) (> targ_y pos_y) )  "up" 
 (ite ( and (not(= prevCommand "left" )) (>= s3 1) (>= targ_x pos_x) (> targ_y pos_y) )  "right" 
 (ite ( and (not(= prevCommand "right" )) (>= s4 1) (<= targ_x pos_x) (> targ_y pos_y) ) "left"

 (ite (and  (not(= prevCommand "up" ))  (>= s2 1) (< targ_y pos_y) ) "down"
 (ite ( and (not(= prevCommand "left" )) (>= s3 1) (>= targ_x pos_x) (< targ_y pos_y) )  "right" 
 (ite ( and (not(= prevCommand "right" )) (>= s4 1) (<= targ_x pos_x) (< targ_y pos_y) ) "left" 

 (ite (and (not(= prevCommand "left" ))  (>= s3 1)  (> targ_x pos_x)   ) "right"
 (ite (and (not(= prevCommand "down" ))  (>= s1 1) (>= targ_y pos_y)  (> targ_x pos_x)   ) "up"
 (ite (and (not(= prevCommand "up" ))  (>= s2 1) (<= targ_y pos_y) (> targ_x pos_x)   ) "down"

 (ite (and (not(= prevCommand "right" ))  (>= s4 1)  (< targ_x pos_x)   ) "left"
 (ite (and (not(= prevCommand "down" ))  (>= s1 1) (>= targ_y pos_y) (< targ_x pos_x)   ) "up"
 (ite (and (not(= prevCommand "up" ))  (>= s2 1) (<= targ_y pos_y) (< targ_x pos_x)   ) "down"  "null"  

 ))) ))) )))  ))))

 
   
 
