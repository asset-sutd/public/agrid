/**
 * @author      <a href="mailto:2016csy0004@iitrpr.ac.in">Amit Behal</a>
 * @date        16th August 2018
 * @copyright   Copyright (c) Singapore University of Technology and Design.\n
 *              All rights reserved.\n
 *              This project is released under the 3-Clause BSD License.
 *
 * @file    src/smt_printer_velacc_model.cc
 * @brief   Source for the printer.
 */

#include "smt_printer_velacc_model.hh"
#include <bits/stdc++.h>
#include<cstring>

using namespace SMT_extended;


SMTPrinterVelAcc::SMTPrinterVelAcc( VerificationProblem * p ) :
    _problem(p),
    _parameters(p->getParameters())
{

    // Occupancy Space size Input
    // std::cout<<"Enter Map Dimensions\n\n";
    //  std::cin>> mapx;
    //  std::cin>> mapy; 



    mapx  = _problem->getParameters()->width;
    mapy = _problem->getParameters()->height;

    MapWidth << mapx;
    MapHeight << mapy;


    // Number of robots - More than 1 for Modelling Dynamic Obstacles
       
    //std::cout << "\n\nEnter bnumber of robots\n\n";
    //std::cin >> numRobos;
    numRobos = 1;

    numBlocksRobo = _problem->getParameters()->robotsize;


    // Opening Static Obstacles File
    FILE * sob;
    sob = std::fopen("./demo/SMT/simple_three_obstacles/static_obstacles.txt","r");



    // k := Number of Static Obstacles
    std::fscanf(sob, "%d\n", &k );     
    std::fclose(sob); 



 
    // Input which sensor abstarction to be modeled
    //std::cout << "Model with Sensor Type ? (S - Standarad, P - Proximity, R - Radar)*\n\n";
    //std::cin >> SensorType;

    if(_problem->sensors[0]->type == "standard")
    {
        SensorType = 'S';
        SensorDistance = _problem->sensors[0]->length + 1;


    }    

    if(_problem->sensors[0]->type == "proximity")
    {
        SensorType = 'P';
        SensorDistance = _problem->sensors[0]->length;
        
    }    

    if(_problem->sensors[0]->type == "radar")
    {
        SensorType = 'R';
        SensorDistance = _problem->sensors[0]->length;
        
    }    



    // Input the sensor range 
    //if(SensorType == 'P' || SensorType == 'R')
    //{
      //  std::cout << "Enter the maximum sensing distance\n\n";
        //std::cin >> SensorDistance;
    //}   


    //std::cout << "\n\nModel Scenario (P - Positive or N - Negative) ?*\n";
    //std::cin >> modelScenario;

    modelScenario = _problem->getParameters()->model_scenario ;


   //    std::cout << "\n\nModel Sensor and Actuation Delays with Uncertanities.? (Y/n) ?*\n";
   // std::cin >> if_model_sns_act_delays;

    if_model_sns_act_delays = _problem->getParameters()->model_delays_uncertanities ;
    

    if(if_model_sns_act_delays == 'n')
    {
             std::cerr << "\n\nErr: Inappropriate Abstractions Chosen as Velocity-Acc Model Requires Modelling Actuation Delays\n\n";
             std::exit(0);

    }

    
    if(if_model_sns_act_delays == 'Y')
    {
        if(SensorType == 'S')
        {
             std::cerr << "\n\nErr: Inappropriate Sensor Abstraction Chosen For Modelling Actuation Delays\n\n";
             std::exit(0);

        }    
            

        //    // Modelling Sensor Propagation Delays
        //    std::cout<<"\n\nEnter the sensors propoagation Delay\n";
        //    std::cin >> SnsPropagDelay;

        SnsPropagDelay = _problem->getParameters()->sensor_prop_delays ;
       

        // Modelling Actuation Delays
        //std::cout<<"\n\nEnter the Minimum value of Physical Counter\n\n";
        //std::cin >> minpc;

        //std::cout<<"\n\nEnter the Maximum Physical Counter value\n\n";
        //std::cin >> mpc;
  
        minpc = _problem->getParameters()->min_phys_counter ;
        mpc = _problem->getParameters()->max_phys_counter ;
        

        if(modelScenario == 'P')
            robot_stop_criteria = "Max_PC"; 
        else
            robot_stop_criteria = "(- Max_PC 2)";  
  
    }
  
    // Time Limit to complete the mission i.e. bound in terms of number of steps
     //   std::cout << "Enter the timeout\n" ;
    //std::cin >> T;

    T = _problem->getParameters()->time_bound ;
     

    if(if_model_sns_act_delays == 'Y')
    {

       // new Senfors propagation delay 
       T -= SnsPropagDelay;
    }   



}


SMTPrinterVelAcc::~SMTPrinterVelAcc()
{
}

void  SMTPrinterVelAcc::printSMTFile()
{


        /*
                Generate SMTLIB 2.0 Code
 
                This function acts as an interface for calling all the 
                external/ internal functions that are required to be 
                invoked for auto generation of SMT specifications from 
                the user provided problem definition.
                     
        */

 
    // Code Output File
    std::ofstream outfile;
    outfile.open(_parameters->fileOut);    
    


    // Print initial part: Important Structures Declarations and Specifying Static Obstacles.
    outfile << _getScenario(); 
    // Get The Navigation Software 
    
    //int range_val;
    //if (SensorType == 'S')
     //   range_val = 1;
    //else
    //    range_val = SensorDistance;

    outfile << _getSoftwareSkeleton("./demo/SMT/specs/" + _problem->getParameters()->controller); 

  
    // Procedures : Computing the Sensors Values
    switch(SensorType)
    {
        // case 'S':
        //     outfile << _computeSensorValuesStandard(); 
        //     break;

         case 'P':
             outfile << _computeSensorValuesProximity(SensorDistance); 
             break;

         case 'R':
             outfile << _computeSensorValuesRadarUnderDelays(SensorDistance);
             break;

         default: 
             std::cerr << "\nErr: Inavlid Sensor Type !\n\n"  ;

    }  


    // Declaration of variables for robots' positions at different times
    outfile << _robotPositionVars(); 

    // Declaration of Command Output Variables
    outfile << _generatedCommandVars(); 


    // Call To external Actuation Delays Function
    if(if_model_sns_act_delays == 'Y')
    {
        outfile << _CommandInExecution_And_PhysCounter(); 
        outfile << _gtca_for_latest_command();

    }



    // Variables for Robots' targets 
    outfile << _getTargetVars(); 

    // Declaration of Variables for tuple of Sensor Values
    outfile << _getSensorTupleVars(); 

    if(if_model_sns_act_delays == 'Y')
    {

        outfile << _sensors_In_Effect();
        outfile << _generateVelocitySpec();
        outfile << _decidingWhatCommandToExecute(); 


    }


    // ##################################################
    // Get robots' Initial Positions 
    if(modelScenario == 'P')
        outfile << _getInitialPos(); 


    // ################################################## 
    // Call to Procedures for Computing the Sensor Values
    if(if_model_sns_act_delays == 'n') 
        outfile << _getSensorOutput(); 
    else
        outfile << _getSensorOutputUnderDelays();



    // ##################################################
    // Get the Command Generated by Navigation Software
    if(if_model_sns_act_delays == 'n')
        outfile << _getCommandGenerated(); 
    else
        outfile << _getCommandGeneratedUnderDelays()  ;


    // ##################################################
    // Semantics of the Command Recieved
    if(if_model_sns_act_delays == 'n')
        outfile << _getCommandActions(); 
    else
        outfile << _getCommandActionsUnderDelays();        




    // ##################################################
    // Get safety specifcations
    outfile << _getSafetySpec();


    // ##################################################
    //SMT Verification of the negation to find bugs
    outfile << _verifySpecNegation(); 

    // Checking satisfiability and genearting Model
    outfile << _generateModel(); 


    outfile.close();



}



std::string SMTPrinterVelAcc::_getScenario()
{

    std::string ret = "";


    ret += "(echo \"Z3 Auto Generated SMT code\")\n\n";

    ret += "; ##### Sensors Data Structure\n";

    if(SensorType == 'R')
        ret += "(declare-datatypes (a b c d e) (( Sensors(lst (rd a) (up b) (down c) (right d) (left e)    ))))\n\n";
    else    
        ret += "(declare-datatypes (a b c d) (( Sensors(lst (up a) (down b) (right c) (left d) ))))\n\n";


    ret += "\n; ##### Data Structure For Robots Positions\n";
    ret += "(declare-datatypes (a b) (( p(lst (first a) (second b)  ))))\n\n\n";

    ret += "; ##### Map Size\n\n(declare-const mapX Int )\n(declare-const mapY Int )\n";

 
     /* int mapx ;
       int mapy;

       Map Dimensions Input
       std::cout<<"Enter Map Dimensions\n\n";
       std::cin>> mapx;
       std::cin>> mapy; 


       std::stringstream MapWidth;  
       std::stringstream MapHeight;

       MapWidth  << mapx;
       MapHeight << mapy;

    */

    ret += "(assert (= ";
    ret += MapWidth.str();
    ret += " mapX ))\n";         
 

    ret += "(assert (= ";
    ret += MapHeight.str();
    ret += " mapY ))\n\n\n; ######## k - Static Obstacles\n";

    

    // Informtion of Static Obstacles
    std::vector < std::pair <int, int> > StaticObstacles;

    // Reading the static obstacle info file...;

    FILE * sob;
    sob = std::fopen("./demo/SMT/simple_three_obstacles/static_obstacles.txt","r");


    int IgnoreVar;   // Already stored in k i.e. number of static obstacles          
    int count = 1;  //  Flag Variable 
    int a,b;        


    // Assertions on Static Obstacles
    while(!feof(sob))
    {

        if(count == 1 )
        {
            fscanf(sob, "%d\n", &IgnoreVar);
        }   

        fscanf(sob, "%d %d\n", &a,&b ) ;
        StaticObstacles.push_back( std::make_pair(a,b) );
      
        count++;
    }     


    std::string ObsVarname ;  // Obstacle Variables' Name e.g. O1, O2 etc.
   

    for(int obsCount=0;obsCount < k ; obsCount++)
    {
        
        ObsVarname = "";
        std::stringstream kn ;
        kn << obsCount+1;
        std::string kname = kn.str();

        ObsVarname = "o" + kname;
    
        ret += "(declare-const ";
        ret += ObsVarname;
        ret += " (p Int Int ) )\n";
      
        // Declare stringstreams   
        std::stringstream obsCoord1 ;
        std::stringstream obsCoord2 ;

        obsCoord1 << StaticObstacles[obsCount].first;
        obsCoord2 << StaticObstacles[obsCount].second;

        ret += "(assert (and (= (first ";
        ret += ObsVarname;
        ret += ") ";
        ret += obsCoord1.str();
        ret += ")  (= (second ";
        ret += ObsVarname;
        ret += ") ";
        ret += obsCoord2.str();
        ret += ")   )  )\n";


    }   

    ret += "\n";

    fclose(sob);

    
    if(if_model_sns_act_delays == 'Y')
    {    
        std::stringstream min_val_phys_counter;
        std::stringstream max_val_phys_counter;

        min_val_phys_counter << minpc;
        max_val_phys_counter << mpc;

        ret += "\n\n(declare-const Min_PC Int)\n"; 
        ret += "(assert (= Min_PC ";
        ret += min_val_phys_counter.str();
        ret += "))\n";

        ret += "\n\n(declare-const Max_PC Int)\n"; 
        ret += "(assert (= Max_PC ";
        ret += max_val_phys_counter.str();
        ret += "))\n";
    }





    return ret; 
}



std::string SMTPrinterVelAcc:: _robotPositionVars()
{

    std::string str = "";

    // Time bound in trems of number of steps
    std::stringstream timeout_val;
    timeout_val << T;


    str += "\n\n (declare-const T Int ) ";
    str += "\n (assert (= T ";
    str += timeout_val.str();
    str += " ) ) \n\n";

    int i,j;

//    std::cout << "\n\n" <<  numBlocksRobo << "\n\n" ;
    
    for (i= 1 ;i<= numBlocksRobo ; i++)
    {

        for (j=0;j<=T;j++)
        {

            std::string P = "";  

            std::stringstream a ;
            std::stringstream b ;

            a << i;
            b << j;

            P += "Pos_";
            P += a.str();

            P += "_@_" ;
            P += b.str();


            str += "(declare-const ";
            str += P.c_str();
            str += " (p Int Int) )\n";
        
            if(j==0)  
            {

                str += "(assert  (and (>= (first ";
                str += P.c_str();
                str += " ) 1 ) (<= (first ";
                str += P.c_str();
                str += ") ";
                str += MapWidth.str();

                str += " )  (>= (second ";
                str += P.c_str();
                str += " ) 1 ) (<= (second ";
                str += P.c_str();
                str += ") ";
                str += MapHeight.str();
                str += " )    ) )\n";


            }

        }
  
        str += "\n\n";
 

    }



   str += "\n\n\n\n\n\n";


   //#########################
   std::vector < std::pair <int, int> > InitialPositions;
   
   int pc = 0;
 
   FILE * posfp = std::fopen("./demo/SMT/simple_three_obstacles/initial_position.txt", "r");
   
   int temp_max_along_x = -9999; 
   int temp_max_along_y = -9999; 

   int temp_min_along_x = 9999; 
   int temp_min_along_y = 9999; 

   int ignore;




   while(!feof(posfp) )
   {

       if(pc ==0)
           std::fscanf(posfp,"%d", &ignore); 

       else
       {
	       int a,b;
           std::fscanf(posfp,"%d %d", &a, &b); 
	       InitialPositions.push_back(std::make_pair(a,b) );


	       if(a > temp_max_along_x)
	       {
	             temp_max_along_x = a;
	       }

	       if(a < temp_min_along_x)
	       {
	             temp_min_along_x = a;
	       }

	       if(b > temp_max_along_y)
	       {
	             temp_max_along_y = b;
	       }

	       if(b < temp_min_along_y)
	       {
	             temp_min_along_y = b;
	       }


        }
        
        pc++;
       
   }    
   

    str += "\n\n\n\n\n (declare-const Sensor_robot_id_up Int ) ";
    str += "\n (declare-const Sensor_robot_id_down Int ) ";
    str += "\n (declare-const Sensor_robot_id_right Int ) ";
    str += "\n (declare-const Sensor_robot_id_left Int ) ";

    int Sens_up_id = 0;
    int Sens_down_id = 0;
    int Sens_right_id = 0;
    int Sens_left_id = 0;

    for(int i=0;i<numBlocksRobo;i++)
    {

            if(InitialPositions[i].second  == temp_max_along_y)
                  Sens_up_id = i;

            if(InitialPositions[i].second == temp_min_along_y)
                  Sens_down_id = i;


            if(InitialPositions[i].first == temp_max_along_x)
                  Sens_right_id = i;

            if(InitialPositions[i].first == temp_min_along_x)
                  Sens_left_id = i;



    }   

    std::stringstream SnsUp;
    std::stringstream SnsDown;
    std::stringstream SnsRight;
    std::stringstream SnsLeft;

    SnsUp << Sens_up_id;
    SnsDown << Sens_down_id;
    SnsRight << Sens_right_id;
    SnsLeft << Sens_left_id;


    str += "\n\n(assert (= Sensor_robot_id_up ";
    str += SnsUp.str();
    str += ")) ";

    str += "\n\n(assert (= Sensor_robot_id_down ";
    str += SnsDown.str();
    str += ")) ";

    str += "\n\n(assert (= Sensor_robot_id_right ";
    str += SnsRight.str();
    str += ")) ";

    str += "\n\n(assert (= Sensor_robot_id_left ";
    str += SnsLeft.str();
    str += ")) ";

  
    str += "\n\n\n\n\n\n";



    // Segment specfies that initial postions of all robots are distinct i.e. all robots are initially in free blocks
    std::string disrobo = "";

    for(int i=1;i<=numBlocksRobo;i++)
    {
        std::stringstream icount ;
        icount << i;

        disrobo += "Pos_";
        disrobo += icount.str();
        disrobo += "_@_0 ";

    } 

    for(int i=1;i<=k;i++)
    {

        std::stringstream icount ;
        icount << i;

        disrobo += "o";
        disrobo += icount.str();
        disrobo += " ";

    } 


    str += "(assert (distinct ";
    str += disrobo.c_str();
    str += ") )\n\n\n";      


    return str;   
}



std::string SMTPrinterVelAcc:: _generatedCommandVars()
{

    std::string ret = "";


    // variables for Commands Generated by the robotic software
    for (int i= 1 ;i<= numRobos ; i++)
    {

        for (int j=-1;j<=T;j++)
        {

            std::string P = "";  

            std::stringstream a ;
            std::stringstream b ;

            a << i;
            b << j;

            P += "Command_";
            P += a.str();

            P += "_@_" ;
            P += b.str();


            ret += "(declare-const ";
            ret += P.c_str();
            ret += " String )\n"; 


        }
  
        ret += "\n";
 
    }

 
    for(int i=1;i<=numRobos;i++)
    {
       std:: stringstream robo_id;
       robo_id << i;


       ret += "(assert (= Command_";
       ret += robo_id.str();        

       ret += "_@_-1 \"null\") )\n\n\n"; 

    }


   return ret;

}






std::string SMTPrinterVelAcc:: _getTargetVars()
{

    std::string foo = "";
    std::string tQ = "";

    
    for (int i= 1 ;i<= numRobos ; i++)
    {

        std::string P = "";  

        std::stringstream a ;
        //std::stringstream b ;

        a << i;
        //b << j;

        P += "Targ_";
        P += a.str();

        P += "_@" ;
          
        tQ += P;
        tQ += " ";

        foo += "(declare-const ";
        foo += P.c_str();
        foo += " (p Int Int) )\n" ;

    }


    // Targets for different robots are distinct at any given time
    // foo +="\n\n";

    std::string ttQ = "";
    ttQ = tQ;

    for(int i=1;i<=numBlocksRobo;i++)
    {


        std::stringstream dicount ;
        dicount << i;

        ttQ += " Pos_";
        ttQ += dicount.str();
        ttQ += "_@_0";
   
    } 


    for(int i=1;i<=k;i++)
    {


        std::stringstream icount ;
        icount << i;

        ttQ += " o";
        ttQ += icount.str();
        ttQ += " ";


    } 

    // Distinct Targets
    foo += "(assert (distinct ";
    foo += ttQ.c_str();
    foo += " ))\n\n";




    for(int i=1;i<=numRobos;i++)
    {
   
        std::string pa = "";
        std::string pb = "";

        std::stringstream a ;
        std::stringstream b ;
            
        a << i;   
        b << i;

        pa += "Targ_";
        pa += a.str();


        //fprintf(fpz,"(assert ( = true (and  (>= (first %s_@) 1) (<= (first %s_@) %d)) )) \n", pa.c_str(), pa.c_str(), mapx );
       // fprintf(fpz,"(assert ( = true (and  (>= (second %s_@) 1) (<= (second %s_@) %d)) )) \n", pa.c_str(), pa.c_str(), mapy );


        foo += "(assert ( = true (and  (>= (first ";
        foo += pa.c_str();
        foo += "_@) 1) (<= (first ";
        foo += pa.c_str();
        foo += "_@) ";
        foo += MapWidth.str();
        foo += ")) )) \n"; 

        foo += "(assert ( = true (and  (>= (second ";
        foo += pa.c_str();
        foo += "_@) 1) (<= (second ";  
        foo += pa.c_str();
        foo += "_@) ";
        foo += MapHeight.str();
        foo += ")) )) \n"; 


    }    
 

   foo += "\n\n";











    return foo;

}


std::string SMTPrinterVelAcc:: _getSensorTupleVars()
{

    std::string sns_tup = "";

    sns_tup += "\n; Declarations Of Sensors Variables\n\n";

   // ############################################################
   // Sensors - quarduple for {Up, Down, Right, Left}   
   
   for (int i= 1 ;i<= numRobos ; i++)
   {

       for (int j=0;j<=T;j++)
       {

           std::string P = "";  

           std::stringstream a ;
           std::stringstream b ;

           a << i;
           b << j;

           P += "Sns_";
           P += a.str();

           P += "_@_" ;
           P += b.str();


           sns_tup += "(declare-const ";
           sns_tup += P.c_str();

           if(SensorType == 'S') 
               sns_tup += " (Sensors Bool Bool Bool Bool) )\n"; 

           if(SensorType == 'P') 
               sns_tup += " (Sensors Int Int Int Int) )\n"; 

           if(SensorType == 'R' )
           {
                if(if_model_sns_act_delays == 'n')
                    sns_tup += " (Sensors Bool Bool Bool Bool Bool) )\n"; 
                else
                    sns_tup += " (Sensors Bool Int Int Int Int) )\n"; 
           } 




      }
  
      sns_tup += "\n\n";
 
   }
 
   return sns_tup;

}



std::string SMTPrinterVelAcc:: _getInitialPos()
{

    std::string str = "";


    int pc = 1 ;
   
     std::vector < std::pair <int, int> > StartingPositions;
    int coord1, coord2;
 
    FILE * posfp = std::fopen("./demo/SMT/simple_three_obstacles/initial_position.txt", "r");
   
    int ignore; 

    while(!feof(posfp))
    {

       if(pc == 1)
           fscanf(posfp,"%d\n", &ignore); 


       fscanf(posfp,"%d %d\n", &coord1, &coord2  );

       StartingPositions.push_back(std::make_pair(coord1,coord2) );
    
       pc++;

   }    
   
   std::fclose(posfp); 

  
    for(int i=0;i<numBlocksRobo;i++)
    {
       
        std::string pa = "";
        std::string pb = "";

        std::stringstream a ;
        std::stringstream b ;
                
        a << i+1;   
        b << i+1;

        pa += "Pos_";
        pa += a.str();

        std::stringstream first_pos_coord;
        std::stringstream second_pos_coord; 
  
        first_pos_coord << StartingPositions[i].first;
        second_pos_coord << StartingPositions[i].second;

       
        str += "(assert (= ";
        str += first_pos_coord.str();
        str += " (first ";
        str += pa.c_str();
        str += "_@_0)  ) )\n";
        
        str += "(assert (= ";
        str += second_pos_coord.str();
        str += " (second ";
        str += pa.c_str();
        str += "_@_0)  ) )\n\n";

       }    

   str += "\n\n";
 
   return str;

}   



std::string SMTPrinterVelAcc:: _getSensorOutput()
{

    std::string foo = "";

    std::string funpar2 ;
    std::string Fa;

    //int kk ;
    std::string Sensa;

    std::string Fb;
    std::string Sensb;


    for(int i=1;i<=T+1;i++)
    {
        std::string funpar2 = "";
        

        for(int j=1;j<=numRobos; j++)
        {
        
            Fa = "";     
            Sensa = "";     

            Fb = "";     
            Sensb = "";     


            std::stringstream efa ;
            std::stringstream efb ;

            std::stringstream sefa ;
            std::stringstream sefb ;

     
            efa << i-1;
            efb << j;

            sefa << i-1;
            sefb << j;

            std::string effa = efa.str()  ;
            std::string effb = efb.str();

            std::string seffa = sefa.str()  ;
            std::string seffb = sefb.str();


            Fa += "(first Pos_";
            Fa += effb ;
            Fa += "_@_";


            Fa += effa;
            Fa += ") " ;


            Fb += "(second Pos_";
            Fb += seffb ;
            Fb += "_@_";

            Fb += seffa;
            Fb += ") " ;


            Sensa += "Sns_";
            Sensa += effb ;
            Sensa += "_@_";


            Sensa += effa;
            //Sensa += ") " ;


            Sensb += "Sns_";
            Sensb += seffb ;
            Sensb += "_@_";

            Sensb += seffa;
            //Sensb += ") " ;
        

            Fa += "(second Pos_";
            Fa += effb ;
            Fa += "_@_";

            Fa += effa;
            Fa += ") " ;


            Fb += "(first Pos_";
            Fb += seffb ;
            Fb += "_@_";


            Fb += seffa;
            Fb += ") " ;


            for(int kk=1;kk<=numRobos; kk++)
            {

                if(kk!=j){
                    std::stringstream fa ;
                    std::stringstream fb ;
     
                    fa << i-1;
                    fb << kk;

                    std::string ffa = fa.str()  ;
                    std::string ffb = fb.str();

                    Fa += "(first Pos_";
                    Fa += ffb ;
                    Fa += "_@_";


                    Fa += ffa;
                    Fa += ") " ;
               

                    Fa += "(second Pos_";
                    Fa += ffb ;
                    Fa += "_@_";


                    Fa += ffa;
                    Fa += ") " ;


                    Fb += "(second Pos_";
                    Fb += ffb ;
                    Fb += "_@_";


                    Fb += ffa;
                    Fb += ") " ;
               

                    Fb += "(first Pos_";
                    Fb += ffb ;
                    Fb += "_@_";


                    Fb += ffa;
                    Fb += ") " ;

                }
  
            }


            // Computing Sensor Values
            if(SensorType == 'S'  ||   SensorType == 'P' )
            {    
                foo += "(assert (= (up ";
                foo += Sensa.c_str();
                foo += ") (det_up ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (down ";
                foo += Sensa.c_str();
                foo += ") (det_down ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (right ";
                foo += Sensa.c_str();
                foo += ") (det_right ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

                foo += "(assert (= (left ";
                foo += Sensa.c_str();
                foo += ") (det_left ";
                foo += Fa.c_str();
                foo += ") ) )\n";         

            }

            else
            {


                foo += "\n\n\n\n(assert (= (rd ";
                foo += Sensa.c_str();
                foo += ")  (det_radar \"rd\" ";
                foo += Fa.c_str();
                foo += ") ) )"; 

     
                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (up "; 
                foo += Sensa.c_str();
                foo += ") true ) ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (up ";          
                foo += Sensa.c_str();
                foo += ")  (det_radar_up ";
                foo += Fa.c_str();
                foo += ") ) ))"; 

                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (down "; 
                foo += Sensa.c_str();
                foo += ") true ) ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (down ";          
                foo += Sensa.c_str();
                foo += ")  (det_radar_down ";
                foo += Fa.c_str();
                foo += ") ) ))"; 


                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (right "; 
                foo += Sensa.c_str();
                foo += ") true ) ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (right ";          
                foo += Sensa.c_str();
                foo += ")  (det_radar_right ";
                foo += Fa.c_str();
                foo += ") ) ))"; 

                foo += "\n\n(assert (implies (= true (rd ";
                foo += Sensa.c_str();
                foo += ") )   (= (left "; 
                foo += Sensa.c_str();
                foo += ") true ) ))";
                foo += "\n(assert (implies (= false (rd ";
                foo += Sensa.c_str();
                foo += ")) (= (left ";          
                foo += Sensa.c_str();
                foo += ")  (det_radar_left ";
                foo += Fa.c_str();
                foo += ") ) ))"; 







            }   
        }   
   
        foo += "\n\n";
    }

    return foo;
}    




std::string SMTPrinterVelAcc:: _getCommandGenerated()
{

    std::string ret = "";

    //##########################################

    for(int i=0;i<T;i++)
    {

        for(int j=1;j<=numRobos;j++)
        {
             
            std::string scom = "";
            std::string sns = "";
            std::string spos = "";
            std::string starg = "";
             
            std::stringstream aj ;
            std::stringstream ai;
            aj << j;
            ai << i;

            scom += "Command_"; 
            scom += aj.str();
            scom +=  "_@_";
            scom += ai.str();


            sns += "Sns_"; 
            sns += aj.str();
            sns +=  "_@_";
            sns += ai.str();

            spos += "Pos_"; 
            spos += aj.str();
            spos +=  "_@_";
            spos += ai.str();

            starg += "Targ_"; 
            starg += aj.str();
            starg +=  "_@";
            //starg += ai.str();



            std::stringstream prevCommand_index;
            std::stringstream roboId;

            prevCommand_index << i-1;
            roboId << j;

            ret +=  "(assert (= ";
            ret += scom.c_str();
            ret += " ( Mel Command_";
            ret += roboId.str();
            ret += "_@_";
            ret += prevCommand_index.str();
            
            // Composite Radar Sensor Value
            if(SensorType == 'R')  
            {
                ret += "  (rd ";
                ret += sns.c_str();
                ret += ")";
            }

            ret += "  (up ";
            ret += sns.c_str();
            ret += ")";

            ret += " (down ";
            ret += sns.c_str();
            ret += ")";

            ret += " (right ";
            ret += sns.c_str();
            ret += ")";

            ret += " (left ";
            ret += sns.c_str();
            ret += ")";
            

            ret += " (first ";
            ret += spos.c_str();
            ret += ")";

            ret += " (second ";
            ret += spos.c_str();
            ret += ")";

            ret += " (first ";
            ret += starg.c_str();
            ret += ")";

            ret += " (second ";
            ret += starg.c_str();
            ret += ")";

            ret += "       ) ) )\n";

        }
   
        ret += "\n\n";


    }   
 
    return ret;
}



std::string SMTPrinterVelAcc:: _getCommandActions()
{

    std::string str = "";

    //##################################################################################################
    for(int i=0;i<T;i++)
    {

        for(int j=1;j<=numRobos;j++)
        {

            std::stringstream aj ;
            std::stringstream ai;
            aj << j;
            ai << i;
     
            std::stringstream posai ;
            posai << i+1;
          
            std::stringstream posaiprev ;
            posaiprev << i+1;


            std::string scom = "";


            std::string spos = "";
            std::string sposprev = "";

            scom += "Command_"; 
            scom += aj.str();
            scom +=  "_@_";
            scom += ai.str();
  
            spos += "Pos_"; 
            spos += aj.str();
            spos +=  "_@_";
            spos += ai.str();


            sposprev += "Pos_"; 
            sposprev += aj.str();
            sposprev +=  "_@_";
            sposprev += posaiprev.str();


            str += "(assert (implies (= ";
            str += scom;
            str += " \"up\" ) (and (= (first "; 
            str += sposprev; 
            str += ") (first ";
            str += spos;   
            str += ") ) (= (second "; 
            str += sposprev; 
            str += ") (+ 1 (second ";
            str += spos;   
            str += ")) )    )  )  )\n";


            str += "(assert (implies (= ";
            str += scom;
            str += " \"down\" ) (and (= (first "; 
            str += sposprev; 
            str += ") (first ";
            str += spos;   
            str += ") ) (= (second "; 
            str += sposprev; 
            str += ") (- (second ";
            str += spos;   
            str += ") 1) )    )  )  )\n";


            str += "(assert (implies (= ";
            str += scom;
            str += " \"right\" ) (and (= (second "; 
            str += sposprev; 
            str += ") (second ";
            str += spos;   
            str += ") ) (= (first "; 
            str += sposprev; 
            str += ") (+ 1 (first ";
            str += spos;   
            str += ")) )    )  )  )\n";


            str += "(assert (implies (= ";
            str += scom;
            str += " \"left\" ) (and (= (second "; 
            str += sposprev; 
            str += ") (second ";
            str += spos;   
            str += ") ) (= (first "; 
            str += sposprev; 
            str += ") (- (first ";
            str += spos;   
            str += ") 1) )    )  )  )\n";


            str += "(assert (implies (= ";
            str += scom;
            str += " \"null\" ) (and (= (second "; 
            str += sposprev; 
            str += ") (second ";
            str += spos;   
            str += ") ) (= (first "; 
            str += sposprev; 
            str += ") (first ";
            str += spos;   
            str += ") )    )  )  )\n";
            
        }


        str += "\n\n";
    }

    return str;

}



std::string SMTPrinterVelAcc:: _getSafetySpec()
{

    std::string safety_spec = "";

    // Checking Safety
    safety_spec += "\n\n(declare-const collision Bool)\n\n\n";
    safety_spec += "(assert (implies (and \n";

    int j;
   
    for(int i=1;i<=T;i++)
    {  
        std::string Col = "";

        for(j=1;j<=numBlocksRobo; j++)
        {

            std::stringstream a ;
            std::stringstream b;


            b << i;
            a << j;

            Col += "Pos_";
            Col += a.str();
            Col += "_@_";
            Col +=  b.str();
            Col += " ";

        }

  
        for(int loopcnt=1;loopcnt<=k;loopcnt++)
        {


            std::stringstream icount ;
            icount << loopcnt;

            Col += "o";
            Col += icount.str();
            Col += " ";

        } 
 
        j = 1;


        safety_spec += "\n                     (distinct ";
        safety_spec += Col.c_str();
        safety_spec += " ) ";


        std::stringstream eval_step;
        std::stringstream robo_id;

        eval_step << i;
        robo_id << j;  
 
  

        for(int dummyVar = 1; dummyVar <= numBlocksRobo ; dummyVar ++)
        {
         
            std::stringstream tempVar_roboId;
            tempVar_roboId << dummyVar;

            safety_spec +=  "\n                     (>= (first Pos_";
            safety_spec += tempVar_roboId.str();
            safety_spec += "_@_";
            safety_spec += eval_step.str();

            safety_spec += ") 1)  (<= (first Pos_";
            safety_spec += tempVar_roboId.str();
            safety_spec += "_@_";
            safety_spec += eval_step.str();

            safety_spec += " ) ";
            safety_spec += MapWidth.str();

            safety_spec += ") (>= (second Pos_";
            safety_spec += tempVar_roboId.str();
            safety_spec += "_@_";
            safety_spec += eval_step.str();
            safety_spec += ") 1) (<= (second Pos_";

            safety_spec += robo_id.str();
            safety_spec += "_@_";
            safety_spec += tempVar_roboId.str();

            safety_spec += " ) ";
            safety_spec += MapHeight.str();

            safety_spec += ") ";


        }



        //fprintf(fpz,"\n                     (>= (first Pos_%d_@_%d) 1)  (<= (first Pos_%d_@_%d) %d) (>= (second Pos_%d_@_%d) 1)
         // (<= (second Pos_%d_@_%d) %d) ", j,i,j,i, mapx, j,i,j,i,mapy  );


 
    } 


    safety_spec += "\n\n) (= false collision) ))  \n\n\n";

    return safety_spec;

}




std::string SMTPrinterVelAcc:: _verifySpecNegation()
{

    std::string foo = "";
   
    foo += "\n\n; ####### Checking Safety\n\n";
    foo += "(assert (= true  (or (= true collision)  \n\n\n(and ";

    //############################################################ Checking Liveness 


    // fprintf(fpz,"; ##### Chehcking liveness\n\n");

    std::string Liva ;
    std::string Livb ;

    //  Commenetd Out for all robots reaching their respective targets
    //  for(j=1;j<=numRobos; j++)
    for(int j=1;j<=numBlocksRobo; j++)
    {


        Liva = "";
        Livb = "";

        std::stringstream a ;
        std::stringstream b;


        a << j;
        b << T;

        Liva += "Pos_";
        Liva += a.str();
        Liva += "_@_";
        Liva += b.str();

        Livb += "Targ_";
        Livb += a.str();
        Livb += "_@";

        foo += "(not (= ";
        foo += Liva.c_str();
        foo += " ";
        foo += Livb.c_str();
        foo += "))\n";
    

   }
        
 
   foo += "))))\n\n";
 
   return foo;

}






std::string SMTPrinterVelAcc:: _generateModel()
{

    std::string gen_model = "";
    gen_model += "\n(check-sat)\n(get-model)\n\n";

    return gen_model;
}
 